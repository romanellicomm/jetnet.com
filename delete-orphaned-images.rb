require 'uri'
require 'nokogiri'
require 'fileutils'

def whitelist_image?(image, safe_directories)
	safe_directories.any? { |dir| image.start_with?(dir) }
end

def get_all_images(path)
	images = []
	Dir.glob(File.join(path, '**', '*.{png,jpg,jpeg,gif,svg}')) do |file|
		images << file
	end
	images
end

def get_all_source_files(path)
	source_files = []
	Dir.glob(File.join(path, '**', '*.{erb,html,html.erb,html.markdown,css,scss,sass,js}')) do |file|
		source_files << file
	end
	source_files
end

def get_used_images(source_files)
	used_images = []
	source_files.each do |file|
		content = File.read(file)

		# Look for image URLs anywhere in the file
		matches = content.scan(/(?:\/)?assets\/[^)'\s]+?\.(?:png|jpg|jpeg|gif|svg)/)
		matches.each do |match|
			decoded_match = URI.decode_www_form_component("./source/#{match.delete_prefix('/')}")
			used_images << decoded_match
		end
	end

	used_images.uniq
end

def get_images_to_delete(all_images, used_images, safe_directories)
	images_to_delete = []
	all_images.each do |image|
		if whitelist_image?(image, safe_directories)
			puts "Whitelisted (safe directory): #{image}"
			next
		end
		unless used_images.include?(image)
			images_to_delete << image
		end
	end
	images_to_delete
end

def delete_images(images)
	images.each do |image|
		FileUtils.rm(image)
		puts "Deleted: #{image}"
	end
end

safe_directories = [
	'./source/assets/images/summit/galleries/',
	'./source/assets/uploads/pages.reports/',
]
assets_path = './source/assets/'
source_path = './source/'
source_files = get_all_source_files(source_path)

all_images = get_all_images(assets_path)
used_images = get_used_images(source_files)

images_to_delete = get_images_to_delete(all_images, used_images, safe_directories)
delete_images(images_to_delete)
