---
title: "Rising GPS Jamming & Spoofing Threats in Business Aviation"
summary: "The recent surge in GPS jamming and spoofing, driven by escalating political tensions and military conflicts, poses significant challenges for business aviation. Let’s delve into what this means for pilots and how they can stay informed and prepared for such incidents."
meta_description: Increasing GPS jamming and spoofing threats challenge business aviation, emphasizing the need for pilot awareness and preparedness.
image: /assets/uploads/blog/2023-11-21/GPSJam_MainImage_HR.jpg
opengraph:
  title: "Rising GPS Jamming & Spoofing Threats in BizAv"
  description: "Know More"
  image: /assets/opengraph/JN_GPSJamBlog_FB.jpg
twitter_card:
  title: "Rising GPS Jamming & Spoofing Threats in BizAv"
  description: "Know More"
  image: /assets/opengraph/JN_GPSJamBlog_TW.jpg
author: Dan Streufert
---

The recent surge in GPS jamming and spoofing, driven by escalating political tensions and military conflicts, poses significant challenges for business aviation. Let’s delve into what this means for pilots and how they can stay informed and prepared for such incidents.

**Understanding GPS Interference**

GPS interference manifests in two primary forms: “Jamming” and “Spoofing.” Jamming involves the creation of “noise” on the GPS frequencies, resulting in GPS malfunctions. Spoofing, on the other hand, is more deceptive, tricking an aircraft’s GPS receivers into believing the plane is in a different location than it actually is. The GPS signals from satellites are inherently weak, necessitating highly sensitive GPS receivers. This sensitivity, however, also means that these receivers are easily overwhelmed by “stronger” ground-based signals, which can overpower the satellite's feeble signals.

**Recent Incidents and Pilot Experiences**

In recent months, there have been some serious “spoofing” incidents on business aircraft flying near the Iraq/Iran border, with some aircraft coming close to erroneously entering Iranian airspace without a clearance! During these incidents, many aircraft required vectors from ATC in order to stay on course.

One Phenom 300 pilot describes his GPS interference incident as follows:

> On December 1, 2022 I took off from OPLA (Lahore, Pakistan). At approximately 1500' I encountered GPS jamming (or perhaps spoofing). The GPS failure caused AHRS failure and, most significantly, the HSI compass started spinning at a rapid rate and was unusable. The autopilot also failed. The AHRS did not recover…
>
> We were more concerned about returning to the airport. Handheld GPS devices were also jammed in this situation. We also received an immediate TAWS warning, which needed to be silenced.
>
> I can not stress how confusing and disconcerting this scenario was. Takeoff, GPS failure, TAWS collision alert, autopilot disconnect, spinning HSI compass all concurrently.
>
> Multiple other aircraft reported GPS issues—including Boeings and Airbuses. A second aircraft returned to the airport due to the GPS jamming, and a Gulfstream on approach went missed.
>
> We returned to the airport, refueled and waited for the GPS jamming to subside. Fortunately, I had an SIC and conditions were VMC. Single pilot IMC may have had a worse outcome.
>
> After this incident I would STRONGLY recommend you request this failure scenario in the SIM.

**Global Impact and Wider Concerns**

Be aware that GPS interference incidents aren’t confined to international regions. For instance, on January 21, 2022, GPS signals around the DEN VOR were sporadically unusable from the surface to FL400 for a period of 33 hours. Government investigations later revealed that a ground-based transmitter was mistakenly broadcasting on the GPS L1 frequency band, causing the disruption.

Moreover, on October 18, 2023, Dallas Airport experienced a significant GPS disruption lasting 24 hours. This incident led to the temporary suspension of some RNAV arrivals and necessitated traffic rerouting. The cause of this disruption remains under investigation and has not yet been identified.

<div class="row my-4">
  <div class="col-sm-12">
    <figure>
  	  <a data-gallery="map" data-caption="" href="/assets/uploads/blog/2023-11-21/GPS_Map1.jpg">
        <img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-11-21/GPS_Map1.jpg">
	  </a>
	</figure>
  </div>
</div>

In 2019, a passenger aircraft near Sun Valley, ID almost flew into the side of a mountain due to [a GPS interference incident](https://www.gpsworld.com/nasa-report-passenger-aircraft-nearly-crashes-due-gps-disruption/).

**Preventative Measures**

So, how can we be vigilant about these sorts of risks?  First off, pilots should check NOTAMs about any “known/planned” GPS interference.  Sometimes, these are planned by the US and other militaries and advance notice is available via a NOTAM.  

However, there is another method of real time detection. [ADS-B Exchange](https://www.jetnet.com/products/adsb-exchange/) gathers thousands of ADSB packets per second from around the globe.  Many of these packets contain feedback from the onboard avionics regarding the “quality” of the received signals.  The Navigation Accuracy Category for Position (NACp) value can be used to determine how accurate the GPS signals are.

<figure class="float-right ml-2 pl-3" style="width:50%">
  <a data-gallery="chart" data-caption="" href="/assets/uploads/blog/2023-11-21/Chart.png">
	<img class="w-100 img-fluid" src="/assets/uploads/blog/2023-11-21/Chart.png">
  </a>
</figure>

The FAA considers any NACp value less than 8 to be non-compliant.

 [ADS-B Exchange](https://www.jetnet.com/products/adsb-exchange/) and [JETNET](https://www.jetnet.com/) make this data available in real-time (or historically) to commercial customers for incorporation into their internal packages for analysis.  However, leading developer in the space, John Wiseman, has constructed a public-facing website ([GPSJAM.org](https://gpsjam.org)) that visualizes this data from around the globe on a daily basis.

As you can see below, global hotspots tend to have increased GPS interference as tensions escalate:

<div class="row my-4">
  <div class="col-sm-3">
	<figure>
	  <a data-gallery="gps-hotspots" data-caption="" href="/assets/uploads/blog/2023-11-21/GPS-Hotspots-1.jpg">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-11-21/GPS-Hotspots-1.jpg">
	  </a>
	</figure>
  </div>
  <div class="col-sm-3">
	  <figure>
		<a data-gallery="gps-hotspots" data-caption="" href="/assets/uploads/blog/2023-11-21/GPS-Hotspots-2.jpg">
		  <img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-11-21/GPS-Hotspots-2.jpg">
		</a>
	  </figure>
	</div>
  <div class="col-sm-3">
		<figure>
		  <a data-gallery="gps-hotspots" data-caption="" href="/assets/uploads/blog/2023-11-21/GPS-Hotspots-3.jpg">
			<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-11-21/GPS-Hotspots-3.jpg">
		  </a>
		</figure>
	  </div>
  <div class="col-sm-3">
	  <figure>
		<a data-gallery="gps-hotspots" data-caption="" href="/assets/uploads/blog/2023-11-21/GPS-Hotspots-4.jpg">
		  <img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-11-21/GPS-Hotspots-4.jpg">
		</a>
	  </figure>
	</div>
</div>

It’s important to ensure you familiarize yourself with the failure modes of the GPS system in your specific aircraft avionics package. It's also advisable to rehearse handling these scenarios in a simulator if available, to be better prepared for any GPS-related issues that might arise during flight.

What should you do if you encounter GPS interference? First, fly the plane, but once the situation is under control, the Cybersecurity and Infrastructure Security Agency (CISA) advises pilots to take the following actions:

1. Pilots experiencing a GPS anomaly should report the time, location, speed, magnetic heading altitude, and duration of the outage. 
2. Pilots in-flight immediately report, by radio contact, to the controlling Air Traffic Control (ATC) facility or Flight Service Station (FSS) and, by telephone, to the nearest ATC facility controlling the airspace where the disruption was experienced. 
3. Additionally, post-flight, pilots should complete the GPS Anomaly Reporting Form located on the Federal Aviation Administration (FAA) website at: [https://www.faa.gov/air_traffic/nas/gps_reports/](https://www.faa.gov/air_traffic/nas/gps_reports/).

In conclusion, the rise in GPS jamming and spoofing incidents presents a critical challenge for business aviation. Pilots should stay alert to these threats by regularly checking NOTAMs for planned GPS interference and using real-time detection tools like [ADS-B Exchange](https://www.jetnet.com/products/adsb-exchange/) and [GPSJAM.org](https://gpsjam.org/). Familiarity with your aircraft's GPS failure modes and practicing these scenarios in simulators is essential. In case of an incident, prioritize maintaining control, then report the disturbance to ATC and complete the FAA's GPS Anomaly Reporting Form. Through vigilant preparation and prompt reporting, pilots can navigate these challenges, contributing to the overall safety of business aviation.

Learn more about [ADS-B Exchange](https://www.jetnet.com/products/adsb-exchange/) and how our real-time & historical data on aircraft positions can provide even more precise insights into aviation trends and activities.

<%= partial 'partials/blog-signature-dan-streufert' %>

*JETNET maintains the world’s most comprehensive and granular database of actionable business aviation information. Our ability to collect, analyze and disseminate highly relevant business aviation data is unmatched. Visit us at JETNET.com to learn how we can help you make better business decisions by leveraging the power of world-class data and unbiased market intelligence.*