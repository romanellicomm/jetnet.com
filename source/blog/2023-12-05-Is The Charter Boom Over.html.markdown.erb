---
title: "Is The Charter Boom Over?"
summary: "In the most recent edition of Corporate Jet Investor’s (CJI) conferences, hosted in Miami, WINGX moderated a panel on the state of the business jet charter sector."
meta_description: "Explore key trends and challenges in the business jet charter industry discussed by leading experts at the 2023 Corporate Jet Investor Miami conference"
image: /assets/uploads/blog/2023-12-05/CharterBoom_MainImageHR.jpg
opengraph:
  title: "Is The Charter Boom Over?"
  description: "Know More"
  image: /assets/opengraph/JN_CharterBoomBlog_FB.jpg
twitter_card:
  title: "Is The Charter Boom Over?"
  description: "Know More"
  image: /assets/opengraph/JN_CharterBoomBlog_TW.jpg
author: Richard Koe
---

In the most recent edition of Corporate Jet Investor’s (CJI) conferences, hosted in Miami, [WINGX](https://wingx-advance.com) moderated a panel on the state of the business jet charter sector. We had a well qualified spectrum of panelists, including Leona Qi, president of [VistaJet US](https://www.vistajet.com/en-us/), Randy Mckinney, president of [Elevate Aviation](https://eag.aero), Per Marthinsson, founder of charter broker market place [Avinode](https://www.avinode.com), and Shaan Bhanji, who leads business development at [Tuvoli](https://www.tuvoli.com), which provides interface tools for B2B charter bookings. In short, the industry's largest global charter operator, a leading aircraft management group, and two key facilitators of the online business jet charter market, all came together for one intellectual panel discussion.

About one third of all business jet flights are operated by aircraft certified to provide commercial air transport, registered under the part 135 certificate in the US market. However, not all these flights are operated for third parties, with the passenger(s) often being the aircraft owner and/or their aircraft management company maintaining the certificate to cover both owner and third-party missions. About half of Part 135 flights are operated by what WINGX describes as ‘branded charter operators’, meaning operators whose fleets are dedicated to third party charter flights. Elevate Aviation would fall into the former category, Vista Jet into the latter. Both Avinode and Tuvoli facilitate such flights by connecting operators and brokers who represent the end users paying for the charter.

<div class="row my-4">
  <div class="col-sm-12">
	<figure class="my-0">
		<a data-gallery="charts" data-caption="Part 135 global bizjet flight activity Jan-No 2023 compared to previous years" href="/assets/uploads/blog/2023-12-05/Chart_1__Part_135_global_bizjet_flight_activity_Jan_No_2023_compared_to_previous_years.png">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-12-05/Chart_1__Part_135_global_bizjet_flight_activity_Jan_No_2023_compared_to_previous_years.png">
	  </a>
	  <figcaption class="mt-2"><i>Part 135 global bizjet flight activity Jan-No 2023 compared to previous years</i></figcaption>
	</figure>
  </div>
</div>

The context for the discussion is the rollercoaster performance in charter demand over the last three years. Surging charter demand was the first sign of rebound in business aviation following the onset of the pandemic. A combination of lockdown cabin-fever and the health hazard of flying brought in a raft of first-time users. The massive economic stimulus helped, as stock markets soared and quantitative easing accelerated. Charter trips were up 50% on pre-pandemic 2019 by the start of 2022. It was a genuine charter boom, limited only by the available capacity. The larger charter operators, conspicuously Vista Jet and Wheels Up, went on buying sprees to secure additional lift.

The data on charter operations this year though suggest that the boom is over. Part 135 operations are down 10% compared to last year, while branded charter flying has dropped almost 17% year-on-year and there are a number of variations within that trend. The European charter market seems to have taken the hardest hit with small aircraft seeing the softest demand and older aircraft of all sizes, drafted in to meet peak demand, now on the sidelines—smaller fleets have seen much lower unit utilization than the larger fleets. The common denominator is exposure to adverse operating conditions—cost of fuel, shortage of pilots, more regulation—and to the wider macroeconomic and geopolitical climate—conflict, risk, inflation, and stagnating growth.


<div class="row my-4">
  <div class="col-sm-12">
	<figure class="my-0">
		<a data-gallery="charts" data-caption="Operator Type flight activity Jan-No 2023 compared to previous years" href="/assets/uploads/blog/2023-12-05/Chart_2__Operator_Type_flight_activity_Jan_No_2023_compared_to_previous_years.png">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2023-12-05/Chart_2__Operator_Type_flight_activity_Jan_No_2023_compared_to_previous_years.png">
	  </a>
	  <figcaption class="mt-2"><i>Operator Type flight activity Jan-No 2023 compared to previous years</i></figcaption>
	</figure>
  </div>
</div>

The panelists at CJI Miami agreed that first-time entrants to the charter market had significantly inflated its size, accounting for as many as half the brokered requests in 2021 and 2022. Quick to enter the market, they have also been quick to quit. Many were only in it for the necessity created by the one-off conditions created by the pandemic. The good news is that their exit is not necessarily a bad thing for the industry. Inexperience in flying private and first time charter requests are often impractical, price-sensitive, and tend to have low conversion rates. While operators may see their overall activity decline as these customers erode, their margins may actually improve as a result.

There is also an important distinction between the charter programs offered by the likes of VistaJet’s block hours program and the spot-market charter provided by operators with variable availability on managed aircraft. The spot-market is typically business-to-business (B2B), and has come under relentless pricing pressure as an inflated pool of charter brokers compete to reverse-auction ad-hoc requests from end-users. Although the likes of Avinode and Tuvoli have done much to improve the efficiency of the spot market, it still suffers from search and transaction opacity, and wide variation in aircraft standards. 

On the other hand, the block hour programs offered by Vista Jet, distinctive from their ad hoc charter service provided through the XO brand, are more closely comparable in terms of customer profile to the leading fractional programs. Essentially, Vista Jet is competing as a global brand with NetJets and Flexjet. Their target customer’s travel requirements may have been buffeted by deteriorating market conditions, but users aren’t likely to exit the industry. Indeed, charter programs have benefited from wealthy new entrants to the industry who have found the spot charter market insufficiently reliable, but these programs have also benefited from aircraft owners selling their aircraft and shifting usage to the anonymity of charter programs. 

For the rump of the charter market, the next 12 months look challenging. Pure-play charter, the branded operators, will need to contend with softening demand and operating costs which are still significantly inflated. Many small operators, though still operational, will inevitably exit. And larger operators will continue to face the pressure of the once roaring, cooled post-pandemic market, with Wheels Up’s rescue by Delta being the obvious example. There is also more scope for operator consolidation, with the vast majority of charter fleets having just a handful of tails. But operator consolidation is challenging, with few fixed cost synergies and very regional brand recognition. 

Risks are mitigated for the multi-service aircraft management groups, like Elevate, who generally aren't exposed to the aircraft ownership and operating costs, and will continue to benefit from the aircraft’s usage by the owner. Charter revenue, shared with the owner, is a contribution to the bottom line but may be substantially outweighed from other revenue streams deriving from aircraft management, including maintenance, FBO and aircraft trading. Elevate is one of several management companies pursuing vertical integration, with the managed aircraft fleet at the hub of multiple service channels. Fractional operator Air Share’s acquisition of Wheels Up’s managed fleet in September is the most recent example.

Whilst the charter market has peaked in the near-term, it still appears to be substantially lower than it could be. Compared to 2019, the Part 135 fleet’s activity has grown by a third, even more modestly for the branded charter fleets. But even with this increase, the charter market is barely bigger than it was ten years ago. At the same time, the overall size of the bizjet fleet has almost doubled, suggesting an artificially low ceiling for charter operations. Much of this comes down to the challenge of sustaining profitable operations. An unregulated and oversized charter broker market depresses margins. Branded charter companies also need to compete with managed aircraft for charter, with owners willing to price spare capacity at operating cost.

For the charter end-user, exploring and booking the optimum charter is still a trying experience. Global brands and consistent standards are emerging, but the high level of intermediation tends to sustain opacity and inconsistency. It’s no surprise that some users get put off by high pricing and poor service, either leaving the industry or moving up to guaranteed programs. Inventory-search platforms, secure transaction tools, and fleet management software are amongst the many digital innovations which are addressing these barriers to growth. There won’t be a charter boom next year, but the years ahead should see a much larger charter business emerge.

Learn more about WINGX and how our actionable business intelligence can provide even more insight into charter operation trends and activities.

<div class="cta-buttons">
  <a href="https://wingx-advance.com" class="btn btn-primary">
	<span class="icon icon--double-arrow"></span> Learn More
  </a>
</div>

<%= partial 'partials/blog-signature-richard-koe' %>

*JETNET maintains the world’s most comprehensive and granular database of actionable business aviation information. Our ability to collect, analyze and disseminate highly relevant business aviation data is unmatched. Visit us at JETNET.com to learn how we can help you make better business decisions by leveraging the power of world-class data and unbiased market intelligence.*