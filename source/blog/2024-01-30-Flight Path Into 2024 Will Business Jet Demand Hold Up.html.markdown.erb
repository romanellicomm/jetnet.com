---
title: "Flight Path Into 2024; Will Business Jet Demand Hold Up?"
summary: Forecasting demand for business jet flying hasn’t exactly been straightforward the last few years, but that wasn’t always the case. Back in the teens, the year-to-year growth in flight activity was predictably paired to the stagnant market dynamics of an oversupplied fleet.
meta_description: "Forecasting demand for business jet flying hasn’t exactly been straightforward the last few years, but that wasn’t always the case."
image: /assets/uploads/blog/2024-01-22/24FlightPath_MainImageHR.jpg
opengraph:
  title: "Will Business Jet Demand Hold In 2024?"
  description: "Know More"
  image: /assets/opengraph/JN_BizJetDemandBlog_FB.jpg
twitter_card:
  title: "Will Business Jet Demand Hold In 2024?"
  description: "Know More"
  image: /assets/opengraph/JN_BizJetDemandBlog_TW.jpg
author: Richard Koe
---

Forecasting demand for business jet flying hasn’t exactly been straightforward the last few years, but that wasn’t always the case. Back in the teens, the year-to-year growth in flight activity was predictably paired to the stagnant market dynamics of an oversupplied fleet. Purchasers and users alike took a decade to nurse the hangover from the Global Financial Crisis. With green shoots from 2017, it was relatively clear that we were in a cyclical upswing into the early 20s. The continuation of that trend in January 2020, while appearing to make sense at the time, was then capsized by the black swan of COVID-19. Fast forward to April 2020, the future of business jet demand was in a black box.

Retrospectively, with health issues paramount and convenience trumping cost as the airlines grounded their fleets, it seems logical that an untapped market of potential business jet users would start flying private once the lockdown lifted. At the time, the pace of the upswing in business jet activity confounded many. Suppliers were caught on the hop and as demand overwhelmed supply, prices surged. The market was bound to restabilize but it wasn’t clear when that would happen, and few could have forecasted the Ukraine war in early 2022, which took the wind out of the European bizav market.

In March 2022, business jet activity was peaking 30% higher than previous records, but from there, demand started to subside. The supply chain repercussions of the Ukraine invasion were global, triggering runaway inflation and swift interest rate rises. From the back end of 2022, an economic recession loomed across major bizav markets in the US and Europe. Positive industry indicators like pre-owned transactions bounced off historic peaks and started to fade by 2023. Demand for new aircraft rose in astounding numbers, but production lines slowed as global supply chains distorted, making it difficult to keep up.

Twelve months ago, forecasting 2023 looked like a surer bet. Bizjet demand was bound to deflate further as the economy wobbled and inflation bit. The regional banking crisis in March 2023 was a case in point. However, despite a moderate slowdown in flight demand threatening an industry relapse, by the summer, demand in the US stabilized just a couple of points below 2022. A steady overall trend in flights, close to 20% above 2019 levels, held some significant geographical variances. Florida maintained its status as the post-COVID hub for bizjet travelers, whereas California ebbed as its economic fortunes deteriorated.

<div class="row my-4">
  <div class="col-sm-12">
	<figure class="my-0">
	  <figcaption class="mb-2"><b>Chart 1: Global Business Jet Activity In 2023 Compared To Previous Year</b></figcaption>
	  <a data-gallery="charts" data-caption="Global Business Jet Activity In 2023 Compared To Previous Year" href="/assets/uploads/blog/2024-01-22/Chart1.jpg">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2024-01-22/Chart1.jpg">
	  </a>
	</figure>
  </div>
</div>

The weak spot in utilization over the last year was in the charter market, especially in the entry-level space opened by the surge of newcomers during COVID. Wheels Up felt the pain, with its overextended service coverage seeing yawning losses in its financial results by the second half of 2023. This softness in the market was also reflected in smaller aircraft flying less. Peaks of the charter boom in 2022 dragged older fleets back into high frequency service and by mid-2023, these platforms were seeing activity levels fall back to pre-pandemic. Wheels Up was rescued by Delta Air Lines in September, while another significant operator of small jets, Jet It, closed its doors altogether in May.

<div class="row my-4">
  <div class="col-sm-12">
	<figure class="my-0">
	  <figcaption class="mb-2"><b>Chart 2: Business Jet Utilization By Operator Type</b></figcaption>
	  <a data-gallery="charts" data-caption="Business Jet Utilization By Operator Type" href="/assets/uploads/blog/2024-01-22/Chart2.jpg">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2024-01-22/Chart2.jpg">
	  </a>
	</figure>
  </div>
</div>

The charter slowdown was not restricted to the lightest end of the market. Global charter program provider VistaJet grew rapidly during the pandemic, needing multiple acquisitions of competing operators to meet rampant demand for large long-range aircraft. But when the market wobbled in early 2023, questions loomed around the sustainability of high operating costs and softening overall demand. As it turned out, Vista’s fleet mix was well suited to the variable demand for different business jet segments in 2023; light and heavy jets flying much less than before, but super midsize and ultra-long range jets continuing to see strong demand compared to 2019.

The environment for business operators was tougher in Europe. The EU’s economies were stagnant before the Ukraine war, already engulfed in inflation and debt from the lockdown. The Ukraine war exposed the Union’s energy insecurity, especially in Germany. Ironically, the energy crisis coincided with a post-COVID surge in climate activism around fossil fuel production. The use of business jets fell into the spotlight as exhibit A for extravagant pollution. 2023 saw a surge in incidents of disruption and sabotage at business aviation events, conferences and expos, making it difficult to not form a connection with the dwindling rates of corporate flight activity in Europe, now well below 2019 levels.

Corporate flight department activity in the US also waned. The inertia of digital working stalled the recovery of business travel in the pandemic’s aftermath, but in terms of new aircraft orders, corporate interest held strong according to the broker community. Inadvertently, corporate users began switching out of dedicated flight departments into fractional and managed programs. This partly explains the growth of leading aircraft management companies and fractional operators in 2023. It was a stellar year for fractional business, reaching new heights in flight activity and record-breaking options on new aircraft.

<div class="row my-4">
  <div class="col-sm-12">
	<figure class="my-0">
	  <figcaption class="mb-2"><b>Chart 3: Business Jet Demand By Segment</b></figcaption>
	  <a data-gallery="charts" data-caption="Business Jet Demand By Segment" href="/assets/uploads/blog/2024-01-22/Chart3.jpg">
		<img class="w-100 img-fluid blog-image-border" src="/assets/uploads/blog/2024-01-22/Chart3.jpg">
	  </a>
	</figure>
  </div>
</div>

The sustained demand for larger long-range aircraft has underlined the growing importance of geographic markets outside the US and Europe. The Middle East, and the Gulf States particularly, have seen a dramatic increase in business aviation since 2019. Although demand was galvanized by COVID, momentum grew from the broader government-backed investment programs in the region, not least in Saudi Arabia. The World Cup in Qatar in 2022 was a pinnacle, but also just one of a multitude of investments in sports and entertainment by Gulf States eager to cash in on global tourism. Geopolitically, the region was heading in the right direction, until October 2023.

Traditionally, geopolitics have been an outrider for business aviation demand, setting the bounds of growth within each industry cycle. But in recent years, geopolitical considerations have evolved beyond peripheral considerations. Whether bizav users work in energy, tech or finance markets, the geopolitical situation is now setting the agenda with seemingly dispersed crises in Ukraine, Israel and Taiwan joining up to spell out a narrative of a West under siege. The unipolar world in which business aviation long incubated is now coming undone and the globalization of trade and commerce, a locomotive for business travel since the 1990s, is coming off the tracks. It’s difficult not to see this as a headwind for the industry in the years ahead.

The flipside to this argument is that the risk and uncertainty of a geopolitically turbulent world may encourage people to use business aviation when they travel. Scheduled airlines may have largely recovered their capacity from pre-COVID, but airline service quality has been ridden by strikes, staff shortages, and logistics issues. On a broader canvas, the multipolar world emerging from the West’s globalization is galvanizing new trading relationships. Globally capable business jet operators and suppliers are benefitting from the growth of strategic hubs such as the Middle-East, and more broadly, the closer collaboration of countries across the global south.

Coming back to the numbers, in comparison to 2023, WINGX expects to see a modest increase in flight activity this year, reversing the decline we saw last year and moving the market back toward its record activity in 2022. Some growth in utilization would endorse the tenor of customer surveys, including JETNET iQ, while resilient demand for business jets would reflect a slightly improved though still-fragile economic climate. Regardless, business jet activity will benefit from growth in aircraft deliveries, particularly the large jets flying longer sectors, notably the G700 and Falcon 6X. The bias of deliveries towards fractional operations will also bolster growth.

Positive momentum will nonetheless be tempered by the prevailing headwinds—weakening consumption demand, especially in Europe, the distraction of elections, notably in the US, the ever-growing challenge of the sector’s poor environmental profile, and the ongoing kinks in global supply chains—these will limit any growth to a few percentage points. However, there may be upside risks, with inflation coming swiftly under control, or, perish the thought, a smooth presidential transition. On the other hand, the downside risk list is much longer. Geopolitical risk, and specifically the outbreak or escalation of regional conflicts, is worryingly near the top of that list. Fingers crossed for a smooth climb in 2024.

Learn more about WINGX and how our actionable business intelligence can provide even more insights in 2024 and beyond.

<div class="cta-buttons">
  <a href="https://wingx-advance.com" class="btn btn-primary">
	<span class="icon icon--double-arrow"></span> Learn More
  </a>
</div>

<%= partial 'partials/blog-signature-richard-koe' %>

*JETNET maintains the world’s most comprehensive and granular database of actionable business aviation information. Our ability to collect, analyze and disseminate highly relevant business aviation data is unmatched. Visit us at JETNET.com to learn how we can help you make better business decisions by leveraging the power of world-class data and unbiased market intelligence.*