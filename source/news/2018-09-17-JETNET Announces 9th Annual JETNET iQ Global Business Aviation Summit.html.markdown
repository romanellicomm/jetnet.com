---
title: JETNET Announces 9th Annual JETNET iQ Global Business Aviation Summit
opengraph:
  title: "Announcing JETNET iQ Summit 2019"
  description: "Early Bird registration details"
  image: /assets/opengraph/Sept27_PR_FB.jpg
twitter_card:
  title: "Announcing JETNET iQ Summit 2019"
  description: "Early Bird registration details"
  image: /assets/opengraph/Sept27_PR_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has announced that it will host its 9th annual JETNET iQ Global Business Aviation Summit in White Plains, NY on June 4-5, 2019. Registration for the event is open, and a very limited number of seats are available. The JETNET iQ Summit gathers the biggest names in business aviation including C-suite executives from sales and marketing to finance, manufacturing, supply, and data analysis, to share their unique perspectives on the current state and future direction of aviation.

READMORE

JETNET iQ provides independent quarterly intelligence for the business aviation industry, including economic and industry analyses, aircraft owner/operator survey results, and delivery and fleet forecasts. JETNET iQ recently published its 31st quarterly report. The foundation of JETNET iQ is a proprietary survey database with information from more than 15,000 owner/operator respondents from 129 countries, the largest on-going research of customer sentiment available in the business aviation industry.

“Our forecasts have proven to be 99.6% accurate for five years in a row, the best in business aviation,” said Rolland Vincent, Creator/Director of JETNET iQ. “Industry leaders and our participants say this Summit gives them a sharper focus on the direction of their businesses for the coming year, and rely on our regular reports to steer their efforts.”

The Summit affords attendees the opportunity to hear from and network with industry executives first-hand, as well as share insights with their peers. The event includes a workshop and opening reception on Tuesday, June 4, followed by an all-day program featuring industry speakers and panelists on Wednesday, June 5, at The Ritz- Carlton New York, Westchester in White Plains, New York. For the convenience of participants, the Summit will again coincide with the 2019 NBAA Regional Forum on June 6th at Westchester County Airport, White Plains, NY.

“Business aviation’s thought leaders bring their insights to share at this event, the most important of its kind in the industry,” said Paul Cardarelli, JETNET Vice President of Sales. “We’ll also be sharing our latest JETNET iQ research intelligence and forecast. There is no better way to stay informed by, and network with, the movers and shakers of our marketplace.”

“As a company executive, if you value accurate information and forecasts, you should be there, front and center, to see, be seen, and speak what’s on your mind,” added Mike Foye, JETNET Director of Marketing. “These spirited sessions encourage intelligent interaction, penetrating questions, and the wisdom of our participants as well. Ultimately, you’ll return home with real, independent intel with which you can make even better decisions.”

Participants can reserve seats by December 31st, 2018 for an early bird rate of $795, a savings of $300 off the regular rate of $1,095. More information and registration form can be found at JETNET.com/Summit.

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.


[Download the full release (PDF)](/assets/uploads/news/2019-JETNET-iQ-Summit-Announcement.pdf)
