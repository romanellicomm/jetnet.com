---
title: JETNET Demonstrating JETNET for Salesforce® on Salesforce AppExchange at 2020 NBAA Schedulers & Dispatchers Conference
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---


UTICA, NY – JETNET LLC, a leading provider of aviation market information, will be demonstrating its recently announced <a href="https://appexchange.salesforce.com/appxListingDetail?listingId=a0N3A00000FtT1XUAV">JETNET for Salesforce<sup>®</sup> on Salesforce AppExchange</a> at this year’s <a href="https://nbaa.org/events/2020-schedulers-dispatchers-conference/">NBAA Schedulers & Dispatchers Conference</a> at Booth #339 from March 10-13 at the Charlotte Convention Center in Charlotte, North Carolina. JETNET is empowering customers of JETNET and Salesforce services to access and import aircraft, companies and contacts from JETNET, including their simple step sync of chosen datasets. They will also be showcasing updates to their extensive list of products and services.

READMORE

**JETNET for Salesforce**

The new app allows JETNET and Salesforce clients to access and import JETNET company, contact, and aircraft data; create, update, and customize company, contacts, and aircraft records with a single click; maintain original—or create new—company relationships in aircraft records; navigate JETNET program links and records without leaving the Salesforce platform; and easily identify and sync data changes since your last visit, among other capabilities. JETNET worked closely on the implementation with <a href="https://cloudoneplus.com/">CRM consulting/development firm CloudOne+</a>, which specializes in Salesforce implementations for the aviation industry.

“JETNET for Salesforce provides access to one of the largest and most accurate business aviation databases from within the Salesforce Platform,” said Tony Esposito, JETNET President. “Salesforce clients can now work faster, identify prospects sooner, and better qualify opportunities.”

**New Products, Services**

JETNET will also be demonstrating a full suite of products and services tailored to supporting the needs of companies that provide fuel services, aircraft maintenance, interior and exterior aircraft refurbishment, concierge services, catering, ground service support, avionics equipment, aircraft management, insurance, and appraisal work, among others. JETNET also offers dozens of time-saving tools business aviation professionals can use to identify prospects and better understand their clients.

“Our Tech/Fuel Stop Search, Operator/Airport Flight Activity View, Route Analysis Tool, and Fleet Portfolio Manager Solution all continue to please customers and tantalize prospects,” says Mike Foye, Director of Marketing. “We are filled with an amazing sense of pride and exhilaration when the work of the research and program designers comes to life in our trade show demonstrations. It’s the rare occasion that people seeing our products are not transfixed, and ask how we can possibly get all of this information into a user-friendly interface. Despite all of the tools we’ve added and upgraded over the years, our tail number search remains a constant favorite. When that aircraft record opens and they start reading and clicking into the details, it’s magic.”

**Research**

JETNET’s tools capture aircraft specific data that is only actionable when paired with accurate contact information. “Knowing that the industry depends on our professional research staff to update and discover new owner/operator contact information on every aircraft record we archive keeps us centered and extremely focused,” says Lisa Carnevale, JETNET’s Director of Research. “We’ve elevated the research process to a science, and are constantly improving ways to clearly and concisely detail the key players associated with each aircraft. Our research team specializes in investigating and verifying key data found in a wide range of sources, such as fractional positions, leases, shared ownership, trusts, delivery positions, owners/operators, foreign registries, industry publications, and FAA documents. If we find a stone, we turn it over.”

**JETNET iQ 2020 Summit**

JETNET has also announced its <a href="https://www.jetnet.com/summit/">10th Anniversary JETNET iQ Global Business Aviation Summit</a>, to be held at the The Ritz-Carlton New York, Westchester in White Plains, NY, June 8-9, 2020. This year’s event features Keynote Speaker John G. Rosanvallon, Special Senior Advisor to the Chairman & CEO of Dassault Aviation, and brings together business aviation industry leaders from around the globe to to present information and engage in conversation on a wide range of topics facing the business aviation industry. Learn more at <a href="https://www.jetnet.com/summit/">www.jetnet.com/summit/</a>

JETNET invites attendees of the NBAA Schedulers & Dispatchers Conference to stop by their booth #339 to see live demonstrations of the latest products and features.

As a leading provider of aviation market information, JETNET delivers a comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is a source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

Salesforce and AppExchange are among the trademarks of <a href="https://www.salesforce.com/">salesforce.com</a>, inc.

[Download the full release (PDF)](/assets/uploads/news/JETNET-at-Schedulers-Dispatchers-Conference-2020.pdf)
