---
title: Business Aviation Podcast – 2021 Aircraft Price & Sales Activity Podcast
meta_description: "Listen to our associates Paul Cardarelli and Rolland Vincent featured in business aviation advisors 2021 podcast, discussing the future of aircraft price and sales activity."
opengraph:
  title: "2021 Business Aircraft Pricing And Sales Activity"
  description: "Listen Today"
  image: /assets/opengraph/Podcast_FB_LI.jpg
twitter_card:
  title: "2021 Business Aircraft Pricing And Sales Activity"
  description: "Listen Today"
  image: /assets/opengraph/Podcast_TW.jpg
---

We are glad to see 2020 in the rearview mirror, where there was an underwhelming amount of retailed aircraft deal consummated. So, what does this mean for 2021?

Our own Paul Cardarelli and Rolland Vincent had the opportunity to sit down with Gil Wolin from Business Aviation Advisor to discuss the future for business aircrafts, and our projections for both new and pre-owned aircraft price and sales activity looks like heading into the future.
READMORE

<img src="/assets/uploads/news/bizac.jpg" alt="" class="img-fluid" />

<br>

## Listen Here

**Business Aviation Advisor**

<iframe style="border: none" src="//html5-player.libsyn.com/embed/episode/id/17699489/height/90/theme/custom/thumbnail/yes/direction/backward/render-playlist/no/custom-color/008896/" height="90" width="100%" scrolling="no"  allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>

<br>

**Spotify**

<iframe src="https://open.spotify.com/embed-podcast/episode/5SQQah846UawgnlQW29vQr" width="100%" height="232" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<br>

**Apple Podcast**

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="175" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.podcasts.apple.com/us/podcast/2021-off-to-the-races-or-just-off/id1441881193?i=1000506669206"></iframe>
