---
title: JETNET Celebrates 30 Years Providing Aviation Intelligence
---

UTICA, NY – JETNET LLC, the leading provider of aviation market information, is celebrating its 30th anniversary this year. The firm has been a leader in using original research and the latest technology to provide timely and accurate intelligence to the business aviation community. JETNET provides information on business jets and turboprops, helicopters, and yachts.

“Since 1988, when our father started JETNET, we have pushed the boundaries of what’s possible in getting accurate information to our clients as quickly and conveniently as possible,” said Tony Esposito, JETNET Executive Vice President, who runs the company with his brother Vincent, who is President. “We have always stood apart because we obtain our intelligence with research, and we take advantage of the latest technology.”

READMORE

JETNET was among the first companies to deliver information using emerging technologies, including DOS on PCs, modems, Windows, email, SMS text messaging, and mobile devices.

“For three decades we have been in daily contact with aircraft owners and operators around the globe,” said Paul Cardarelli, JETNET Vice President of Sales. “From this effort has emerged the most comprehensive database of its kind for corporate and commercial aviation, covering more than 100,000 in-service airframes. We regard it our mission to empower our customers with unparalleled fleet and market intelligence to keep them a step ahead of the competition.”

Mike Foye, JETNET Director of Marketing, added, “We use the safest and fastest technology available, and we’re constantly designing and releasing new programs and capabilities. JETNET maintains more than 100,000 aircraft files that include both business and commercial owners and operators for jets, turboprops, pistons, helicopters, and yachts. We consider literally every sector of the industry when developing our products and solutions, and work to make every aspect of business aviation smarter.”

JETNET employs approximately 50 trained, full-time research professionals—many multilingual—who, in addition to contacting owners and operators on a regular basis, work with registries throughout the world to keep their information current. They have developed an extensive network to stay current on the needs of the fast-moving business aviation marketplace: trade show attendees around the world; customers across many facets of the industry, from sales to support; and top-tier consultant relationships. The result: the most complete and accurate global database of information in the business aviation market.

In addition to their aircraft database services, the company also provides aircraft utilization intelligence, market analysis reports, and transactions histories on aircraft dating back to 1988. In 2011, the JETNET iQ Business Aviation Forecast Service was launched, and is today considered a leading prognosticator for the corporate aviation sector. Under the JETNET iQ brand, the company holds an annual industry summit attended by thought leaders from across the industry. In the same year, it also launched Yachtspot as the first-ever database featuring the ability to identify owners of both aircraft and yachts. Today Yachtspot provides details on some 6,500 luxury yachts around the globe.

“We would like to thank our subscribers and supporters in the industry for a tremendous 30 years,” added Tony Esposito. “We look forward to advancing the health of business aviation for many years to come.”

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 100,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET 30th Anniversary.pdf)
