---
title: JETNET iQ Announces Educational Partnership with Vaughn College for 2022 JETNET iQ Global Business Aviation Summit
meta_description: "UTICA, NY - JETNET, the leading provider of corporate aviation information, has partnered with Vaughn College to provide students the opportunity to interact with, and learn from, industry leaders at the 2022 JETNET iQ Global Business Aviation Summit."
---

UTICA, NY - JETNET, the leading provider of corporate aviation information, has partnered with Vaughn College to provide students the opportunity to interact with, and learn from, industry leaders at the 2022 JETNET iQ Global Business Aviation Summit. New York City-based Vaughn College is a four-year private college offering programs in engineering, technology, management, and aviation. Three students have been selected to participate in the Summit, which will be held September 15-16, 2022, at the New York Marriott Marquis in Times Square.

“We are thrilled to partner with Vaughn College and have students take part in this year’s JETNET iQ Global Business Aviation Summit,” stated JETNET CEO, Greg Fell. He continued, “Students from Vaughn College represent tomorrow’s leadership within business aviation. We believe having them attend the event and hear from today’s leaders is essential for future industry success.”

The three students selected are Boyuan Chen, Rachel Chin, and Nusrat Khan. Chen is a graduate student studying Aviation Management and junior student Chin and senior Khan are both in Airport Management.

“Students like Boyuan, Rachel, and Nusrat will help pave the way for the next generation of business aviation leaders,” said Rolland Vincent, JETNET iQ Creator and Director. “We hope their experience at the Summit will inspire them, and us, to new opportunities and aspirations within our industry.”

JETNET’s dedication to leadership and diversity is present on their Summit’s agenda, including a key panel titled “Expanding the Talent Pool - Who Is Moving the Needle?” This panel includes Dr. Sharon DeVivo, President of Vaughn College and Chair of FAA’s Youth Access to American Jobs in Aviation Task Force (YIATF). Dr. DeVivo said, “Vaughn College serves an incredibly diverse population of students who become outstanding contributors to all facets of the aviation and aerospace industry.” She continued, “We are pleased to partner with JETNET iQ during their annual conference, and are so appreciative of the opportunity provided to students to attend, network, and add to their educational experience.”

**About JETNET iQ**

Summits are part of JETNET iQ, an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly Intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET iQ, visit [jetnet.com](https://www.jetnet.com/) or contact Rolland Vincent, JETNET iQ Creator/Director, at 972.439.2069 or [rollie@jetnet.com](mailto:rollie@jetnet.com). For information on JETNET, visit [jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com); Derek Jones, Lead Sales Contact: Americas & Asia Pacific, at 800.553.8638, or [derek@jetnet.com](mailto:derek@jetnet.com); Karim Derbala, Lead Sales Contact: EMEA (Europe, Middle East, Africa), at +41.0.43.243.7056 or [karim@jetnet.com](mailto:karim@jetnet.com).

**About Vaughn College**

Founded in 1932, Vaughn College is a private, nonprofit, four-year college that enrolls more than 1,500 students in master’s, bachelor’s, and associate degree programs in engineering, technology, management, and aviation on its main campus in New York City and online. The student-faculty ratio of 14 to 1 ensures a highly personalized learning environment. Ninety-nine percent of Vaughn College graduates are placed in professional positions, 89 percent in their field of study, or choose to continue their education within one year of graduation. The institution serves many first-generation college students and is recognized by the U.S. Department of Education as a Hispanic-Serving Institution. Vaughn was ranked no. 1 in upward mobility nationwide in a study conducted by The Equality of Opportunity Project.

[Download the full release (PDF)](/assets/uploads/news/JETNETiQ Summit_Vaughn College Partnership PR.pdf)
