---
title: JETNET Releases March 2018 and First Quarter 2018 Pre-Owned Business Jet, Business Turboprop, Helicopter, and Commercial Airliner Market Information
---

UTICA, NY – JETNET LLC, the leading provider of aviation intelligence, has released March 2018 and first quarter 2018 results for the pre-owned business jet, business turboprop, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in Table A are key worldwide trends across all aircraft market segments, comparing March and first quarter of 2018 to March and first quarter of 2017. **Generally, inventories are down across the board, except for Piston Helicopters! The Business Jet market showed the largest decrease in sale transactions (-3.8%) of all markets reported.**

Business Jets showed the largest decrease in for-sale numbers, with 350 fewer jets at the end of the first quarter of 2018 vs. 2017. Piston helicopters were the only segment to show an increase in for-sale numbers (up 13), from 541 in March 2017 to 554 in March 2018. Across all aircraft sectors, there was a total of 5,675, or 10.7% fewer, aircraft for sale in the quarterly comparison, with Business Jets accounting for 51% of the 680 fewer aircraft for sale.

Accordingly, Fleet For Sale Percentages for Business Jets and Business Turboprops are showing the largest decreases of all market sectors in the quarterly comparison, at 9.3% (down 1.9 pts.), and 6.9% (down .9 pt.), respectively.

Business Jet full sale transactions showed a 3.8% decrease, and business jets are taking less time to sell than last year (-16 days). However, Business Turboprops showed a decrease of -0.7% in sale transactions, and are taking more time to sell (+13 days).

Turbine Helicopters saw increases in sale transactions in the first quarter comparisons, at 4.7%. They took 8 fewer days to sell. However, Piston Helicopters are taking 104 fewer days, with a decline of 1.7% in sale transactions.

Commercial airliners are reported by JETNET in **Table A - Worldwide Trends**, and include the numbers for sale for both commercial jets (including airliners converted to VIP) and commercial turboprop aircraft. Interestingly, Business Jets (at 605) and Commercial Jets (at 516) accounted for 53% of the total full sale transactions (2,128). The total full sale transactions were up 2.5%, with commercial airliners showing the highest percentage increases.

**Table B** shows the March-ending changes from 2005 to 2018 for the business jet in-operation fleet, number for sale, percentage for sale, and the year-over-year point change (9.3% less 11.2% equals -1.9 points). March 2016 is the first March-ending period that the percentage for sale and year-over-year points have increased since March 2009 (highlighted in yellow). Interestingly, the March 2018 number of aircraft for sale (2,020) is the lowest number since March 2008.

<img src="/assets/uploads/images/2018-05-09-Table_A_-_Worldwide_Trends.jpg" class="img-fluid mx-auto d-block" alt="Table A">

In 2009, as the recession struck, there was an explosion of 62%—or 1,095—more business jets listed for sale compared to the year prior. What then followed was a seven-year trend toward recovery, as the market steadily shed inventory and moved closer to again being a healthy seller’s market. While this decrease in units for sale is a far cry from the pronounced spike of 2009, Q1 2018 at 9.3% must be regarded as a positive step in the direction of a stabilized inventory for sale.

<img src="/assets/uploads/images/2018-05-09-Table_B_-_Business_Jets.jpg" class="img-fluid mx-auto d-block" alt="Table B">

**Pre-Owned Full Retail Sale Transaction Trends**

In total, there were 24 fewer business jet transactions (-3.8%) in the first quarter of 2018 than the first quarter of 2017. The comparisons in **Table C** illustrate where the changes occurred by aircraft weight group.

The results were mixed by weight class when comparing the first quarters of 2018 and 2017. While the light jet weight class accounts for 33% (200 out of 605) of the total sale transactions, its change in the quarterly comparison showed a difference of 52 fewer transactions, and it was the only weight class to show a decline. The heavy segment (35,000+ lbs.) showed the largest increase in full retail sale transactions, up by 13 units, or 5.9%. At 233 total sales, this segment accounted for 38% of all pre-owned retail jet transactions in Q1 2018.

<img src="/assets/uploads/images/2018-05-09-Table%20C%20-%20Business%20Jets%20by%20Weight%20Class.jpg" class="img-fluid mx-auto d-block" alt="Table C">

**U.S. GDP**

From 1950 to 2000, U.S. economic growth as measured by GDP averaged 3.6% annually. But since 2000, GDP growth has been cut by half to 1.7%. Meanwhile, the world economy has been slowing down, and the expected recovery is rather shallow and surrounded by risks. Global GDP growth is expected to have reached a low point in 2016, and is projected to pick up gradually from 3.2% in 2016 to 3.7% in 2017 and 3.9% in 2018.

When the U.S. GDP is 3.0% or higher, business aviation does very well. Table D shows the U.S. GDP by year from 2000 to 2017. The years 2000, 2004, and 2005 were the only years that the U.S. GDP growth rate was 3.0% or greater annually (highlighted in yellow).

The U.S. economy certainly seems stronger, as a 1.5% GDP in 2016 improved to 2.3% in 2017.

<img src="/assets/uploads/images/2018-05-09-Table%20D%20-%20US%20GDP.jpg" class="img-fluid mx-auto d-block" alt="Table D">

Real gross domestic product—the output of goods and services produced by labor and property located in the United States—increased at an annual rate of 2.3% in the first quarter (that is, from the fourth quarter of 2017 to the first quarter of 2018), according to the “advance” estimate released by the <a href="https://www.bea.gov/">Bureau of Economic Analysis (BEA)</a>. In the fourth quarter, real GDP increased 2.9%.

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET March Market Info 2018.pdf)
