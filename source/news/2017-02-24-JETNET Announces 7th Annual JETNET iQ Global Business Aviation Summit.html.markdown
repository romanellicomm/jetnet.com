---
title: JETNET Announces 7th Annual JETNET iQ Global Business Aviation Summit
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has announced that it will host its 7th annual JETNET iQ Global Business Aviation Summit in New York City on September 5-6, 2017.

READMORE

The JETNET iQ Summit brings together titans of the business aviation industry—in fields as diverse as finance, manufacturing, supply, and data analysis—along with sales and marketing executives who seek an unvarnished take on the current state and future direction of aviation. This year’s Summit will feature a workshop and opening reception on Tuesday, September 5, followed by an all-day program featuring industry speakers and panelists on Wednesday, September 6, at The Westin New York at Times Square, 270 West 43rd St., New York City. For the convenience of participants, the Summit will once again coincide with the NBAA Regional Forum, to be held at Morristown Airport (MMU) in Morristown, NJ on Thursday, September 7, 2017.

“Our venue and timing make this the perfect event for aviation’s thought leaders to gather,” said Paul Cardarelli, JETNET Vice President of Sales. “We’ll be sharing our latest JETNET iQ research insights, and industry leaders will provide their intelligence and predictions from many perspectives. Together, they inform business aviation’s most compelling single event.”

JETNET iQ provides independent quarterly intelligence for the business aviation industry, including economic and industry analyses, aircraft owner/operator survey results, and delivery and fleet forecasts. JETNET iQ recently published its 24th quarterly report. The foundation of JETNET iQ is a proprietary survey database with information from more than 12,000 owner/operator respondents from 129 countries, the largest on-going research of customer sentiment available in the business aviation industry.

“For three years in a row, our forecasts have proven to be 99.6% accurate, the best in the aviation industry,” said Rolland Vincent, Creator/Director of JETNET iQ. “Our participants tell us they look forward to hearing our predictions, and subscribe to our regular reports once they see how well we map out the coming year. It makes our Summit that much more impactful.”

[Download the full release (PDF)](/assets/uploads/news/JETNET_Announces_2017_iQ_Summit.pdf)
