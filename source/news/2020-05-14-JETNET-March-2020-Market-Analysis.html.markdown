---
title: JETNET Releases March 2020 and First Quarter 2020 Pre-Owned Business Jet, Business Turboprop, Piston, Helicopter, and Commercial Airliner Market Information
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---

UTICA, NY – JETNET LLC, the leading provider of aviation market intelligence, has released March 2020 and first quarter 2020 results for the pre-owned business jet, business turboprop, piston, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in Table A are key worldwide trends across all aircraft market segments, comparing March and first quarter of 2020 to March and first quarter of 2019. Generally, inventories are up across the board, but only slightly, from 5.5% last year to 5.6% in 2020. Total sale transactions for aircraft and helicopters were down 337, or 15.4%, for all the markets reported in the first quarter of 2020 compared to the first quarter of 2019.

Across all aircraft sectors, there were a total of 6,369, or 3.0%, more aircraft for sale in the quarterly comparison. This was a difference of 186 more aircraft and helicopters for sale.

Interestingly, Fleet For-Sale percentages for business jets showed 9.9% for 2020, an increase of .6 points from 9.3% for 2019 in the quarterly comparison.

Total full sale transactions were down by 337 aircraft and helicopters. The top two were Turbine Helicopters (- 144), and Commercial Airliner Jets (-89) totaling 233, or 69%, of the total decline in the quarterly comparisons.

Business Jet full sale transactions showed a 5.8% decrease, and are taking more time to sell (13 days) than last year. However, Business Turboprops showed a decrease of 13.8% in sale transactions, and are selling in less time (51 fewer days) comparing 2020 to 2019.

Turbine Helicopters saw a large decrease in sale transactions in the first quarter comparisons, at -34.5%. However, turbine helicopters took 31 fewer days to sell.

Commercial Airliners are reported by JETNET in Table A - Worldwide Trends and include the numbers for sale for both commercial jets (including airliners converted to VIP) and commercial turboprop aircraft. Business Jets (at 507) and Commercial Jets (at 358) accounted for 47% of the total full sale transactions (1,846). Interestingly, full sale transactions were up 7.1% for commercial turboprops, while piston aircraft also showed a 5.6% increase. Note: JETNET does not cover all piston aircraft inventory or sales.

**Table A – Worldwide Trends**

<img src="/assets/uploads/images/2020-05-14-TableA.jpg" class="img-fluid mx-auto d-block" alt="Table A – Worldwide Trends">

Table B shows the March-ending changes from 2005 to 2020 for the Business Jet in-operation fleet, number for sale, percentage for sale, and the year-over-year percentage point change. March 2020 marks the first time since 2016 that the month of March demonstrated a year-over-year increase in the fleet percentage for sale. Interestingly, the March 2018 number of aircraft for sale (2,020) is the lowest number since March 2008. While the percentage for sale increased to 9.9% comparing 2020 to 2019, the number of for sale increased by 162, or 7.9%, to 2,215.

In 2009, as the great recession struck, there was an explosion of 62%, or 1,095, more business jets listed for sale compared to the year prior. What then followed was a trend toward recovery, as the market steadily shed inventory and moved closer to again being a healthy seller’s market (below 10%). This increase in units for sale in Q1 2020 at 9.9% is a far cry from the pronounced spike of 2009 at 17.9% and 2,857 for sale (shown in yellow in table B).

**Table B – Business Jets**

<img src="/assets/uploads/images/2020-05-14-TableB.jpg" class="img-fluid mx-auto d-block" alt="Table B – Business Jets">

**PRE-OWNED FULL RETAIL SALE TRANSACTION TRENDS**

Real gross domestic product (GDP) decreased 4.8% in the first quarter of 2020, according to the "advance" estimate released by the Bureau of Economic Analysis. In the fourth quarter of 2019, real GDP increased 2.1%.

This 4.8% decrease in US GDP in the first quarter of 2020 is very concerning. In the first quarter of 2009, US GDP declined by 6.4%. There were 337 (15.4%) fewer transactions in the first quarter of 2020 than the first quarter of 2019. Interestingly, almost 80% of the transaction decline in the first quarter of 2020 was in the United States.

**Turbine Helicopters**

Chart A displays the 12-month moving average for Turbine Helicopters full retail transactions from March 2012 to March 2020. From January 2017, used turbine helicopters transactions steadily increased until May 2018, at 1,749. Then there was a sudden drop to 1,375 transactions in March 2020, below the low point in January 2017.

**Chart A – Turbine Helicopters**

<img src="/assets/uploads/images/2020-05-14-ChartA.jpg" class="img-fluid mx-auto d-block" alt="Chart A - Turbine Helicopters">

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET%20March%202020%20Market%20Analysis.pdf)
