---
title: JETNET Acquires ADS-B Exchange
meta_description: "Acquisition extends JETNET’s product offering by providing real-time and historical flight data to the aviation industry"
opengraph:
  title: "JETNET Acquires ADS-B Exchange"
  description: "Know More"
  image: /assets/opengraph/ADSBexchangePR_FB.jpg
twitter_card:
  title: "JETNET Acquires ADS-B Exchange"
  description: "Know More"
  image: /assets/opengraph/ADSBexchangePR_FB.jpg
---

UTICA, NY - [JETNET](https://www.jetnet.com/), a leading provider of aviation data and market intelligence, announced today that it acquired [ADS-B Exchange](https://www.adsbexchange.com/), one of the world’s largest networks of ADS-B/Mode S/MLAT feeders and providers of real-time and historical flight data. The acquisition is the second of what the company anticipates will be several future acquisitions as JETNET expands its data-driven product offerings for the aviation industry.

Founded in 2016 by Dan Streufert, ADS-B Exchange aggregates approximately 750,000 messages per second worldwide via receivers hosted by aviation enthusiasts around the world. The acquisition of ADS-B Exchange will enable JETNET to expand its flight data solutions with real-time information.

“ADS-B Exchange was founded as the go-to resource for aviation and flight-data enthusiasts,” said Dan Streufert, President and Founder of ADS-B Exchange. “Joining forces with JETNET is the perfect match as we look to meet the business needs of our users while maintaining our enthusiast roots and unfiltered data. With a long history of providing highly valuable data to the aviation industry, JETNET offers the resources we need to accelerate our growth.”

Like JETNET, ADS-B Exchange serves numerous constituents across the aviation industry, including Maintenance, Repair, Overhaul (MRO), airport operations, and aircraft leasing. In addition, its real-time data is used by dozens of commercial customers across numerous end markets, including aerospace & defense, government, research/academic, and financial services. 

“We are committed to providing our customers with innovative product offerings which provide the information and intelligence they rely on to make critical business decisions,” said JETNET CEO Derek Swaim. “We’ve long admired ADS-B Exchange and know how strategic the company’s real-time data offerings are to the aviation industry. Dan has done an incredible job building a fast-growing business that customers love. We believe he, and the ADS-B Exchange platform, will bring significant value to our customers.”

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit [www.jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or paul@jetnet.com.

**About ADS-B Exchange**

ADS-B Exchange collects data from over 10,000 receivers located around the world, aggregating the data onto a real-time web-based display for enthusiasts, and raw-data products for professional and commercial usage. Visit [www.ADSBexchange.com](http://www.ADSBexchange.com) for more information.

**About Silversmith Capital Partners**

Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $3.3 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, Appfire, DistroKid, impact.com, Iodine Software, LifeStance Health, PDFTron, and Webflow. For more information, including a full list of portfolio investments, visit [www.silversmith.com](http://www.silversmith.com) or follow the firm on [LinkedIn](https://www.linkedin.com/company/silversmith-capital-partners/).

**Media Contact:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET_PR_ADSBexchange.pdf)
