---
title: JETNET Demos Customized Solutions at VBACE
meta_description: 
opengraph:
  title: "JETNET Demos Customized Solutions at VBACE"
  description: ""
  image:
twitter_card:
  title: "JETNET Demos Customized Solutions at VBACE"
  description: ""
  image:
---

UTICA, NY - JETNET, the world leader in business aviation market intelligence, will be showcasing their full suite of next-gen products and services during the NBAA GO Virtual Business Aviation Convention & Exhibition (VBACE).
READMORE

**Virtually Above the Rest**

VBACE will be the first-ever, completely immersive online business aviation trade show. The two-day event is free for members to attend and will incorporate many of the traditional elements of NBAA’s successful live events, including over 200 virtual 3D exhibit booths, two keynotes, press conferences, education sessions and more.

JETNET invites business aviation professionals to their virtual booth, where they can speak with representatives in real time and take part in customizable demonstrations of their products. JETNET’s demonstrations can be tailored for a wide array of business aviation solutions including Aircraft Sales Dealers and Brokers, FBOs, Ground Services, Parts and Maintenance, OEMs, Analysts and Consultants, Big Data, and Business Consulting. Each demo is a unique experience based on the client’s needs. No other demonstration in the industry provides users with as niche of an experience, nor with as much detailed, readily available customized research data.

**Annual State of the Market Briefing**

As they have for past conferences, JETNET iQ will be holding their annual State of the Market Press Conference, on Wednesday, December 2, at 10:00am EDT, hosted by Rolland Vincent, Creator/Director of JETNET iQ, and Paul Cardarelli, JETNET Vice President of Sales. JETNET iQ will provide details on market conditions, the latest forecasts on fleet and new aircraft deliveries of business fixed wing turbines, and proprietary insights from JETNET iQ’s Global Business Aviation Surveys of business aircraft owner/operator of opinions. The largest on-going research effort of its kind, JETNET iQ Reports include insights from more than 17,000 survey respondents in 133 countries worldwide.

As the leading provider of business aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of business aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace,
comprised of more than 112,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

More detail, including registration, booth and press conference information will be available as it is updated at <a href="https://nbaa.org/professional-development/on-demand-education/nbaa-go/2020-vbace/vbace-newsroom/vbace-press-conference-schedule/">https://nbaa.org/professional-development/on-demand-education/nbaa-go/2020-vbace/vbace-newsroom/vbace-press-conference-schedule/</a>.

[Download the full release (PDF)](/assets/uploads/news/JETNET VBACE 2020.pdf)
