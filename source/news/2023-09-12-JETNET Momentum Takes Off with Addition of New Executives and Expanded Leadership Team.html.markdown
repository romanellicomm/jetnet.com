---
title: JETNET Momentum Takes Off with Addition of New Executives and Expanded Leadership Team
meta_description: "Data intelligence company achieves record revenue and customer growth over past year following completion of three strategic acquisitions."
opengraph:
  title: "Meet Our Emerging Leaders"
  description: "Know More"
  image: /assets/opengraph/JN_ExpandedLeadership_FB.jpg
twitter_card:
  title: "Meet Our Emerging Leaders"
  description: "Know More"
  image: /assets/opengraph/JN_ExpandedLeadership_TW.jpg

---
UTICA, NY - JETNET, a leading provider of aviation data and market intelligence, today announced significant momentum with the expansion of its executive and leadership teams as the company propels its vision to advance aviation intelligence worldwide. JETNET is pleased to welcome newly appointed executives Josh Baird as Chief Operating Officer, Katherine Minty as Chief Financial Officer, Eric Foss as Senior Vice President of Engineering, and Greg Kimball as Senior Vice President of Product, as well as two additional leadership team members. 

“When I joined JETNET a year ago, I believed the company had a huge opportunity to accelerate its growth by bringing more data and intelligence to the aviation market,” said JETNET CEO, Derek Swaim. “I am proud of the talented team who has helped us harness that opportunity to create incredible value for our customers and the company. As we embark on a new phase of growth, I am excited to have the expertise and experience of the new additions to our leadership team.”

**New Executive Appointments:**

**Josh Baird** – *Chief Operating Officer (COO)*
Josh is an experienced finance and strategy executive who previously served as COO, CFO and Board Member at RedAwning. At JETNET, Josh oversees the company’s day-to-day operations, driving operational excellence and strategic alignment.

**Katherine Minty** – *Chief Financial Officer (CFO)*
Katherine joined JETNET in 2022 as Vice President of Finance and brings significant experience in mergers & acquisitions, financial analysis, and strategic planning. Katherine leads the transformation of JETNET’s finance, accounting, and HR functions to position the company for continued growth.

**Eric Foss** – *Senior Vice President of Engineering*
Eric brings extensive experience in delivering software and technology solutions across a variety of sectors including aviation. Prior to JETNET,  Eric was VP of Software Development at Absorb Software, and served in Director of IT roles at WestJet for over 11 years.

**Greg Kimball** – *Senior Vice President of Product*
Greg is a creator and builder with deep experience in product management. Prior to JETNET, Greg served in senior product management leadership roles at Validity and ReturnPath.

**Expanded Leadership Team:**

In addition to the new executive appointments, JETNET announced new members and promotions on its leadership team including:

**Craig Smith** – *Vice President of Program Management*
Craig brings more than 20 years of experience in technology and operations roles including serving as Executive Vice President of Operations at Absorb Software.  

**Julie Mandello** – *Controller*
Julie is a seasoned controller with over a decade of experience in finance roles. Prior to JETNET she served as a controller for MDaudit.

The new leadership team members join an existing team of experienced business aviation professionals, many with over 20 years serving the industry, including Jason Lorraine, recently promoted to Assistant Vice President of Strategic Solutions and Product Sales and David Labrecque, recently promoted to Assistant Vice President of Research.  

Since closing a majority strategic growth investment from Silversmith Capital Partners in June 2022, JETNET has accelerated growth across all aspects of the business. Milestones the company has achieved over the past year include:

*	Closed three strategic acquisitions including Asset Insight (October 2022), ADS-B Exchange (January 2023) and WINGX (June 2023), which enabled JETNET to expand product offerings with real-time flight data, flight data analytics, and accelerate international growth. In addition, the acquisitions brought highly-respected aviation data and intelligence veterans to the team including Richard Koe and Christoph Kohler, co-founders of WINGX, and Dan Streufert, founder of ADS-B Exchange
*	Surpassed 60% revenue growth 
*	Increased customer base by over 30%

**About JETNET**

As a leading provider of market research and data for the global aviation market, JETNET delivers comprehensive and reliable insights to customers worldwide. JETNET is the ultimate source of fixed-wing and helicopter fleet information, including transaction data, market intelligence, flight data and analytics, and industry forecasts. Headquartered in Utica, NY, JETNET offers comprehensive, user-friendly aircraft and flight data via APIs and real-time web applications.

For information on JETNET and our exclusive services, visit www.jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com), or Karim Derbala, JETNET Managing Director of Global Sales at +41 (0) 43.243.7056 (Switzerland) or [karim@jetnet.com](mailto:karim@jetnet.com).

**Media Contact:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET_Expanded_Leadership.pdf)
