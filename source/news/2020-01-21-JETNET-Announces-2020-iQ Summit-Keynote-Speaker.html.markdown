---
title: John Rosanvallon, Legendary Leader of Dassault Falcon Jet, To Be 2020 JETNET iQ Summit Keynote Speaker
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---


UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has announced that John G. Rosanvallon, Special Senior Advisor to the Chairman & CEO of Dassault Aviation, will be Keynote Speaker at their 2020 10th Anniversary JETNET iQ Global Business Aviation Summit. The 10th annual summit of business aviation’s leaders will be held June 8-9, 2020 in White Plains, New York.

“We are honored to have an industry legend of Mr. Rosanvallon’s stature headline our 2020 event,” said Paul Cardarelli, JETNET Vice President of Sales. “He can offer a unique and insightful perspective on our industry.”

READMORE

**John G. Rosanvallon**

In a career spanning 45 years, John Rosanvallon has been devoted to the worldwide development of business aviation. A former CEO of Dassault Falcon Jet and Executive Vice President, Civil Aircraft of Dassault Aviation, John is currently Special Senior Advisor to the Chairman & CEO of Dassault Aviation. Rosanvallon is also on the Board of Directors of the General Aviation Manufacturers Association, the Corporate Angel Network, and the French-American Chamber of Commerce, and serves on the Advisory Council of the National Business Aviation Association. He has received the Lifetime Aviation Industry Leader Award, the French Legion of Honor, and the French National Order of Merit. Learn more about Rosanvallon at <a href="/summit/speakers/">jetnet.com/summit/speakers</a>

**10th Anniversary JETNET iQ Summit**

This will be the 10th Anniversary JETNET iQ Global Business Aviation Summit, an event that has grown every year, and brought together business aviation industry leaders from around the globe to discuss the industry. This year’s event will be held at the The Ritz-Carlton New York, Westchester in White Plains, NY, June 8-9, 2020. At the summit, Rolland Vincent, iQ Creator and Director, and Paul Cardarelli, JETNET Vice President of Sales, will join dozens of global business aviation C-level leaders and professionals to present information and engage in conversation on a wide range of topics.

JETNET iQ Summits feature a virtual Who’s Who of business aviation discussing the present and future of the business aviation industry, with predictions, industry revelations, and wisdom from global leaders. The fast- paced summit will also allow attendees time to meet their contemporaries, network, and develop topics outside of scheduled sessions.

A discounted Early Bird Registration rate for the JETNET iQ Summit, $200 savings off the regular rate, is available through January 31, 2020 at <a href="/summit/register/">jetnet.com/summit/register/</a>

**About JETNET iQ**

Summits are part of JETNET iQ, an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET LLC and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy.

**About JETNET LLC**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.


[Download the full release (PDF)](/assets/uploads/news/JETNET-iQ-Summit-2020-Keynote-Announced-3.pdf)
