---
title: JETNET Announces Greg Fell as Company’s First Chief Executive Officer
meta_description: ""
opengraph:
  title: "JETNET’s New CEO"
  description: "Read More"
  image: /assets/opengraph/GFell_CEO_FB.jpg
twitter_card:
  title: "JETNET’s New CEO"
  description: "Read More"
  image: /assets/opengraph/GFell_CEO_TW.jpg
---

UTICA, NY - JETNET, the leading global provider of business aviation data announced that Greg Fell has been named the first Chief Executive Officer in the company’s history.
READMORE

Fell has nearly 30 years of senior management experience, including strategic development and IT infrastructure growth, across the industries of business aviation, automotive, software/technology, and social media.

“Greg is the right leader for JETNET, at the right time” said Tony Esposito, President, owner, and majority shareholder of JETNET. “This was an essential and momentous decision for us and Greg’s extensive experience, values, and ethics helped us realize that he was the appropriate person to bring into the fold.”

Fell most recently served as Chief Executive Officer for Display Social, a social media company where he led the company through a successful launch that gained seven million downloads in less than 6 months, and is poised to be a major new entrant in the social media landscape. He also served as Chief Operating Officer for Gama Aviation, leading a massive growth phase for the company that saw them emerge as the largest Part 135 operator in the US, which was ultimately acquired by Wheels Up in 2020. For the Ford Motor Company, Fell led the transformation of all IT operations across more than 40 international facilities while overseeing a team of over 400 personnel. He built the entire global IT organization for Terex Corporation from the ground up, providing the technology infrastructure and process to support the global enterprise.

“I am very honored and excited to be joining the JETNET family,” said Greg Fell. “I know how vital the company is to our team, our industry, and the community we call home. There are significant expectations to fill and I greatly look forward to hitting the ground running. As a user of JETNET’s services over the years, I know the organization is of the highest quality and believe the company is in a position to see substantial growth across many markets. ”

Fell is a certified Six Sigma Champion and serves on multiple boards and advisory councils across various industries including telecommunications, aviation, and oil & gas. He also practices as an Industrial Mentor for Columbia University.

“My main focus as an Industrial Mentor is getting others to succeed,” Fell continued. “I carry that over across all avenues of life including the teams I work with. We’re in this together. I am eager to get started helping my esteemed colleagues at JETNET find a new level of success within themselves and as a company.”

“Greg’s robust experience in raising capital, extraordinary business development skills, and talent for leading both small and large businesses will help JETNET usher in a new era of growth,” said Michele Husnay, General Manager, who oversees all business operations. “We are taking JETNET to the next level and he has the background to do this. We are excited to have such an accomplished leader join the JETNET team.”

**FAQs**

**Q:** Why has JETNET made this move?<br>
**A:** We believe now is the appropriate time to invest in our future and make JETNET the premier provider of information and intelligence for business and commercial aviation.

**Q:** Does this represent a change in JETNET’s ownership?<br>
**A:** Ownership of JETNET shall remain with the Esposito family, with Tony Esposito working closely with the CEO.

**Q:** Will there be dramatic changes in the services JETNET provides?<br>
**A:** JETNET will continue to deliver the same comprehensive and reliable service it has built its reputation on. Additionally, and immediately, JETNET will embark on a strategic plan to increase its research capabilities, harness the latest in information technology, and develop a BI (Business Intelligence) platform unparalleled in the industry.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 112,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For more information on JETNET and our exclusive services, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 ext. 254 (USA) or paul@jetnet.com; Lead Sales Contacts: Americas & Asia Pacific: Derek Jones, at 800.553.8638 ext. 603 or derek@jetnet.com; EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at 41.0.43.243.7056 ext. 854 or karim@jetnet.com.

[Download the full release (PDF)](/assets/uploads/news/JETNET CEO Announcement PR.pdf)
