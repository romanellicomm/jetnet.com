---
title: 2021 JETNET iQ Summit Adds Three Industry-Leading Speakers
meta_description: "Joining fellow experts at the podium will be Éric Martel, Bombardier; Brian Kirkdoffer, Clay Lacy Aviation; and Maria Della Posta, Pratt & Whitney Canada."
opengraph:
  title: Industry-Leading Speaker Additions
  description: Learn More Now
  image: /assets/opengraph/iQ_July_1200x628_FB.jpg
twitter_card:
  title:
  description:
  image:
---

UTICA, NY - The JETNET iQ Summit brings business aviation’s top leaders together to discuss
ideas that will guide the future of the industry. The 10th JETNET iQ Summit will be held
September 15-16th, 2021 at TWA Hotel at JFK Airport. JETNET, the leading provider of
corporate aviation information, has announced additional industry leaders to their 2021 JETNET
iQ Global Business Aviation Summit speaker line-up. Joining fellow experts at the podium will
be Éric Martel, President and CEO, Bombardier; Brian Kirkdoffer, President and CEO, Clay
Lacy Aviation; and Maria Della Posta, President, Pratt & Whitney Canada.
READMORE

<img src="/assets/images/summit/EricMartel_HR.jpg" class="float-right d-block image-border mt-2 mb-2 ml-4" width="240">

“We’re very excited to add Éric, Brian, and Maria to the stage,” stated Rolland Vincent, JETNET
iQ Creator and Director. “Their knowledge, insights, and ideas are key to helping drive our
industry forward, especially in this new post-pandemic era. We are all in for a treat.”

From 2002 until 2015, Martel held positions of increasing responsibility within Bombardier,
including President of Bombardier Business Aircraft, President of Bombardier Aerospace
Services, Vice-President and General Manager of the Global and Challenger platforms, and he
also worked at Bombardier Transportation from 2002 to 2004 as Vice President Operations for
North America. Prior to joining Bombardier, Mr. Martel worked for various other high-profile
multinational companies, such as Pratt & Whitney, Rolls Royce, Procter & Gamble, and Kraft
Foods.

<img src="/assets/images/summit/BrianKirkdoffer_HR.jpg" class="float-left d-block image-border mt-2 mb-2 mr-4" width="240">

Brian Kirkdoffer was fortunate to have the founder of Clay Lacy as his flight instructor. In 1990,
Brian joined Clay Lacy Aviation as a pilot and business development executive. As a captain
who flew to over 130 countries, his list of elite passengers included former U.S. presidents, a
British prime minister, international business leaders, and major figures from the sport and
entertainment worlds. In 2003, he was named president and later acquired the company in
2012. His leadership has guided Clay Lacy Aviation’s national expansion to nearly 800 team
members, a fleet of managed aircraft valued in excess of $2 billion, and over 35 U.S. operating
locations, including multiple full-service FBOs and FAA-certified MRO facilities. Brian has served
on several OEM advisory boards, held aviation industry leadership positions, and is currently an
active member of the Young Presidents Organization (YPO). He is a graduate of the University
of Washington’s Foster School of Business.

<img src="/assets/images/summit/MariaDellaPosta_HR.jpg" class="float-right d-block image-border mt-2 mb-2 ml-4" width="240">

Della Posta joined Pratt & Whitney in 1985 and progressed through roles of increasing
leadership in Supply Chain, Finance, and Customer Service. Named Vice President, Customer
Support, in 2001, Senior Vice President, Sales and Marketing in 2010, and Senior Vice
President Pratt & Whitney Canada in 2012. Ms. Della Posta is recognized as a champion of
inclusion and leadership development both within Pratt & Whitney and the aviation industry. She
serves on a number of industry boards and associations, including AIAC, Aero Montreal, GAMA,
Centraide (United Way) of Greater Montreal, and McGill University Advisory Boards.

The three newest additions to the Summit podium represent a broad spectrum of leadership
from across the industry, including aircraft OEM, engine OEM, aircraft charter, management,
FBO, and MRO services.

The 10th Summit promises to be an intimate gathering of a Who’s Who of business aviation
discussing the present and future of the industry, with predictions, industry revelations, and
insights from global leaders. The fast-paced Summit will also allow attendees time to meet their
contemporaries, network, and develop topics outside of scheduled sessions.

Registration to attend is currently open, now through September 1st, 2021. Pricing for attending
the event is $1,295 USD. Registration can be found at <a href="/summit">jetnet.com/summit</a>.

The TWA Hotel at JFK Airport is a new and stylish, neo-retro location that honors the elegance
of air travel while inspiring new life and new ideas with its captivating use of space and light.
JETNET iQ’s summit theme is “We’re back in time...and in person.”

[Download the full release (PDF)](/assets/uploads/news/JETNET iQ Summit Speaker Line Up PR.pdf)
