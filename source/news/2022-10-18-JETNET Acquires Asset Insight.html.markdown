---
title: JETNET Acquires Asset Insight
meta_description: "Acquisition Accelerates Growth Through Best-in-Class Data and Market Intelligence Offerings for the Aviation Industry"
opengraph:
  title: "JETNET Acquires Asset Insight"
  description: "Know More"
  image: /assets/opengraph/AssetInsight-FB_TW.jpg
twitter_card:
  title: "JETNET Acquires Asset Insight"
  description: "Know More"
  image: /assets/opengraph/AssetInsight-FB_TW.jpg
---

UTICA, NY - [JETNET](https://www.jetnet.com/), a leading provider of aviation data and market intelligence,
today announced that it has agreed to acquire [Asset Insight, LLC](https://www.assetinsight.com/), one of the world’s most experienced aviation-focused valuation firms and creator of eValues, business aviation’s most advanced real-time valuation tool. The acquisition is the first of what the company anticipates will be several future acquisitions as JETNET expands its data-driven product offerings for the aviation industry and leverages a recent majority investment led by [Silversmith Capital Partners](https://www.silversmith.com/).

“We’ve had the opportunity to work with Asset Insight as a partner over the course of many years and know how strategic the company’s valuation and cost-of-ownership products are to our customer base,” said JETNET CEO Greg Fell. “Tony Kioussis is a highly-respected and talented expert in the aviation industry who we are excited to have join the JETNET team as we expand our product offerings and global footprint.”

JETNET offers the most comprehensive and reliable database of business and commercial aviation information to its global customer base. Working directly with operators, owners, and manufacturers, JETNET maintains accurate information on pricing, availability, and location of mission-critical aviation resources. Asset Insight provides a broad spectrum of aircraft valuation software and analytics tools to help inform purchasing decisions, financing costs, and total-cost-of-ownership data for its customers. Combined, the two companies will deliver best-in-class products that provide customers with up-to-date, easy-to-understand, actionable information they require for making business decisions.

“Asset Insight and JETNET share a vision to simplify and improve the overall aircraft ownership
experience for customers,” said Tony Kioussis, President & CEO of Asset Insight. “Decision-makers in the aviation industry use JETNET’s data and market intelligence as an essential part of their daily operations. We look forward to joining the JETNET team to help bolster its product offerings and provide even more value to our shared customer base.”

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit [jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com); Lead Sales Contact: Americas & Asia Pacific: Derek Jones, at 800.553.8638, or [derek@jetnet.com](mailto:derek@jetnet.com); EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at +41.0.43.243.7056 or [karim@jetnet.com](mailto:karim@jetnet.com).

**About Asset Insight**

As one of the world’s most experienced, aviation-focused valuation firms, Asset Insight has been involved in thousands of aircraft appraisals, from light single-engine to heavy airline transport aircraft, and has earned a reputation for quality service, responsiveness, and innovative use of technology. Asset Insight also provides aircraft maintenance evaluation and asset financial optimization services.

**About Silversmith Capital Partners**

Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $3.3 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, Appfire, DistroKid, impact.com, Iodine Software, LifeStance Health, PDFTron, and Webflow. For more information, including a full list of portfolio investments, please visit [www.silversmith.com](https://www.silversmith.com/) or follow the firm on [LinkedIn](https://www.linkedin.com/company/silversmith-capital-partners).

[Download the full release (PDF)](/assets/uploads/news/JETNET Acquires Asset Insight PR.pdf)
