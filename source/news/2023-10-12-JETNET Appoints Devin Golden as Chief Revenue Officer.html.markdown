---
title: JETNET Appoints Devin Golden as Chief Revenue Officer
meta_description: "JETNET is thrilled to announce the appointment of Devin Golden as the company’s new Chief Revenue Officer (CRO)."
opengraph:
  title: "JETNET Appoints Devin Golden as CRO"
  description: "Know More"
  image: /assets/opengraph/JN_DGolden_FB.jpg
twitter_card:
  title: "JETNET Appoints Devin Golden as CRO"
  description: "Know More"
  image: /assets/opengraph/JN_DGolden_TW.jpg

---
UTICA, NY - JETNET, a global leader in aviation market data and intelligence, is thrilled to announce the appointment of Devin Golden as the company’s new Chief Revenue Officer (CRO). Golden will play a key part in advancing JETNET’s revenue growth, supporting its expansion efforts, and optimizing sales activities. This strategic appointment underscores JETNET’s commitment to driving innovation and customer success in the aviation industry.

Derek Swaim, CEO of JETNET, expressed his enthusiasm for the company’s newest addition, saying, "I am excited to welcome Devin Golden to JETNET as our Chief Revenue Officer. His extensive experience and proven track record in data, research, and software sales leadership roles make him a valuable addition to our executive team. Devin’s expertise will be instrumental in driving our revenue growth and enhancing the customer experience."

Golden brings over 25 years of sales experience to his role at JETNET. His impressive career includes seven years as Senior Vice President of Sales at Dun & Bradstreet, a global leader in business information and research. Dun & Bradstreet’s vast global database, with information on more than 240 million companies, highlights Golden’s ability to navigate complex data landscapes and drive business growth. In addition to his tenure at Dun & Bradstreet, Golden spent four years in sales leadership roles at Forrester Research, a leading global research and advisory firm. 

As JETNET’s CRO, Golden is excited about working alongside JETNET’s sales leadership team including VP of Sales Paul Cardarelli, AVP of Strategic Solutions Jason Lorraine, and Managing Director of Global Sales Karim Derbala. Golden stated, "I look forward to working closely with JETNET’s talented team and our dedicated sales representatives. Together, we will optimize our sales activities and ensure our customers receive the best possible service."

Golden’s role extends beyond revenue growth, as he will also work closely with JETNET’s Customer Success Team to enhance the overall customer experience.

Devin Golden’s appointment as Chief Revenue Officer marks an exciting chapter in JETNET’s journey. With Golden’s leadership and expertise, JETNET is poised for continued growth and innovation in the aviation market intelligence sector.

**About JETNET**

As a leading provider of market research and data for the global aviation market, JETNET delivers comprehensive and reliable insights to customers worldwide. JETNET is the ultimate source of fixed wing and helicopter fleet information, including transaction data, market intelligence, flight data and analytics, and industry forecasts. Headquartered in Utica, NY, JETNET offers comprehensive, user-friendly aircraft and flight data via APIs and real-time web applications.

For information on JETNET and our exclusive services, visit www.jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com), or Karim Derbala, JETNET Managing Director of Global Sales at +41 (0) 43.243.7056 (Switzerland) or [karim@jetnet.com](mailto:karim@jetnet.com).

**Media Contact:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET_Devin_Golden_PR.pdf)
