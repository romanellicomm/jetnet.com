---
title: JETNET iQ Wraps Summit, Looks Ahead to NBAA-BACE
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, wrapped up its 7th annual JETNET iQ
Summit recently, and attendees graded it as an enormous success. With presentations and panel appearances
from industry luminaries like NBAA’s Ed Bolen, GAMA’s Pet Bunce, NARA’s Brian Proctor, and NAFA’s Ford von
Weise, among many others, attendees were treated to one of aviation’s most concentrated discussions of the
current market and future direction of aviation. Serious networking was among the most prized facets of the event.
JETNET iQ leaders also presented their prescient survey results and aircraft market predictions, giving attendees
an insightful review of the state of business aviation today and forecast for the next ten years. More information is
available at JETNETiQ.com.

The company’s popular JETNET iQ (Independent, Quarterly) research team conducts a detailed and insightful
survey each quarter with 500 business aircraft owners and operators around the globe. By the close of 2017,
almost 14,000 responses will have been collected since surveys began in 2011—likely the most comprehensive
on-going study of business aircraft owner/operator behavior and sentiment ever conducted. The surveys include
questions on market sentiment, aircraft purchase and selling intentions, aircraft/engine/FBO brand quality and
awareness, flight utilization patterns, and expectations for the next 12-24 months, amongst many other factors.

Their next stop: NBAA-BACE 2017.

Once again, members of the JETNET iQ team will host their annual JETNET iQ State of the Market Briefing press
conference highlighting the latest results from their business aviation forecast surveys. The press conference will
be held in Press Room N116 at the National Business Aviation Association Business Aviation Convention &
Exhibition (NBAA-BACE) at the Las Vegas Convention Center, at 11:00 am on Tuesday, October 10th. The 45-
minute event will provide an update on the latest JETNET iQ industry analyses and forecasts, an overview of the
latest insights from JETNET iQ Global Business Aviation Surveys, and JETNET’s analyses of market dynamics
and trends.

JETNET will present their full range of products and services, including live demonstrations, at the National
Business Aviation Association Business Aviation Convention & Exhibition. This year’s NBAA-BACE will be held
October 10th-12th, and JETNET will be in booth #C12235.

[Download the full release (PDF)](/assets/uploads/news/JETNETiQ_Summit_press_release.pdf)
