---
title: JETNET Demonstrating New Aircraft Activity Features at 2018 NBAA Schedulers & Dispatchers Conference
opengraph:
  title: "New Features Demos"
  description: "Stay ahead of your competitors"
  image: /assets/opengraph/2018/Feb2_FB.jpg
twitter_card:
  title: "New Features Demos"
  description: "Stay ahead of your competitors"
  image: /assets/opengraph/2018/Feb2_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, is announcing several major updates and new capabilities at this year’s NBAA Schedulers & Dispatchers Conference Booth #1961, February 6-9 at the Long Beach Convention Center in Long Beach, California. JETNET already provides key contacts, including owner/operator, chief pilot, directors of maintenance, and the serial-number-specific equipment details for business jets, turboprops, pistons, and helicopters world wide. Subscribers can now take advantage of a new Aircraft Flight Activity/Utilization Program to prospect for new customers, and better serve existing customers. JETNET is now providing North American and Greater European aircraft activity details by owner/operator, serial number, and fleet, with the industry’s first Fuel/Tech Stop finder tool. The Aircraft Activity/Utilization Program tools also provide top airport pairs, and top serial number/makes/models/manufacturers by route. The latest capabilities will be available for live demonstration at the conference.

READMORE

Tony Esposito, Vice President of JETNET, reports his company has doubled down on supporting business aviation’s service sectors: “Over the past year, we have committed significant resources, technology, and customer input to upgrade our airport and business aircraft flight reporting capabilities. Our goal is to become the premier single-source solution, combining the most detailed aircraft owner/operator database with the best flight activity reporting capabilities available in the industry.”
“Our next-generation Aircraft Flight Activity/Utilization Program includes prospecting tools designed with FBO and airport operations service providers in mind,” said Paul Cardarelli, JETNET Vice President of Sales. “You can easily generate lists to target decision-makers for your fuel and/or ground services.”

JETNET users can take advantage of access to:

* Aviation activity by serial number, owner/operators, airports, and fleets across North America and Europe
* Comprehensive flight activity reports by serial number, owner/operator, airports and fleets from 2005-2018
* Serial-number-specific refuel opportunities across North America and Europe with the Aircraft Fuel/Tech Stop Tool
* A single-source solution to manage client/prospect aircraft activity information across North America and Europe
* Aircraft activity tools and reports to identify and make contact with key prospects worldwide
* Flight activity patterns by serial number, owner/operator, airports, and fleets across North America and Europe
* Aircraft flight activity totals by OEM (makes/models) across North America and Europe
* Top individual destinations and most-visited airport pairs by serial number, owner/operator, airports, and fleets across North America and Europe
* One-click aggregate flight activity details for operators with multiple locations across North America and Europe

“The new Fuel/Tech Stop tool is a game changer for our customers,” said Jason Lorraine, JETNET Senior Tech/Sales Specialist. “This feature allows them to identify where aircraft make Fuel/Tech stops across North America and Europe, and is huge time saver in generating prospecting lists for fuel providers.” JETNET reports that this feature, in combination with the other Aircraft Flight Activity tools, have generated a significant increase in product demonstration requests and sales well ahead of projected marketing benchmarks.

JETNET invites attendees of the NBAA Schedulers & Dispatchers Conference to stop by their booth #1961 for a live demonstration of JETNET products that showcase the new Aircraft Flight Activity/Utilization features.

For 30 years, JETNET has delivered the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of some 100,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time Internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET Schedulers Dispatchers Conference.pdf)
