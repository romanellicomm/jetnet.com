---
title: JETNET Attending 2017 HAI Heli-Expo
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will exhibit at the 2017 HAI
Heli-Expo in Dallas, Texas. JETNET will present their full range of software products and services at this
year’s Heli-Expo, including demonstrations of their new operator/airport utilization feature. JETNET’s
exhibit will run March 7th-9th, and JETNET will be in Hall E booth 8556.

“Helicopter market professionals have unprecedented intelligence on the market and individual aircraft,”
said Tony Esposito, JETNET Vice President. “They can now make decisions faster, with more
confidence, and more profitably than ever before.”

JETNET’s comprehensive helicopter database contains more than 31,600 in-service airframes
worldwide, and information on transactions for more than 49,000 retail helicopter sales over the last two
decades, along with 14 makes and 143 models of both turbine and piston airframes. Subscribers also
have access to detailed individual aircraft specifications, and a new operator/airport utilization feature that
provides information on more than 15,000 flights registered over the last 24 months.

JETNET researchers capture, on average, 500 major database “event" changes per day, and capture
and record 900-1,000 retail aircraft transactions per month. They work daily with dealers and brokers
worldwide to maintain records, spec sheets, documents, and photos for about 6,500 aircraft for sale.

JETNET employs a full-time staff of approximately 50 research specialists who contact industry
professionals around the world each business day. They make real-time updates to the most
comprehensive database of its kind.

“We have really been advancing the leading edge of intelligence in the helicopter market,” said Paul
Cardarelli, JETNET Vice President of Sales. “All the data we gather directly is checked for accuracy and
completeness, so our subscribers around the globe have a distinct advantage in working with timely,
authentic information.”

The company will be offering demonstrations of all their software, including the new operator/airport
utilization feature.

“The new insights will change the way our clients do business,” added Esposito. “They’ll be opening their
competitive advantage gap with this information.”
JETNET has extended an invitation to any Heli-Expo attendees to learn more about their products and
services in Hall E booth 8556.

[Download the full release (PDF)](/assets/uploads/news/JETNET_HeliExpo_press_release.pdf)
