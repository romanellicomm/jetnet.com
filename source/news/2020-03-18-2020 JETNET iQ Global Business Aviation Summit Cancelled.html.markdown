---
title: 2020 JETNET iQ Global Business Aviation Summit Cancelled
meta_description: 
opengraph:
  title: "iQ Summit Cancelled"
  description: "Looking forward"
  image: /assets/opengraph/SummitCanceled_FB.jpg
twitter_card:
  title: "iQ Summit Cancelled"
  description: "Looking forward"
  image: /assets/opengraph/SummitCanceled_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, today announced the decision to cancel the 2020 JETNET iQ Global Business Aviation Summit due to concerns related to the evolving coronavirus (COVID-19) pandemic. The event was scheduled for June 8-9, 2020 in White Plains, New York.

READMORE

JETNET has closely followed the spread of COVID-19, as well as announcements from international and U.S. domestic governmental and medical authorities regarding restrictions on travel and large gatherings. Serious concerns and restrictions have grown, prompting the need to cancel this year’s event.

Rolland Vincent, JETNET iQ Creator/Director, said, “This decision has been a difficult one to make, but for the safety of our attendees, clients, and staff, it is our only choice. We will keep our partners and clients up to date on future developments as they unfold.”

Refunds will be provided via the iQ Summit 2020 event ticketing service.

**About JETNET iQ**

Summits are part of JETNET iQ, an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET LLC and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy.

**About JETNET LLC**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET%20Cancels%202020%20iQ%20Summit.pdf)
