---
title: JETNET Releases New Capabilities at NBAA-BACE 2019
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---

UTICA, NY – JETNET LLC, the world leader in business aviation market intelligence, will be using <a href="https://nbaa.org/events/2019-business-aviation-convention-exhibition">NBAA 2019</a> to launch a series of new products and upgrades at NBAA-BACE 2019 in Las Vegas, Nevada, including upgrades for Values, their new Fleet Portfolio Manager, an update to their Flight Activity tools, JETNET API, their new Aircraft Prospector tool, and a new Aircraft Acquisition View. JETNET will be demonstrating their full suite of software products and services at the National Business Aviation Association Business Aviation Convention & Exhibition (NBAA-BACE), October 22-24, 2019, at booth #C12435.

READMORE

**JETNET Values and eValues™**

The latest upgrade to <a href="/products/jetnet-values.html">Values</a> includes projected residual values for a wide range of business aircraft, including current market values and 60-month estimated residual values. With these additions, JETNET Values is the only valuation tool to provide a comprehensive look at markets from all perspectives. Subscribers also have access to JETNET’s worldwide aircraft database, along with integration of historical asking price trends, recent sale prices, and sale price trends for target markets, estimated values (eValues™, developed in collaboration with Asset Insight, LLC) generated automatically for most markets. The add-on provides actual reported sold prices, as well as tools for better analysis of pricing data.

**New Fleet Portfolio Manager**

JETNET’s new Fleet Portfolio Manager allows subscribers to organize, review, and analyze even the most complex aircraft fleets. They can build a fleet based on their needs, and review it based on a wide variety of perspectives, such as age of fleet, value of fleet, maintenance analytics, flight activity, owners, operations, models, history, and more, as well as review existing corporate fleets.

**Flight Activity Report**

JETNET’S Marketplace and AERODEX Elite products give users the flexibility to organize and analyze individual aircraft, fleet, or operator/company historical flight activity by arrivals and/or departures for selected airports worldwide. Clients can now create detailed reports that include top origin and destination airports, countries, and continents; top operators by business type, country, and continent; top aircraft by serial number with seat capacity for crew/passengers; the number of flights per month; and fuel/tech stops.

**JETNET API: Customizable Data Delivery Solutions**

<a href="/products/jetnet-api.html">JETNET API</a> (Application Programming Interface) is a flexible application program interface that allows clients to build their own complex custom solutions powered by JETNET’s data. Users can also explore and design proprietary programs able to connect with and manage the data delivered via the firm’s RESTful API, including CRMs, market analysis tools, transaction trends, aircraft inventory programs, fleet management activities, or other complex applications.

**New Aircraft Prospector Tool**

<a href="/products/evolution-marketplace.html">JETNET Evolution Marketplace</a> clients can now use the company’s comprehensive historical transaction database to target companies most likely to purchase a given make/model of aircraft. Saving hours of research, the new tool correlates the make/model specific length of ownership data against the length of time for the current owner, as well as identifying companies which have sold aircraft and not bought another, companies with expiring leases or fractions, and more.

**New Aircraft Acquisition View**

The majority of JETNET’s dealer and sales professional tools have been centered around supporting professionals in selling aircraft. Many of the firm’s customers play dual roles as brokers for selling aircraft and brokers for buying aircraft. JETNET’s new Aircraft Acquisition View is designed to provide Buyers’ Brokers with better, easier-to-use tools for effectively specifying buyer needs and then identifying aircraft that may meet those needs. A user need only identify the type of aircraft, passengers, distance, features desired, and maintenance requirements, and then launch.

**Annual State of the Market Briefing**

As they have for past NBAA-BACE conferences, <a href="/products/jetnet-iq/">JETNET iQ<a/> will be holding their annual State of the Market Briefing on Tuesday, October 22, in Room N119, from 10:45am-11:30am, hosted by Rolland Vincent, Creator/Director of JETNET iQ, and Paul Cardarelli, JETNET Vice President of Sales. JETNET will provide details on market conditions, the latest forecasts on fleet and new aircraft deliveries of business fixed wing turbines, and proprietary insights from JETNET iQ’s Global Business Aviation Surveys of business aircraft owner/operator opinions. The largest on-going research effort of its kind, JETNET iQ Reports include insights from more than 17,000 survey respondents in 133 countries worldwide. JETNET iQ has also just announced the dates for their 10th Anniversary JETNET iQ Summit for June 8-9, 2020. The Summit will convene business aviation professionals from across the industry to share and expand their understanding of the market.

JETNET invites business aviation professionals to their NBAA-BACE booth #C12435 to ask them about, and see a demonstration of, their latest software and services.

As the leading provider of business aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of business aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 112,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/2019-JN-New-Capabilities.pdf)
