---
title: JETNET to Showcase Training Courses and Cutting-Edge Research Products at EBACE 2023
meta_description: "Designed to provide the highest level of service and analytical solutions in business aviation, featured products will include MARKETPLACE, AERODEX, and Values."
opengraph:
  title: "JETNET to Showcase Training Courses and Cutting-Edge Research Products at EBACE 2023"
  description: "Know More"
  image: /assets/opengraph/
twitter_card:
  title: "JETNET to Showcase Training Courses and Cutting-Edge Research Products at EBACE 2023"
  description: "Know More"
  image: /assets/opengraph/
---

UTICA, NY - [JETNET](https://www.jetnet.com/), the world leader in aviation market intelligence, will display their innovative training courses and their research products at the European Business Aviation Convention & Exhibition (EBACE), at booth #M71, May 23–25, at the Palexpo in Geneva, Switzerland. Designed to provide the highest level of service and analytical solutions in business aviation, featured products will include MARKETPLACE, AERODEX, and Values. JETNET will also highlight their free training sessions, created to help subscribers get the most out of their subscription.

“JETNET is consistently providing critical tools and data needed to thrive in today’s European aviation market,” stated Derek Swaim, JETNET’s CEO. “We are excited to showcase our evolving training services and research products to business aviation professionals who can utilize our data to surpass their customers’ and prospects’ expectations.”

**Training Courses**

JETNET’s training courses offer an array of dynamic sessions, helping participants learn how to best utilize their subscriptions to stay ahead on business aviation trends and research. Training can be customized for individuals and teams, and is free to annual subscribers.

**MARKETPLACE**

JETNET’s MARKETPLACE captures current and historical information on business aircraft on an encyclopedic scale. Covering the worldwide fleets of jets, turboprops, pistons, helicopters, and commercial transport aircraft,the database provides key info such as owner and operator information, transaction history, flight activity, aircraft valuation data, and more.

**AERODEX**

JETNET’s AERODEX helps business aviation professionals find comprehensive information and solutions to their daily challenges. Covering the worldwide fleets of jets, turboprops, pistons, helicopters, and commercial transport aircraft, users can find data points at the serial number and fleet level. Users also have the ability to find aircraft maintained under the European Union Aviation Safety Agency Part 145.

**Values**

JETNET Values is a unique, single-source asset valuation platform that combines key historical market data and reported sold prices with access to the current market values and 60-month estimated residual value figures. Users can effectively analyze individual or multiple aircraft simultaneously.

“JETNET’s state-of-the-art tools and training courses offer the ultimate solution for fulfilling both immediate and future data needs for business aviation professionals across Europe and the world”, says Swaim. “These vital instruments aid them in making well-informed tactical, strategic, and profitable decisions.”

EBACE attendees will be able to see live demonstrations of JETNET’s MARKETPLACE, AERODEX, and Values tools, as well as learn in-depth knowledge on JETNET’s training courses, at booth #M71.

**JETNET iQ State of the Market Briefing**

JETNET will also be sharing results from their JETNET Q1 2023 Survey, as well as details of their latest 10-year business jet delivery forecast during a press conference on Tuesday, May 23, from 11:00-11:45 a.m. in Room U of the Palexpo. The most recent trends and market sentiment from aircraft owners/operators will be highlighted during this exclusive presentation.

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit [www.jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or paul@jetnet.com.

**About Silversmith Capital Partners**

Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $3.3 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, Appfire, DistroKid, impact.com, Iodine Software, LifeStance Health, PDFTron, and Webflow. For more information, including a full list of portfolio investments, visit [www.silversmith.com](http://www.silversmith.com) or follow the firm on [LinkedIn](https://www.linkedin.com/company/silversmith-capital-partners/).

**Media Contact:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET_EBACE_PR.pdf)
