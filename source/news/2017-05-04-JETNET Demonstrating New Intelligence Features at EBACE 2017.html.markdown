---
title: JETNET Demonstrating New Intelligence Features at EBACE 2017
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will demonstrate a series of new
releases and features in their aircraft research service lineup at EBACE2017 at the Geneva Palexpo in Geneva,
Switzerland. Highlights this year include: Aerodex Elite, a new product that combines owner/operator data with the
FAA’s Traffic Flow Management System in near real time; a mobile implementation of JETNET Aerodex; a new, indepth
intelligence feature dubbed INTEL, including a new Utilization option; JETNET Values, with Reported Sold
Prices Program, previewed last year and now is setting new standards for aircraft resale reporting; their YachtSpot
product line; and JETNET’s admission into the European Business Aviation Association (EBAA). JETNET’s full
suite of services will be available for review at the European Business Aviation Convention & Exhibition, May 22nd-
24th, at booth # X129.

“This year, we’ve put our resources behind developing new services, while enhancing our existing products,” said
Tony Esposito, JETNET Vice President. “Our clients have been amazed at the depth of intelligence and level of
detail the new features give them. We’re keeping them two steps ahead of their competition.”

## Aerodex Elite

Aerodex Elite is a new JETNET product that combines a strong owner/operator database with a new application
to manage the FAA’s Traffic Flow Management System (TFMS) in near real time. The product includes live
access to current and historical utilization reports at the aircraft serial number level, for customers and prospects
on the ground and in the air. Subscribers can investigate and understand the activities of aircraft within the FAA
airspace, and the airports and companies that charter, manage, operate, refuel, and service these assets. Clients
also receive detailed current and historical analysis and summaries by individual aircraft, or fleets across
operator(s), airport(s), and management companies, with fuel uplift details and more.

## Aerodex Mobile

JETNET’s popular Aerodex service has now been implemented as a mobile platform and made available to
subscribers of Aerodex Elite and standard Aerodex. This mobile platform version makes vital data available on a
handheld device. The service is a version of Aerodex and Aerodex Elite tailored specifically for FBOs, MROs, and
other service providers who need accurate and timely information on aircraft owners and operators worldwide.
Clients now have access to the full capability through any mobile device.

## JETNET INTEL

INTEL (Intelligence) is JETNET’s new shortcut tool, providing one-click access to comprehensive single-page
summaries about utilization, and extensive portfolio data on each aircraft, company, operator, airport, and more.
Available to all Evolution Marketplace and Aerodex Elite customers, INTEL maximizes search effectiveness,
especially for LIVE subscribers. INTEL pulls together and presents data accurately, easily, and with customized
exporting capabilities.

When a company details page is displayed, an additional INTEL menu will appear for companies that have
additional intelligence to be displayed. When an aircraft details page is displayed, an additional INTEL menu may
appear for aircraft that have additional intelligence to be displayed. A Utilization option on the INTEL menu will
summarize utilization for an airport, aircraft, company, or operator. Intel is already allowing customers to see how
Utilization views can better inform their understanding of aircraft workloads by individual airframe, or by fleet.

## JETNET Values

Subscriptions to JETNET Values, announced just last year, have grown rapidly, with wide acceptance of the
program across the aviation industry. A single-source solution designed by JETNET, Values provides reported
sold prices, combined with data assessment tools that aviation professionals can use to make informed decisions
relating to finance, insurance, valuation, appraisals, and more.

Values is offered as an optional add-on component to the JETNET Marketplace program, and includes a
database of reported aircraft sales prices with tools to predict and summarize trends. New sales prices are
voluntarily provided to JETNET daily by reputable sources from among its network of subscribers. JETNET
reports that Values is now populated with more than 2,600 sales prices for more than 225 different aircraft
models—most of which pertain to transactions having occurred within the last 18 months—from more than 275
independent sources. The firm has also built a host of tools in its Marketplace Manager customer relationship
manager (CRM) for those users to maintain their own proprietary database of sales prices alongside those
gathered by JETNET. They have also announced a new incentive program through which new subscribers to
JETNET Values in 2017 will receive complimentary 12-month use of Marketplace Manager at no additional
charge.

“Values was launched in response to what we detected as desperation of our customers to better understand the
current and unprecedented trend in aircraft residual values,” said Paul Cardarelli, JETNET Vice President of
Sales. “We know this is a sensitive area—that the resale markets generally hold such intelligence in a very
guarded manner—and we respect that. But reliable information is what we are tasked to provide our customers,
and we believe that ultimately it makes for a market that performs in optimal fashion. One year after the launch of
Values, we are gratified to have received a high level of interest and cooperation for this initiative.”

## YachtSpot

The YachtSpot database continues to penetrate the worldwide luxury fleet, expanding to nearly 7,000 vessels at
or greater than 24 meters. Of increasing importance to both the yacht and aircraft industries, YachtSpot is the
only database providing detailed information about those individuals and businesses with relationships to both on
a worldwide basis. Within one platform, OEMs, dealers and brokers, and shipyards can examine the YachtSpot
ranking of top performers in yacht and aircraft resale and charter. Rapidly becoming the most in-depth collection
of fleet information by hundreds of metrics, YachtSpot brings clarity to the assets with key contacts about resale
or charter opportunities, classification, flagged countries, amenities, pedigree, and more. YachtSpot is the only
database designed to introduce professionals from aviation and yacht industries to each other, and to
opportunities for collaboration in resale, charter, referrals, insurance, finance, and more.

## EBAA Membership

In February JETNET was admitted as a member of the European Business Aviation Association (EBAA). They
were also selected to participate as a member of EBAA’s Associate Member Council (AMAC), specifically, the
subcommittee for Aircraft Sales and Acquisitions.

“We are delighted to join EBAA and to have the opportunity it affords us to shape the future of business aviation in
Europe,” said Karim Derbala, JETNET Managing Director of Global Sales. “JETNET has long regarded Europe as
a major player in our industry. Today the continent represents 10% of the fixed wing—and 22% of the rotary
wing—global fleet, generating nearly E$20B in annual revenue, and employing some 164,000 people. Business
aviation is a true economic powerhouse here, and EBAA is its true advocate.”

## JETNET Reach

Among JETNET’s clientele are leading manufacturers, suppliers, and government and advocacy groups for
business and commercial aviation worldwide. JETNET’s database of in-operation aircraft exceeds 100,000
airframes, which includes business jets, business turboprops, commercial airliners (both jets and turboprops), and
helicopters (both turbine and piston).

“2017 marks JETNET's sixteenth consecutive appearance at EBACE,” added Tony Esposito. “This has always
been an excellent venue for us, one where we have learned much about the highly important European
component of the global business aviation market. And while the last eight years have been a challenge for us all,
today there is an undeniable optimism for the future. It’s evident in North America—and even more so throughout
Europe—so we are delighted to return to Geneva with so many wonderful new tools to better serve this great
industry.”

The JETNET database includes comprehensive details on aircraft airframes, engines, avionics, and cabin
amenities, as well as aircraft owners and operators, lessors and lessees, fractional owners, and a host of other
entities associated with aircraft. Also included are transaction histories on aircraft dating back more than 25 years,
monthly aircraft market summary reports, and a variety of other tools for assessing the global fleet and aircraft
marketplace.

JETNET invites attendees to see all of their new product and service offerings at this year’s EBACE, May 22nd-
24th, at booth # X129. Additionally, on Monday, May 22nd at 11am in Salle R, they will conduct their annual
JETNET iQ European State of the Business Jet Market briefing. All business aviation professionals and press are
invited to attend.

[Download the full release (PDF)](/assets/uploads/news/JETNET_EBACE_2017_pressrelease.pdf)
