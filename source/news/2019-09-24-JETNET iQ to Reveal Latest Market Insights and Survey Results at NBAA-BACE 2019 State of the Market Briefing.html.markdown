---
title: JETNET iQ to Reveal Latest Market Insights and Survey Results at NBAA-BACE 2019 State of the Market Briefing
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will present a briefing by JETNET iQ on the state of the business aviation market at NBAA-BACE 2019 in Las Vegas, Nevada. JETNET iQ Creator/Director Rolland Vincent and JETNET Vice President of Sales Paul Cardarelli will conduct their annual JETNET iQ State of the Market briefing on Tuesday, October 22, from 10:45-11:30am in Room N119 of the Las Vegas Convention Center.

READMORE

JETNET iQ, the result of a collaboration between JETNET and aviation consultancy Rolland Vincent Associates, provides independent quarterly intelligence for the business aviation industry, and includes owner/operator survey results, economic and industry analyses, and forecasts. At the core of JETNET iQ is a detailed quarterly survey of more than 500 business aircraft owners and operators worldwide. Responses from more than 18,000 owners/operators in 132 countries have been collected since surveys began in 2011, representing the largest commercially available database of its kind in the industry.

The NBAA 2019 briefing will include an overview of JETNET iQ’s latest insights, including results from the recently completed Q3 2019 Global Business Aviation Survey. Attendees will receive an update on economic and industry developments, new aircraft orders and backlogs, pre-owned aircraft sales and inventory trends, and aircraft owner/operator sentiment from JETNET iQ’s research. Highlights from JETNET iQ’s latest 10- year business aircraft delivery and fleet forecasts will also be shared.

“Staying apprised of the mood of the market, and interpreting and amplifying the voices of business aircraft owners and operators, are at the core of JETNET iQ,” said Rolland Vincent. “Key metrics that we monitor, including customer sentiment, have been in flux since NBAA-BACE 2018, and we look forward to updating attendees on how we believe these changes affect the industry’s current situation and outlook.”

“These are transformational times for business aviation,” Paul Cardarelli added. “Today the perceived value proposition of a business aircraft is being redefined. OEMs, MROs and the like must look to new markets and appeal to them with a fresh message. Perhaps never before has our industry been so in need of intelligence on the sentiment of the aircraft owner/operator. This is exactly the mission of JETNET iQ.”

JETNET invites Market Briefing attendees to see all of their new product and service offerings at booth #C12435.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 112,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-NBAA-BACE-MarketBriefing.pdf)
