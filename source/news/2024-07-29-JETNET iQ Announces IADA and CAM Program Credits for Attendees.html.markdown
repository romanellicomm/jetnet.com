---
title: JETNET iQ Announces IADA and CAM Program Credits for Attendees
meta_description: "Qualified members of IADA’s Certified Broker program and NBAA’s Certified Aviation Manager (CAM) program will be eligible for credits, including those seeking initial certification or recertification."
opengraph:
  title: "Earn Continuing Education Credits"
  description: "Buy Tickets"
  image: /assets/opengraph/JN_CECredit_FB.jpg
twitter_card:
  title: "Earn Continuing Education Credits"
  description: "Buy Tickets"
  image: /assets/opengraph/JN_CECredit_TW.jpg
---

UTICA, NY – JETNET, the leading provider of corporate aviation information, announces a partnership with the International Aircraft Dealers Association (IADA) and the National Business Aviation Association (NBAA) to offer continuing education (CE) credits to qualified members attending the 2024 JETNET iQ Global Business Aviation Summit. The Summit takes place September 24–25 at the New York Marriott Marquis in Times Square.

“We’re excited to partner with IADA and NBAA to provide their members with this valuable opportunity,” states Rolland Vincent, JETNET iQ Creator and Director. “This intimate and influential aviation event allows attendees to earn CE credits while gaining insights from industry leaders.”

Qualified members of IADA’s Certified Broker program and NBAA’s Certified Aviation Manager (CAM) program will be eligible for credits, including those seeking initial certification or recertification.

“IADA continually seeks innovative methods to offer valuable learning opportunities for Certified Brokers fulfilling annual CE requirements,” said Erika Ingle, IADA’s Managing Director. “The JETNET iQ Summit offers members a prime opportunity to gain unique insights into critical industry topics, alongside networking with industry leaders in an intimate setting.”

“NBAA CAM members are expected to maintain a high standard of industry knowledge and expertise,” said Jo Damato, CAM, the National Business Aviation Association’s (NBAA) Senior Vice President of Education, Training & Workforce Development. “As innovators shaping the future of business aviation, the JETNET iQ Summit provides our CAM members the perfect opportunity to demonstrate their commitment to excellence while igniting inspiration and fostering collaboration with industry veterans and newcomers alike.”

To further incentivize participation, members will receive a special promo code via email from their respective organization to utilize during registration and benefit from this exclusive offer.

**About JETNET**<br>
Established in 1988, JETNET powers investment, growth, operations, and safety through the most comprehensive data and actionable intelligence in aviation worldwide. JETNET uses a combination of powerful data ingestion automation, proprietary algorithms, machine learning, and meticulous human curation to collect and organize data at the serial number level on over 170,000 airframes.

**About the JETNET iQ Global Business Aviation Summit**<br>
The JETNET iQ Global Business Aviation Summit is the premier industry gathering that brings together leaders and innovators from across the corporate aviation spectrum. The Summit features high-level discussions, networking opportunities, and insights into the latest trends, and challenges facing the industry.

For more information on JETNET iQ, visit jetnet.com or contact Rolland Vincent, Creator/Director of JETNET iQ, at 972-439-2069 or [rollie@jetnet.com](mailto:rollie@jetnet.com) or Paul Cardarelli, JETNET Vice President of Sales, at 315-797-4420 x254 or [paul@jetnet.com](mailto:paul@jetnet.com).

**Media Contact for JETNET:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET%20iQ%20IADA%20and%20CAM%20Program%20Credits%20PR.pdf)
