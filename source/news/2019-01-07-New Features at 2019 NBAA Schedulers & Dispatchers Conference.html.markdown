---
title: JETNET Demonstrating New Features at 2019 NBAA Schedulers & Dispatchers Conference
meta_description: 
opengraph:
  title: "Visit Us in San Antonio"
  description: "At SDC2019"
  image: /assets/opengraph/Jan18_FB.jpg
twitter_card:
  title: "Visit Us in San Antonio"
  description: "At SDC2019"
  image: /assets/opengraph/Jan18_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, is demonstrating new features and capabilities at this year’s NBAA Schedulers & Dispatchers Conference at Booth #321 from January 29-February 1 at the Henry B. Gonzalez Convention Center in San Antonio, TX. JETNET is helping top-tier equipment providers, fuel and ground service providers, resale and finance companies, insurance and refit specialists, and maintenance shops find and analyze historical flight activity to fit their individual missions. Understanding how clients, prospects, and fleets are being utilized is a complex task that JETNET says their new software helps manage at multiple levels simultaneously by providing new ways to use flight data.

READMORE

Mike Foye, JETNET Director of Marketing, says JETNET specializes in helping aviation professionals figure out how to use historical flight data to their advantage. “We’re all about converting flight activity into serial number level contacts, including the owners/operators, chief pilots, directors of maintenance, and more. We solve questions like how to figure out the key contacts behind the aircraft you want to service, refuel, manage, insure, finance, overhaul, and paint. We do it using our flight activity & route analysis.”

The applications also help companies run date range and airport-specific flight activity searches to find aircraft they want to refuel, and/or provide ground services like concierge food catering, transportation and lodging. There is no limit on the number of historical reports users can run. They can search, receive, and repeat, exploring flight activity by airframe, airport, fleet, operator, and fuel/tech stop, and compare flight activity by specific date ranges across targeted airports and special events.

“Our clients stay current with key aviation changes,” said Paul Cardarelli, JETNET Vice President of Sales. “Our research center, on average, adds 1,200 new registration numbers, 1,100 bases, and 600 operators to our worldwide aircraft database each month.” Subscribers can use JETNET flight activity to search serial-number-specific historical time on ground to identify fuel/tech, repositioning activities, and customs stops. “If you feel like it’s too much work trying to match flight activity with the key owner/operator aircraft contacts, we remove that frustration and difficulty.”

If mission distance of aircraft is a key search priority when analyzing flight activity, JETNET’s flight activity and route analysis include customizable range/distance searches. Applications allow users to capture and easily manage flight activity by fleet, operator, clients, or prospects, with JETNET’s Fleet Portfolio Manager, providing a way to see changes and trends in the early stages that create opportunities or to prevent problems.

“Say your manager sends you an Excel report with the 1,000 flights arriving and departing TEB, and wants you to start closing some new business. Does it include verified chief pilots, owners, operators, management company contact details?” asks Jason Lorraine, JETNET Senior Tech/Sales Specialist. “Our logical and consistent research and tools let you customize, to align the key data you want.”

Tony Esposito, Vice President of JETNET adds, “We are constantly improving and adding to the functionality of our airport and business aircraft flight reporting and route tracking capabilities. Combined with our detailed aircraft owner/operator database, we’re giving our clients an unparalleled set of tools to compete in the marketplace.”

The latest capabilities will be available for a live demonstration at the conference. JETNET invites attendees of the NBAA Schedulers & Dispatchers Conference to stop by their booth #321 for a live demonstration of JETNET products that showcase the new features.

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-Schedulers-Dispatchers-Conference.pdf)
