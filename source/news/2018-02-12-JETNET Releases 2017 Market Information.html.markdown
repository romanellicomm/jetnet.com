---
title: JETNET Releases December 2017 and the Year 2017 Pre-Owned Business Jet, Business Turboprop, Helicopter, and Commercial Airliner Market Information
---

UTICA, NY – JETNET LLC, the leading provider of aviation market information, has released December 2017 and 2017 year-end results for the pre-owned business jet, business turboprop, helicopter, and commercial airliner markets.

READMORE

[Download the full release (PDF)](/assets/uploads/news/JETNET December Market Information.pdf)