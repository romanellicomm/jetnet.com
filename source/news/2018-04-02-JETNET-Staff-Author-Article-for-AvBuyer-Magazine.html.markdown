---
title: The Latest on ADS-B &#58; JETNET Staff Author AvBuyer Magazine Article
---

Are you prepared? JETNET has been researching ADS-B equipped aircraft for the past 2 years, and our staff is giving you the latest in this article appearing in AvBuyer’s April 2018 edition. This latest “JETNET >> Know More” article is co-authored by Mike Chase, JETNET Director of Special Projects and President of Chase and Associates, and Mike Foye, JETNET Director of Marketing, and sources a number of JETNET staff.

READMORE


[Download the article here (PDF)](/assets/uploads/news/JETNET_KnowMore_ADS-B_Status.pdf)

