---
title: JETNET iQ PULSE Launched
meta_description: 
opengraph:
  title: ""
  description: ""
  image:
twitter_card:
  title: ""
  description: ""
  image:
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, announced the launch of <a href="https://www.jetnet.com/iq-pulse/">JETNET iQ PULSE</a>, a regular bulletin on the state of the business aviation market, and the latest version of its JETNET iQ portfolio of aviation market research, strategy, and forecasting services.

READMORE

Rolland Vincent, JETNET iQ Creator/Director, said, “This is an unprecedented time for business aviation. Postponed trips. Canceled conferences and conventions. Face-to-face meetings–remember those? Business aviation leaders have not lost their insatiable appetite for market and competitive intelligence, and today is absolutely not the time to be flying blind. With JETNET iQ PULSE, we are pleased to provide readers with insights they need to safely and intelligently navigate the skies ahead.”

Paul Cardarelli, JETNET Vice President of Sales, added: “Our research team is second to none at gathering voices of customers across the world. With JETNET iQ PULSE, we focus on key “need to know” developments in this rapidly evolving environment. The best thing about JETNET iQ PULSE: it’s FREE.” 

**About JETNET iQ**

JETNET iQ is an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ’s proprietary survey database now includes almost 19,000 business aircraft owner/operator respondents from 130+ countries. JETNET iQ is a collaboration between JETNET LLC and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy. 

**About JETNET LLC**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. 

[Download the full release (PDF)](/assets/uploads/news/JETNET%20iQ%20PULSE%20Launched.pdf)
