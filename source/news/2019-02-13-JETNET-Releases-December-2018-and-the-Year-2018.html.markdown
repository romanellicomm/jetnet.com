---
title: JETNET Releases December 2018 and the Year 2018 Pre-Owned Business Jet, Business Turboprop, Piston, Helicopter, and Commercial Airliner Market Information
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/Feb_FB.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/Feb_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of aviation market information, has released December 2018 and 2018 year-end results for the pre-owned business jet, business turboprop, piston, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in **Table** A are key worldwide trends across all pre-owned aircraft market sectors, comparing
December 2018 to December 2017, as well as 2018 year-end, and comparisons to 2017.

**Most market sectors are showing lower inventory for sale in the comparison of 2018 to 2017, except for Piston Helicopters. Also, there were fewer full-sale transactions in 2018 compared to 2017, except for Business Jets and Fixed Wing Pistons, where retail transactions were up 2.1% and 19%, respectively.**

The Fleet For-Sale percentages for all market sectors, except for Piston Helicopters and Commercial Turboprops, were lower in the December comparisons, with Business Jets and Piston down the most (.9 and .8 percentage points, respectively).

Across all market sectors, JETNET is reporting 9,198 full retail sale transactions for 2018. In 2017, this number was 10,111, a decrease of 917, or 9% fewer transactions. Interestingly, Commercial Jet Airliners transactions decreased by 710, or 28.9%, in 2018 compared to 2017. Business Jets were 2,809 transactions in 2018, and when combined with Commercial Airliners at 1,748 transactions, together they accounted for 50% of the total of 9,198 transactions recorded in 2018.

<img src="/assets/uploads/images/Table-A-2-13.png" class="img-fluid mx-auto d-block" alt="Table A">

All aircraft segments were taking less time to sell (23 days) in 2018 compared to 2017, except for turbine helicopters which took an additional 58 days before being sold.

As the business jet market has finally broken below the 10% threshold of inventory for sale, a period of transition is now in play, wherein the pendulum swings in favor from the buyer to the seller. Today the market of available aircraft continues to shrink, and still many models exhibit the soft pricing brought on by the diminishment of residual values that so completely dominated the post-recession years.

**Chart A** illustrates that the “For Sale” inventory of business jets has decreased steadily from a high point in July 2009 (2,938) to 1,974 jets in December 2018. That’s a reduction in the percentage of the in-service fleet from 17.7% in July 2009 to 9.0% now.

This is a positive sign, as the inventory “For Sale” has dropped, albeit slowly. Of course, we would hope that the reduced “For Sale” inventory continues to drop in 2019, which could lead to prices becoming firmer.

<img src="/assets/uploads/images/Chart-A-2-13.png" class="img-fluid mx-auto d-block" alt="Chart A">

Indeed, most aircraft business jet dealers and brokers today would tell you that the pristine used jets that were on the market a few years ago have become more challenging to locate. Refer to **Table A**. The sage advice for buyers is to act NOW. The council of “just wait a few months—the price will come down” may not present itself as we continue into the current seller’s market environment.

Further analysis of 2018 shows mixed full sale transaction results. Of the seven segments reported in **Table A**, two segments increased in numbers, comparing 2018 to 2017. The two segments were business jets (2.1%) and piston (19%). The remaining five segments reported decreases in transactions, with commercial airliner jets showing the largest drop (-28.9%) in 2018 vs. 2017. Note: JETNET does not cover all piston aircraft inventory or sales. The piston models that JETNET tracks are: Baron 58 series, Cessna 421 series, Diamond DA62, and the Piper high-end singles M350, Malibu, Matrix, and Mirage.

**Table B** shows the Pre-owned Business Jet Retail Transactions (Whole & Leases) comparing 2017 and 2018 by Age grouping. There were 58, or 2.1%, more Pre-owned Business Jets sold in 2018 compared to 2017.

Two observations are:
<ul>
	<li>The age group 0 to 10 years saw a drop of 128, or 16% fewer transactions. Note: Most brokers will
explain the 16% reduction of sales in 2018 as not so much about declining demand as it is about a
lack of aircraft to choose from for the discerning buyer.</li>
	<li>The age groups 0 to 10 (24%) and 11 to 20 (42%) accounted for 66% of the total of 2,809 pre-
owned transactions in 2018.</li>
</ul>

<img src="/assets/uploads/images/Table-B-2-13.png" class="img-fluid mx-auto d-block" alt="Table B">

**Chart B** displays the 12-Month moving average for full retail transactions from December 2011 to December 2018 for business jets.

From December 2011, used business jet transactions steadily increased until 2014, from 2,134 to 2,610. A leveling-off followed in 2015, and 2016 produced mixed activity (while remaining around the 2,600 line of transactions). Then an increase occurred to 2,720, followed by a drop to 2,514 transactions in December 2016. From this point on, the used business jet market segment has shown a sharp recovery through December 2018 (2,809).

<img src="/assets/uploads/images/Chart-B-2-13.png" class="img-fluid mx-auto d-block" alt="Chart B">

**Table C** shows the pre-owned transaction comparison of 2017 and 2018 by aircraft size. Four groups showed increases, with the “Mid-size” group showing the largest increase of 62, or 14.4%. Five groups showed decreases, with the “Large” group showing the largest decrease of 39, or 11.4%.

<img src="/assets/uploads/images/Table-C-2-13.png" class="img-fluid mx-auto d-block" alt="Table C">

**Summary**

The recovery in business aviation during the post-recession period has been with mixed results, with poor overall aircraft residual values that continue to be problematic. Now that 2019 is here, we hope the U.S. pre- owned market, along with improvements in the world economy, will continue to push more new aircraft purchases. As for now, the pre-owned market is in a seller’s market environment, with Pre-Owned For-Sale inventories running at 9.0%.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-December-Market-Info.pdf)
