---
title: JETNET iQ Presenting State of Business Jet Market Briefing at EBACE 2017
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will present a highly anticipated
briefing by JETNET iQ on the state of the business jet market at EBACE2017 at the Geneva Palexpo in Geneva,
Switzerland.

JETNET iQ is the company’s division for observation, commentary, and forecasts of the business aviation industry.
Launched in 2011, JETNET iQ is a partnership between JETNET LLC and the Rolland Vincent Associates aviation
consultancy of Plano, TX. The core mission of JETNET iQ is to measure the pulse of the business jet owner and
operator community by way of surveying 500 of them every quarter. At the close of 2016, JETNET iQ exceeded
12,000 owners and operators surveyed, the most comprehensive ongoing study of aircraft operator sentiment ever
conducted. Customers for JETNET iQ include airframe and power plant OEMs, Tier 1 suppliers, aircraft dealers
and brokerages, FBOs, MROs, and aerospace investment analysts.

At EBACE 2017, Rolland Vincent and JETNET Vice President of Sales Paul Cardarelli will conduct a JETNET iQ
State of the Market briefing, free to attendees, on Monday, May 22nd from 11:00am to 11:45am in Salle R.

“We are delighted to return to EBACE with the latest insights into the state of the market, both in Europe and
globally,” said Rolland Vincent, JETNET iQ Creator. “We look forward to sharing the latest insights from our Q2
2017 JETNET iQ Survey of business aircraft owners and operators.”

Paul Cardarelli added, “Europe is a vital market for business aviation activity—from aerospace design and
manufacturing to aircraft sales and service, maintenance, training, and flight operations. JETNET’s diligent
research team helps keep us and our customers informed in this very dynamic marketplace.”

Each year, JETNET iQ holds a summit in New York City featuring spirited presentations and dialog from industry
leaders. For 2017, the JETNET iQ Global Business Aviation Summit will be held September 5-6 at The Westin
New York at Times Square. For further details, please refer to the JETNET iQ Summit website.

JETNET invites EBACE attendees to see all of their new product and service offerings from May 22nd-24th, at
booth # X129.

[Download the full release (PDF)](/assets/uploads/news/JETNETiQ_EBACE_MarketBriefing_pressrelease.pdf)
