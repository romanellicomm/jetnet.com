---
title: JETNET Releases September 2018 and First Nine Months of 2018 Pre-Owned Business Jet, Turboprop, Piston, Helicopter, and Commercial Airliner Market Information
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has released September 2018 and the first nine months of 2018 results for the pre-owned business jet, turboprop, piston, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in Table A below are key worldwide trends across all aircraft market segments, comparing September 2018 to September 2017. For the first nine months of 2018 there have been 6,781 pre-owned jets and helicopters sold. This is a decrease of 681, or 9.1% fewer sale transactions compared to 2017 across all market sectors. Commercial airliners and commercial turboprops showed the largest declines at 16.1% and 13.1%, respectively.

“Fleet For Sale” percentages for all market sectors, except for Piston Helicopters, were down in the September comparisons, dropping 7.5% or nearly 500 aircraft overall. Business Jets showed the largest decline in the percentages For Sale compared to the other markets. Business Jets For Sale percentages are at 8.9% compared to 10.4% last year at this same time, a drop of 1.5 percentage points.

The decline in business jets for sale is very good news, and clearly shows we are below the 10% mark and firmly in a seller’s market. Inventories of pre-owned business jets for sale have decreased and are now below the 2,000 mark as well, at 1,934.

Business jets are showing a decrease (1.6%) in pre-owned sale transactions (including leases) in the first nine months of 2018 compared to the same period in 2017. However, business jets are taking 27 fewer days to sell, 285 days on average, compared to 312 days last year. Business turboprops average days on the market dropped by 27 days compared to last year as well, while sale transactions declined by 0.9%.

Business piston aircraft are being reported for the first time in Table A. Two observations are noteworthy. The percentage for sale is over 10%, and YTD full sale transactions are up 28.7% (the only market sector to show an increase) compared to the 2017 YTD numbers. Average days on the market for piston aircraft saw a decline of 82 days compared to last year.

Turbine helicopters showed a decrease of 6.3% in YTD sales transactions, whereas piston helicopters saw a double-digit decline in YTD sale transactions at 12%.

**Table A**

<img src="/assets/uploads/images/2018-11-14-Table-A-Key-Worldwide-Trends.jpg" class="img-fluid mx-auto d-block" alt="Table A">

Commercial Airliners are reported by JETNET in Table A and include the numbers for sale for both commercial jets (including airliners converted to VIP) and commercial turboprops. Commercial Jet YTD sale transactions at 1,351 are trailing Business Jets at 1,975 sale transactions. However, Pre-owned Business Jet, Pre-owned Commercial Jet, and Turbine Helicopter market sectors have surpassed the 1,000 mark for YTD sales. These combine to 65% of the total compared to the other market segments. Commercial Jet Full Sale Transactions are down 16.1% in the YTD comparisons.

**12-month Moving Average Retail Sale Transactions Analysis**

Charts A, B, and C focus on pre-owned business jet 12-month moving average retail sale transactions from January 2011 to September 2018. Chart A reports shows that, since December 2016 (lowest point in the past 22 months), the pre-owned business jet market segment saw a very rapid recovery to June 2017. However, the past 3 months have shown a decline of 2.6% as we head into the final quarter of 2018.

**Chart A - Business Jets Market**

<img src="/assets/uploads/images/2018-11-14-Chart-A-PreOwnedBizJets.jpg" class="img-fluid mx-auto d-block" alt="Chart A">

**Chart B – Turbine Helicopter Market**

<img src="/assets/uploads/images/2018-11-14-Chart-B-PreOwnedTurbineHelis.jpg" class="img-fluid mx-auto d-block" alt="Chart B">

Chart B shows that, since January 2017 (lowest point in the past 21 months), the pre-owned turbine helicopter market segment saw a very rapid recovery to April 2018. However, the past 5 months have shown a decline of 8.5% as we head into the final quarter of 2018.

Chart C shows that, since November 2014 (lowest point in the past 46 months), the pre-owned commercial jet airliner market segment saw a steady recovery to February 2018. However, the past 7 months have shown a decline of 14.4% as we head into the final quarter of 2018.

**Chart C – Commercial Jet Airliner Market**

<img src="/assets/uploads/images/2018-11-14-Chart-C-PreOwnedCommAirliners.jpg" class="img-fluid mx-auto d-block" alt="Chart C">

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.


[Download the full release (PDF)](/assets/uploads/news/JETNET-Sept-2018 Market-Info-press-release.pdf)
