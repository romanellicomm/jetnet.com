---
title: Live FAA Aircraft Data Key To JETNET’s New Airport Uplift Views
---

UTICA, NY – JETNET LLC, the leading provider of business aircraft information, is now receiving live aircraft tracking
data from the FAA. This direct data feed allows JETNET to upgrade the JETNET Aerodex product to better support
FBOs, MROs, and flight line service providers. Users of the JETNET Aerodex service will be the first to see FAA
registered flights with activity from England, Ireland, Canada, United States, Mexico, and the Caribbean. JETNET will
be demonstrating this and other new features of its Evolution Aerodex service at this year’s NBAA Schedulers &
Dispatchers Conference, in booth #1132, from February 7-10 at the Fort Worth Convention Center in Texas.

The new Aerodex Airport Uplift search capabilities will focus on aircraft flight patterns, fuel consumption, and
enhanced predictors for key maintenance and inspection events at the serial number level.

“Our capabilities at JETNET to collect, compute, and make flight activity usable, is what makes the Airport Uplift
feature so valuable to our customers,” said Mike Foye, JETNET Director of Marketing. “Our Aerodex Uplift program
gives our subscribers the detailed fuel consumption and travel patterns at the serial number level they need to
understand real opportunities. Regardless of your area of service expertise, from providing fuel and flight support, to
concierge level food or transportation, Aerodex easily converts what happens at the airports every day into a
prospect list you can work with confidence.”

JETNET, located in booth #1132, will be demonstrating the Aerodex Airport Uplift feature, highlighting:

- Top 500 Operators by total uplift
- Top 500 Airport Pairs (origins with destinations)
- Top 500 Originations
- Top 500 Destinations

“Targeted for FBOs, MROs, and airport-based service providers,” added Foye, “our newest web-based version of
Aerodex provides the best answers, updated on a live, weekly, or monthly basis to maintain and expand your
business.”

Aerodex automatically tracks and updates 21,674 business jets, 15,045 turboprops, and 5,207 piston airframes
worldwide. Aerodex, known for decades for providing instantaneous registration and serial number ownership
answers—even while the aircraft is literally rolling down the flight line—is now creating reports tied to flight and uplift
activity at the airport level. Airport Activity Views help subscribers prospect, and learn where aircraft are based, with
detailed owner and operator contact information, along with the top origins and destinations of aircraft by serial
number and tail number. Users can generate reports to see aircraft based at nearby airports for prospecting, with
detailed flight activity reports and owner/operator contact details.

The Aerodex program features dozens of current and historical search capabilities—dating back to 1988—providing
data on aircraft, including ownership, company fleets, certifications, chief pilots, directors of maintenance, liens/lien
releases, and viewable scanned FAA documents. Users can choose from hundreds of search criteria, including
make, model, year of delivery, base of operation, airframe total times, engine makes and models, avionics on board,
and landing cycles, to customize lists of owner and operator prospects. The program allows highly targeted
prospecting lists, with regular updates from a staff of approximately 50 researchers.

JETNET invites attendees of the NBAA Schedulers & Dispatchers Conference to stop by their booth #1132 for a live
demonstration of Evolution Aerodex and the new features.

[Download the full release (PDF)](/assets/uploads/news/JETNET_NBAA_Schedulers_Dispatchers_Conference.pdf)
