---
title: JETNET to Showcase Advanced Tools at EBACE 2022
---

UTICA, NY - JETNET, the world leader in aviation market intelligence, will be highlighting their Marketplace and Flight Insights tools at the European Business Aviation Convention & Exhibition (EBACE), at booth #X112, May 23 - 25, at the Palexpo in Geneva, Switzerland. The two innovative products are designed to provide the highest level of service and analytical solutions in business aviation.

“JETNET is providing critical tools and data needed in today’s European aviation market,” stated Karim Derbala, JETNET’s Lead Sales Contact: EMEA (Europe, Middle East, Africa). “We are excited to showcase our Marketplace and Flight Insights services to business aviation leaders who can utilize our superior data to exceed their customers’ and prospects’ needs.”

**Marketplace**

JETNET’s Marketplace captures current and historical information on business aircraft on an encyclopedic scale. Covering the worldwide fleets of jets, turboprops, pistons, helicopters, and commercial transport aircraft, the database provides key data such as owner and operator information, transaction history, flight activity, aircraft valuation data, and more.

**Flight Insights**

JETNET’s Flight Insights tool is the industry’s premier source for serial number-specific flight activity data tied back to company and operator names. The customizable and mobile-friendly tool tracks over 9 million take-offs and landings over the last 12 months and allows custom APIs to be available at any time.

Greg Fell, JETNET’s CEO, said, “JETNET’s Marketplace and Flight Insights tools provide users the ultimate source of intelligence on worldwide fleets and market data. They are pivotal instruments used to overcome the daily, challenging data requirements of business aviation professionals.”

EBACE attendees will be able to see live demonstrations of JETNET’s Marketplace and Flight Insights tools at booth #X112.

**JETNET iQ State of the Market Briefing**

JETNET will also be sharing results from their JETNET iQ Q1 2022 Survey, as well as details of their latest 10-year business jet delivery forecast during a press conference on Monday, May 23, from 11:00-11:45 a.m. in Room S of the Palexpo. The most recent trends and market sentiment from aircraft owners/operators will be highlighted during this exclusive presentation.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 108,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET and our exclusive services, visit <a href="https://www.jetnet.com">jetnet.com</a> or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or <a href="mailto:paul@jetnet.com">paul@jetnet.com</a>; Derek Jones, Lead Sales Contact: Americas & Asia Pacific, at 800.553.8638, or <a href="mailto:derek@jetnet.com">derek@jetnet.com</a>; Karim Derbala, Lead Sales Contact: EMEA (Europe, Middle East, Africa), at +41.0.43.243.7056 or <a href="mailto:karim@jetnet.com">karim@jetnet.com</a>.

[Download the full release (PDF)](/assets/uploads/news/JETNET EBACE PR.pdf)
