---
title: JETNET Demonstrating New Features at 2020 HAI HELI-EXPO
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---


UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, will be demonstrating new features at the 2020 HAI HELI-EXPO in Anaheim, California. Subscribers can customize reports for client and internal use with JETNET’s popular Market Reports tool, letting them choose which parameters to display under their own logo and branding. JETNET will also be demonstrating their full suite of software products and services, including JETNET for Salesforce<sup>®</sup>, at this year’s HELI-EXPO. JETNET’s exhibit will run January 28th to January 30th in booth #3513.

READMORE

“Our new features will increase professionals’ productivity and improve their client decision-making,” said Mike Foye, JETNET Director of Marketing. “Nearly all of our improvements are based on client and industry input, so we are providing tools our users want and need.”

The Market Reports feature yields booklet-style customized outputs from JETNET’s intelligence software. Inspired by improved research and visual data presentation tools in JETNET’s products, the tools were designed with formal presentations in mind, and can be customized to include a client’s header and logo, along with selected data sets determined and configured by the user. With dozens of serial-number-specific and make/model fleet overviews, subscribers can customize data sets for a more sophisticated audience.

The Market Report format provides approximately 20 pages of model intelligence, including model specifications, fleet composition, market characteristics and insight, buying habits, and sales trends. The report function was designed with both novice and sophisticated subscribers in mind.

JETNET is actively researching 32,500 in-service helicopters worldwide, and their comprehensive database contains information on transactions for more than 59,200 retail helicopter sales over the last two decades, along with 12 makes and 146 models of both turbine and piston airframes. Subscribers also have access to detailed individual aircraft specifications with owner/operator contact information.

JETNET also recently launched JETNET for Salesforce on Salesforce AppExchange, empowering customers to access and import aircraft, companies and contacts from JETNET, including their simple step sync of chosen datasets. The new app allows JETNET and Salesforce clients to access and import JETNET company, contact, and aircraft data; create, update, and customize company, contacts, and aircraft records with a single click; maintain original—or create new—company relationships in aircraft records; navigate JETNET program links and records without leaving the Salesforce platform; and easily identify and sync data changes since your last visit.

JETNET employs a full-time staff of approximately 50 research specialists who contact industry professionals around the world each business day. They make real-time updates to the most comprehensive database of its kind.

The company will be offering demonstrations of all their software, including the Market Report feature. JETNET has invited HELI-EXPO attendees to learn more about their products and services in booth #3513.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-2020-HAI-HELI-EXPO.pdf)
