---
title: JETNET Releases June 2019 and First Six Months of 2019 Pre-Owned Business Jet, Business Turboprop, Helicopter, and Commercial Airliner Market Information
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---

UTICA, NY – JETNET LLC, the leading provider of aviation market information, has released June 2019 and the first six months of 2019 results for the pre-owned business jet, business turboprop, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in Table A below are key worldwide trends across all aircraft market segments, comparing June 2019 to June 2018. “Fleet For-Sale” percentages for all sectors were mixed in the June comparisons. June 2019 is on the rise for “For Sale” percentage (9.5%) for business jets.

**TRENDS: Except for Piston Helicopters, all aircraft sectors reported double-digit percentage decreases in full sale transactions in the first six months of 2019 versus 2018.**

For the first six months of 2019, Pre-owned Business Jets are showing a 21.5% decrease in pre-owned sale transactions, and are taking more time to sell (28 days) than last year. Business turboprops saw a 13.9% decrease in sale transactions, while taking 2 days more to sell than last year.

Interestingly, **New** Business Jet shipments as reported by GAMA increased by 12.5%, from 281 to 316, in the first six months of 2019 compared to 2018. However, **New** Business Turboprops declined by 11.2%, or 29, in the same comparative periods.

Comparing June 2019 to June 2018, turbine helicopters saw a double-digit decrease in YTD Sale Transactions, down 13.3%, while piston helicopters showed a decline of 2.6%.

Commercial airliners are also reported by JETNET in Table A – Worldwide Trends, and include the number for sale for both commercial jets (including airliners converted to VIP) and commercial turboprops. Commercial jets and commercial turboprops were down in full sale transactions, at 19.6% and 30.1% respectively, in the YTD June-over-June comparisons.

For the first six months of 2019 there were a total of 4,270 aircraft and helicopters sold, with business jets (1,122) and commercial jets (874) leading all types and accounting for 47% of the total. The number of retail sale transactions across all market sectors—at 4,270—decreased by 874, or 17%, compared to the first six months of 2018. **Note:** JETNET does not cover all piston aircraft inventory or sales. The piston models that JETNET tracks are: Baron 58 series, Cessna 421 series, Diamond DA62, and the Piper high-end singles M350, Malibu, Matrix, and Mirage.

**Table A**

<img src="/assets/uploads/images/2019-08-14-Table-A.png" class="img-fluid mx-auto d-block" alt="Table A">

**Global, USA, and Non-USA Pre-Owned Market Trends**

Highlighted in Table B are business jets and business turboprops, comparing the months of June from 2012 to 2019 (seven years) for the USA vs. Non-USA aircraft in operation, for sale, and percentage for sale.

The general trend has been that growth in business jets has out-paced turboprops in the last seven years. Since June 2012, 3,545 new business jets have joined the global fleet, as compared to 2,193 turboprops.

The number for sale and percentage for sale have declined since 2012. The split between USA vs. Non-USA for business jets in operation has remained at 60/40 levels, whereas the business turboprops split of in- operation USA vs. Non-USA is about 51/49. Interestingly, the number for sale in the USA vs. Non-USA is 61/39 for business jets and 58/42 for business turboprops. Currently, for sale business jets exceeded the 2,000 mark after a steady decline since 2016.

<img src="/assets/uploads/images/2019-08-14-Table-B.png" class="img-fluid mx-auto d-block" alt="Table B">

**Pre-Owned Business Jet Transactions**

Chart A displays the 12-month moving average for full retail transactions for business jets from December 2011 to June 2019.

From December 2011, used business jet transactions steadily increased until June 2016 to a high point of 2,725. A falling-off occurred in the second half of 2016 to a low point of 2,522 in December 2016, and has since steadily increased to 2,892 transactions in December 2018. However, starting in January 2019, the 12- month moving average for full retail transactions for business jets has declined to 2,584, or 21.5%.

<img src="/assets/uploads/images/2019-08-14-Chart-A.png" class="img-fluid mx-auto d-block" alt="Chart A">

This decline is only slightly above the low point of 2,522 full sale transactions recorded in December 2016. Accordingly, 2019 is off to a bad start for the pre-owned business jet market.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 112,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-June-2019-Market-Info.pdf)
