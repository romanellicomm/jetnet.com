---
title: JETNET Featuring New Reporting Tools at 2018 HAI Heli-Expo
opengraph:
  title: "Visit Us in Las Vegas"
  description: "See a demo at HAI HELI-EXPO 2018"
  image: /assets/opengraph/2018/Feb15_FB.jpg
twitter_card:
  title: "Visit Us in Las Vegas"
  description: "See a demo at HAI HELI-EXPO 2018"
  image: /assets/opengraph/2018/Feb15_TW.jpg
---

UTICA, NY – JETNET LLC, celebrating its 30th anniversary as the leading provider of corporate aviation information, will be featuring new reporting tools at the [2018 HAI Heli-Expo](http://heliexpo.rotor.org/) in Las Vegas, Nevada. The new “Market Reports” tool allows users to customize reports for clients and internal use by choosing which parameters to display, along with their own logos and branding. JETNET will also present their full range of software products and services at this year’s Heli-Expo. JETNET’s exhibit will run February 27th-March 1st in booth #C4134.

READMORE

“Professionals in the helicopter market will find our new Market Report tool invaluable in client decision-making,” said Mike Foye, JETNET Director of Marketing. “They can quickly generate a professional report booklet from intelligence in our Helicopter database, customized for each client and each request, in a more readable format.”

Market Reports are booklet-style customized outputs from JETNET’s popular intelligence software. Inspired by improved research and visual data presentation tools in JETNET’s products, the new tools were designed with formal presentations in mind, and can be customized to include a client’s header and logo, along with selected data sets determined and configured by the user. The upgraded Market Report function now includes the ability to compare three makes/models on the same range map, details key flight utilization information, and includes a dashboard detailing the Market Absorption Rate. With dozens of serial-number-specific and make/model fleet overviews, subscribers can now customize data sets for a more sophisticated audience.

The new Market Report format, which JETNET designed with input from their clients, provides approximately 20 pages of model intelligence, including model specifications, range maps, fleet composition, market characteristics and insight, buying habits, and sales trends. The report function was designed with both novice and sophisticated subscribers in mind.

JETNET’s comprehensive helicopter database contains nearly 32,000 in-service airframes worldwide, and information on transactions for more than 52,000 retail helicopter sales over the last two decades, along with 12 makes and 144 models of both turbine and piston airframes. Subscribers also have access to detailed individual aircraft specifications, and a new operator/airport utilization feature that provides information on more than 24,800 flights registered over the last 24 months.

JETNET employs a full-time staff of approximately 50 research specialists who contact industry professionals around the world each business day. They make real-time updates to the most comprehensive database of its kind.

The company will be offering demonstrations of all their software, including the new Market Report feature. JETNET has extended an invitation to any Heli-Expo attendees to learn more about their products and services in booth #C4134.

For 30 years, JETNET has delivered the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of some 100,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time Internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET_2018HeliExpo_press_release.pdf)
