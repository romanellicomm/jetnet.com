---
layout: 'news_dateless_layout'
title: The History of Learjet
meta_description: "Learjet has announced that they will conclude production. Here is a recap of Learjet’s history and how their brand has impacted the aviation industry."
exclude_from_blog_index: true
opengraph:
  title: "The History of Learjet"
  description: "Numbers Don’t Lie"
  image: /assets/opengraph/Learjet_FB.jpg
twitter_card:
  title: "The History of Learjet"
  description: "Numbers Don’t Lie"
  image: /assets/opengraph/Learjet_TW.jpg
---

## 2-4-2 Fox Trot

Like The Byrds’ timeless “Lear Jet” song, all good things must come to an end, a fact of life reflected in the recent announcement that Learjet production would be suspended by year’s end. When Bill Lear gazed upon the Swiss FFA P-16 jet fighter in the 1950s, a legendary aircraft lineage materialized. Did he imagine what it would become?

Learjet is a brand unto itself.  When I entered this industry 20+ years ago, every business aircraft was a Learjet in my eyes. Sleek, powerful, an iconic symbol of unrestrained speed. What is not to love? Although production will conclude, the Learjet brand is not going away anytime soon. Bombardier will continue to support these aircraft for many years to come. 

## Fleet and Deliveries

According to the latest JETNET databases, Learjet aircraft are based in 41 countries worldwide. Production to date includes 3,043 aircraft built across 6 different model classes, from the original Model 23 through the latest Model 75. More than 70% of Learjet aircraft ever delivered were in service in February 2021 – a significant fleet of 2,145 light through mid-size jets. About 59% of in-service Learjet aircraft are based in the United States; other leading country fleets include Mexico (11% of the world fleet), Brazil (5%), Argentina (4%), and Germany (4%). Learjet aircraft are based in most countries in the Americas, the European Union, and Southern Africa, and across parts of the Middle East and Asia Pacific.

In production since the early 1960s, the Learjet family of aircraft includes 6 distinct lines, from the 20-Series through the most recent 70-Series models. The brand’s heydays were in the late 1970s, led by the fleet leader Learjet 35A, and in and around the dot.com boom in the late 1990s / early 2000s, paced by the Learjet 31A, 45, and 60. A 3rd production surge occurred in 2006-2008 prior to the Global Financial Crisis, with the company offering up to 5 different Learjet models. Since 2014, deliveries have concentrated on the Learjet 75 as production ramped down from 2 to 1 aircraft per month on average.

<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image3.png">
<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image2.png">

## Utilization

Although missions for most Learjet average ~60-90 minutes, the 2,850-nm range Learjet 36/36A stand out as the models with the longest average sectors. Based on the last 2 years of flight operations data, Learjet 36/36A missions have averaged 145-150 minutes in duration, about 2/3rds longer than the in-production Learjet 75. As of late February 2021, more than 70% of the Learjet 36/36A fleet of 52 remaining aircraft have been in service for more than 40 years—a testament to Learjet engineering, robust design, reliability, and high-performance. 

At the height of the Covid-19 pandemic in April 2020, Learjet fleet utilization dropped by nearly 60% YoY, but has since recovered smartly based on the latest flight tracking data available to JETNET. In fact, as of January 2021, Learjet flight operations had recovered to within 7.7% of January 2020 levels. This compares favorably to super-midsize and larger business jet aircraft, for which flight operations were down ~14% YoY. The relative strength of light jet utilization – with Learjet models well represented amongst this fleet – is an encouraging sign for our industry. Needless to say, and despite the COVID-19 headwinds, Learjets continue to soar. 

A review of flight operations data reveals interesting and perhaps unexpected concentrations of Learjet flight activity. Over the past 24 months, 7 of the top 10 airports for Learjet arrivals / departures were outside the contiguous United States, with Alaska-based cargo and medevac operators amongst the fleet leaders. For those readers with an A&P license who are seeking job security and the endless outdoor adventures and hunting / fishing lifestyle, the land of plenty appears to be to the North. 

<table class="table">
  <thead>
    <tr>
      <th colspan="1" rowspan="1">
        #&nbsp;Flights
      </th>
      <th colspan="1" rowspan="1">
        Departure Airport
      </th>
      <th colspan="1" rowspan="1">
        Arrival Airport
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td  colspan="1" rowspan="1">
        851
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
      <td  colspan="1" rowspan="1">
        FairbanksFAI / PAFA
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        803
      </td>
      <td  colspan="1" rowspan="1">
        Bethel BET / PABE
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        737
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
      <td  colspan="1" rowspan="1">
        Bethel BET / PABE
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        649
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
      <td  colspan="1" rowspan="1">
        Juneau JNU / PAJN
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        579
      </td>
      <td  colspan="1" rowspan="1">
        MiamiMIA / KMIA
      </td>
      <td  colspan="1" rowspan="1">
        Ft. Lauderdale Executive FXE / KFXE
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        516
      </td>
      <td  colspan="1" rowspan="1">
        FairbanksFAI / PAFA
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        503
      </td>
      <td  colspan="1" rowspan="1">
        Aeropuerto Internacional General Juan  Alvarez MMAA
      </td>
      <td  colspan="1" rowspan="1">
        Licenciado Adolfo Lopez Mateos MMTO
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        490
      </td>
      <td  colspan="1" rowspan="1">
        Juneau JNU / PAJN
      </td>
      <td  colspan="1" rowspan="1">
        Ted Stevens Anchorage ANC / PANC
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        487
      </td>
      <td  colspan="1" rowspan="1">
        Licenciado Adolfo Lopez MMTO
      </td>
      <td  colspan="1" rowspan="1">
        Aeropuerto Internacional General Juan N. Alvarez MMAA
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        457
      </td>
      <td  colspan="1" rowspan="1">
        Seattle Boeing Field BFI / KBFI
      </td>
      <td  colspan="1" rowspan="1">
        Juneau JNU / PAJN
      </td>
    </tr>
  </tbody>
</table>

<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image4.png">
<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image6.png">

## Aircraft Ownership

An analysis of JETNET aircraft records reveals some fascinating insights into Learjet ownership patterns. Based on a sample of newer models developed since Bombardier purchased the Learjet company in 1990, owners buying factory-new aircraft have tended to keep their aircraft for 71 months on average. This ownership experience is about 58% longer than that for buyers of pre-owned Learjet aircraft, a pattern that is repeated with other popular models in the light jet segment. With entry-in-service in late 2013, a significant number of Learjet 70/75 aircraft remain with their original owners, which accounts for the seemingly lower ownership duration of these two aircraft in the table below

A review of ownership history data in the JETNET database suggests that Learjet customers are brand loyal, consistently choosing to upgrade within the Learjet family to newer and often larger models.  Other key upgrade paths to Learjet models come from operators of a wide variety of light jets including the Citation I / II as well as King Air 100 / 200 aircraft.

<table class="table">
  <thead>
    <tr>
      <th  colspan="1" rowspan="1">
      </th>
      <th  colspan="1" rowspan="1">
      </th>
      <th  colspan="1" rowspan="1">
        New&nbsp;(months)
      </th>
      <th  colspan="1" rowspan="1">
        Pre-owned&nbsp;(months)
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        40
      </td>
      <td  colspan="1" rowspan="1">
        81.77
      </td>
      <td  colspan="1" rowspan="1">
        41.8
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        40XR
      </td>
      <td  colspan="1" rowspan="1">
        76.37
      </td>
      <td  colspan="1" rowspan="1">
        34.88
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        45
      </td>
      <td  colspan="1" rowspan="1">
        75.52
      </td>
      <td  colspan="1" rowspan="1">
        43.72
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        45XR
      </td>
      <td  colspan="1" rowspan="1">
        71.54
      </td>
      <td  colspan="1" rowspan="1">
        35.37
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        60
      </td>
      <td  colspan="1" rowspan="1">
        67.28
      </td>
      <td  colspan="1" rowspan="1">
        41.91
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        60XR
      </td>
      <td  colspan="1" rowspan="1">
        57.22
      </td>
      <td  colspan="1" rowspan="1">
        26.85
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        70
      </td>
      <td  colspan="1" rowspan="1">
        35.16
      </td>
      <td  colspan="1" rowspan="1">
        26.81
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        75
      </td>
      <td  colspan="1" rowspan="1">
        37.31
      </td>
      <td  colspan="1" rowspan="1">
        31.15
      </td>
    </tr>
    <tr>
      <td  colspan="2" rowspan="1">
        ALL LEARJET
      </td>
      <td  colspan="1" rowspan="1">
        70.83
      </td>
      <td  colspan="1" rowspan="1">
        45.22
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        EMBRAER
      </td>
      <td  colspan="1" rowspan="1">
        PHENOM 100
      </td>
      <td  colspan="1" rowspan="1">
        51.48
      </td>
      <td  colspan="1" rowspan="1">
        31.75
      </td>
    </tr>
    <tr>
      <td  colspan="1" rowspan="1">
        EMBRAER
      </td>
      <td  colspan="1" rowspan="1">
        PHENOM 300
      </td>
      <td  colspan="1" rowspan="1">
        45.94
      </td>
      <td  colspan="1" rowspan="1">
        27.28
      </td>
    </tr>
  </tbody>
</table>

## Pre-Owned Transactions

Approximately 25% of light jets transacted in the pre-owned business jet market over the past 10 years have been Learjet-branded aircraft. Interestingly, we can recognize no major ebbs and flows in Learjet pre-owned sales as we might see in other sectors of the light jet fleet.  While average asking prices for pre-owned Learjet light jet models has declined by $U.S. 1.58 Million over the past 10 years, the average age of those aircraft for sale has also increased by ~8 years which is equivalent to a 1995 vintage aircraft. Despite initial drops in residual value, the majority of in-service Learjet aircraft are now 15+ years old and have entered a relatively flat portion of the depreciation curve, with many years of productive operations ahead.

<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image5.png">

## Aircraft Retirements / Removals

As of late February 2021, ~30% of Learjet aircraft ever built have been retired or otherwise removed from service. Noteworthy amongst the inactive fleet are the large number of early-production models, particularly those built prior to 1975. JETNET records suggest that ~73% of 1970s-era Learjet aircraft have been taken out of service, and it would be safe to hypothesize that this generation will continue to be parked at an accelerating pace. Based on current utilization patterns and our assessment of Learjet production life cycles and in-service survivor curves, we expect that fleet retirements will rise again in ~10-15 years when another handful of higher delivery year aircraft will likely reach the end of their useful lives.

The era of Learjet production may be ending, but the value of a Learjet aircraft endures. Alas, I wonder how much Dodson International Aircraft Parts would sell a Lear 25 hull for? That would make for a cool Airbnb experience—or the chilliest tree-fort on the block. 

<img alt="" class="img-fluid my-4 block" src="/assets/uploads/news/history-of-learjet/image1.png">