---
title: JETNET Releases December 2016 and the Year 2016 Pre-Owned Business Jet, Business Turboprop, Helicopter, and Commercial Airliner Market Information
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has released December 2016 and 2016 year-end results for the pre-owned business jet, business turboprop, helicopter, and commercial airliner markets. 

**Market Summary**

Highlighted in Table A are key worldwide trends across all pre-owned aircraft market sectors, comparing December 2016 to December 2015, as well as 2016 year-end and comparisons to 2015. 

Most market sectors are showing lower inventory for sale, with fewer full-sale transactions in 2016 compared to 2015.

The Fleet For Sale percentages for all market sectors, except for Piston Helicopters, were lower in the December comparisons, with Business Jets and Business Turboprops down the most (.5 and .7 percentage points, respectively).

Across all market sectors, JETNET is reporting 8,278 Full Retail Sale Transactions, including leases, for 2016. Last year, this number was 8,900, a decrease of 622—or 7%—transactions. Business Jets were almost 2,500 transactions in 2016, and when combined with Commercial Airliners at 1,882 transactions, accounted for 52% of the total of 8,278 transactions recorded in 2016.

**Table A**

<img src="/assets/uploads/images/2017-02-07-Table_A_Worldwide_Trends.jpg" class="img-fluid mx-auto d-block" alt="Table A">

Business Jets and Business Turboprops are taking less time to sell (9 days and 22 days, respectively) than last year. However, there was a 7.3% decrease in average asking price, and also a 7.3% decrease in retail sale transactions for Business Jets. However, there was a .2% increase in retail sale transactions, with an increase in asking price of 0.9% for Business Turboprops.

**Pre-owned Business Jet Market “For Sale”**

Historically, 2009 was the peak year in terms of business jets “For Sale” (2,782 aircraft, or 16.3% of the fleet in December of that year). Since then, the lowest number of business jets “For Sale” was 2,202 (11% of the fleet) in 2014. The following year brought an increase (2,359, or 11.5% of the fleet “For Sale”), and 2016 a minimal decrease again (2,315, or 11% of the fleet “For Sale”).

As demonstrated by Chart A, the 11% fleet “For Sale” in 2016 falls within the pre-recession range of business jets “For Sale”, and is no longer regarded as being a high inventory level relative to the number of business jets in operation.

Having reviewed the market going back to 2005, the global business jet inventory has not dipped below 10%, which as a rule of thumb has traditionally separated buyer’s and seller’s markets based on supply and demand. (Of course, the 10% rule of thumb applies on a model-by-model level, too.)

Will we see a seller’s market (below 10%) in 2017? For that to occur, there would need to be a reduction of more than 200 business jets “For Sale”, or a similar reduction in the number of business jets in operation (which could be obtained by aircraft being retired).

**Chart A**

<img src="/assets/uploads/images/2017-02-07-Chart_A_PreOwned_Biz_Jets_ForSale.jpg" class="img-fluid mx-auto d-block" alt="Chart A">

**Pre-owned Business Jet Market Full Retail Sale Transactions, Including Leases**

2016 Full Retail Sale transactions, including leases (recorded as of January 30, 2017), dropped to 2,442, or -7.3%, ending six consecutive years of increases, as shown in Chart B. This is perhaps a sign that the pre-owned market is changing direction, with higher inventories for sale, and fewer sales. The drop is 193 fewer business jet transactions, compared to 2015. As noted previously, all market sectors showed fewer Full Retail Sale transactions, including leases, in 2016 compared to 2015, except for Business Turboprops, which increased slightly (by 0.2%).

**Chart B**

<img src="/assets/uploads/images/2017-02-07-Chart_B_PreOwned_Biz_Jets_FullRetail.jpg" class="img-fluid mx-auto d-block" alt="Chart B">

**Pre-owned Business Jet Market By Weight Class**

As shown in Table B, there were across-the-board reductions in all business jet transactions by weight class, totaling -193, or -7.3%, in 2016 compared to 2015. The Heavy weight class led, with 81 (8.9%) fewer transactions, followed by the Medium weight class, with 56 (8.2%) fewer transactions in 2016 compared to 2015. Also, the Light weight class had the largest number of transactions in 2016: 862, or 35% of the total of 2,442.

**Table B**

<img src="/assets/uploads/images/2017-02-07-Table_B_PreOwned_Biz_Jets_byWeightClass.jpg" class="img-fluid mx-auto d-block" alt="Table B">

**Summary**

The recovery in business aviation during the post-recession period has been underwhelming. Now that 2017 is here, we hope the U.S. Pre-Owned market, along with improvements in the world economy, will continue to push more new aircraft purchases in the new year. As for now, it continues to be a buyer’s market environment, with Pre-Owned For Sale inventories running at 11%.  

[Download the full release (PDF)](/assets/uploads/news/JETNET_2016_Year_End_Market_Info.pdf)
