---
title: JETNET CEO Greg Fell issues Call to Arms on Sustainability
meta_description: "JETNET CEO Greg Fell took the stage at the JETNET iQ Business Aviation Summit to issue a call to arms for the industry on sustainability."
opengraph:
  title: "Call To Arms On Sustainability"
  description: "Read More"
  image: /assets/opengraph/JN_GFell_CallToArms_FB.jpg
twitter_card:
  title: "Call To Arms On Sustainability"
  description: "Read More"
  image: /assets/opengraph/JN_GFell_CallToArms_TW.jpg
---

New York City, NY - JETNET CEO Greg Fell took the stage at the JETNET iQ Business Aviation Summit to issue a call to arms for the industry on sustainability. The remarks came at the end of the session called Sustainability: Getting to Our Bluer Skies. Greener Future., featuring Tim Obitts & Bryan Sherbacow of Alder Fuels, Kurt Edwards of International Business Aviation Council (IBAC), Puja Mahajan of Azzera, and Steve Cass of Gulfstream Aerospace.
READMORE

"We must be leaders in the adoption of sustainable aviation fuels and carbon offsets,” Fell stated. "If we wait, we will be disrupted by this issue. The 60-year-old executive flying in a business jet may not care. But the 40-year-old executive flying in a business jet does care. That difference is enough to disrupt our industry. Waiting is not a strategy.”

The theme of this year’s 11th annual iQ Summit is Bluer Skies. Greener Future. The event is the largest in its 11-year history and brings together the business and thought leadership of the business aviation industry.

"The drive to sustainability is real, and we are about to be disrupted by this issue,” Fell continued. "This disruption is going to happen. If you are a student of disruptions, you know how these things work. We can be leaders, or we can wait to be disrupted. I believe we should be leaders. We should be leading the charge into sustainable aviation fuel and carbon offsets. If we don't, someone or something else will disrupt our industry.”

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or paul@jetnet.com; Lead Sales Contact: Americas & Asia Pacific: Derek Jones, at 800.553.8638, or derek@jetnet.com; EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at +41.0.43.243.7056 or karim@jetnet.com.

[Download the full release (PDF)](/assets/uploads/news/GregFellIssuesCalltoArmsonSustainabilityPR.pdf)
