---
title: JETNET Appoints Derek Swaim as Chief Executive Officer
meta_description: "JETNET, a leading provider of aviation data and market intelligence, today announced the appointment of Derek Swaim as Chief Executive Officer."
opengraph:
  title: "JETNET Appoints Derek Swaim as CEO"
  description: "Know More"
  image: /assets/opengraph/DSwaim_CEO_FB.jpg
twitter_card:
  title: "JETNET Appoints Derek Swaim as CEO"
  description: "Know More"
  image: /assets/opengraph/DSwaim_CEO_TW.jpg
---

UTICA, NY - [JETNET](http://www.jetnet.com/), a leading provider of aviation data and market intelligence, today announced the appointment of Derek Swaim as Chief Executive Officer. Swaim has served as President and a member of the Board of Directors at JETNET since joining the company in August 2022. 

JETNET offers the most comprehensive and reliable database of business and commercial aviation information to its global customer base. Working directly with operators, owners, and manufacturers, JETNET maintains the most accurate information in the industry on pricing, availability, and location of mission-critical aviation resources. 

“I am honored to accept the position as Chief Executive Officer at JETNET,” said Swaim. “I have a deep appreciation for what it takes to scale a business like this and feel grateful to have an incredibly talented team who has built a foundation of products that thousands of customers rely on for their day-to-day operations. I look forward to leading the company in its next chapter as we continue to innovate, expand our product offerings, and acquire complementary businesses.”

Prior to joining JETNET, Swaim served as an Executive-in-Residence at [Silversmith Capital Partners](http://www.silversmith.com/) and previously served as Executive Vice President of Corporate Development at Validity, a Silversmith portfolio company, where he played an integral role as the SaaS platform increased revenue by over 10x. Prior to Validity, Swaim was a Managing Director at Aeris Partners, a leading M&A advisory firm to software, digital media, and business information companies. 

“Derek’s experience building and scaling software and data companies have been invaluable to the JETNET team since he joined as President,” said Jim Quagliaroli, Managing Partner of Silversmith. “As the company enters into a new phase of growth, we believe he is the ideal person to lead the company.”

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates. JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit [jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com); Lead Sales Contact: Americas & Asia Pacific: Derek Jones, at 800.553.8638, or [derek@jetnet.com](mailto:derek@jetnet.com); EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at +41.0.43.243.7056 or [karim@jetnet.com](mailto:karim@jetnet.com).

**About Silversmith Capital Partners**

Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $3.3 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, Appfire, DistroKid, impact.com, Iodine Software, LifeStance Health, Nordic Consulting, PDFTron, Upperline Health, and Webflow. For more information, including a full list of portfolio investments, please visit [www.silversmith.com](http://www.silversmith.com) or follow the firm on [LinkedIn](https://www.linkedin.com/company/silversmith-capital-partners).

[Download the full release (PDF)](/assets/uploads/news/JETNET_Derek_Swaim_CEO.pdf)
