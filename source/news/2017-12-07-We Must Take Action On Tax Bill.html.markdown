---
title: An Open Letter from JETNET to the Aviation Community
opengraph:
  url: https://www.jetnet.com/news/we-must-take-action-on-tax-bill.html
  title: "Let’s Act Together"
  description: "Here’s how you can help"
  image: /assets/opengraph/2017/Dec7_FB.jpg
twitter_card:
  title: "Let’s Act Together"
  description: "Here’s how you can help"
  image: /assets/opengraph/2017/Dec7_FB.jpg
---

**We Must Take Action on Tax Bill**

As aviation professionals, we wanted to bring to your attention an alarming
issue in the current Tax Bill currently in congress for reconciliation, and ask
you to join us in taking a few minutes to help address a potentially
catastrophic section in this legislation. We are happy about the “Bonus
Depreciation” section of the bill, providing the ability for a purchaser of a
new aircraft to take all of the depreciation as an expense in year one. This
will of course help.

However, the Senate version of the bill **removed the ability for aircraft
owners to do IRS Code 1031 tax deferred exchanges**. Please see the
[NBAA Associate Member Advisory Council notice](/assets/uploads/news/AMAC_Letter_12.3.17.pdf)
for details on this change. It is a clear and potentially far-ranging threat to
our industry, and we are encouraging you to join us in urging our legislators to
support our manufacturing base by continuing to offer this ability.

**Contact our Congressional Representatives**

Please contact GOP Congress members Reps. Kevin Brady of Texas, Devin Nunes of
California, Peter Roskam of Illinois, Diane Black of Tennessee, Kristi Noem of
South Dakota, Rob Bishop of Utah, Don Young of Alaska, Greg Walden of Oregon,
and John Shimkus of Illinois. They will be working on Conference Committee
Reconciliation of the Senate and House versions of the proposed Tax Bill. We
want to push them for more favorable language, and a positive outcome for our
industry. Changes are desperately needed to remove the offending language that
would absolutely hurt the sales of new and pre-owned business aircraft.

Let’s work together to get this done. Thanks for your assistance!

Business aviation link for taking action:
[https://action.noplanenogain.org/](https://action.noplanenogain.org/)

Senate contacts:
[https://www.senate.gov/general/contact_information/senators_cfm.cfm](https://www.senate.gov/general/contact_information/senators_cfm.cfm)

House contacts:
[https://www.house.gov/representatives/find-your-representative](https://www.house.gov/representatives/find-your-representative)

Regards,<br>The JETNET Team
