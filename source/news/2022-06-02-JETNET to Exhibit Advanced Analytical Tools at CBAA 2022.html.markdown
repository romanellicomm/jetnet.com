---
title: JETNET to Exhibit Advanced Analytical Tools at CBAA 2022
opengraph:
  title: ""
  description: "Learn More"
  image: /assets/opengraph/
twitter_card:
  title: ""
  description: "Learn More"
  image: /assets/opengraph/
---

UTICA, NY - JETNET, the world leader in aviation market intelligence, will be highlighting their Marketplace and Flight Insights tools at the Canadian Business Aviation Association Convention & Exhibition (CBAA), at booth #510, June 14–16, at the Skyservice Hangar in Toronto, Canada. The two innovative products are designed to provide the highest level of service and analytical solutions in business aviation.
READMORE

“JETNET equips business aviation professionals worldwide with tools to overcome the daily, challenging data requirements they face,” stated Paul Cardarelli, JETNET’s Vice President of Sales. “Our Marketplace and Flight Insights tools are pivotal instruments that will allow leaders in today’s Canadian aviation market to surpass their clients’ expectations and increase their business’ analytical capabilities.”

**Marketplace**

JETNET’s Marketplace captures current and historical information on business aircraft on an encyclopedic scale. Covering the worldwide fleets of jets, turboprops, pistons, helicopters, and commercial transport aircraft, the database provides key data such as owner and operator information, transaction history, flight activity, aircraft valuation data, and more.

**Flight Insights**

JETNET’s Flight Insights tool is the industry’s premier source for serial number-specific flight activity data tied back to company and operator names. The customizable and mobile-friendly tool tracks over 9 million take-offs and landings over the last 12 months and allows custom APIs to be available at any time.

Greg Fell, JETNET’s CEO, said, “JETNET’s Marketplace and Flight Insights tools are the ultimate source of intelligence on worldwide fleets and market data. We believe these next-generation analytical solutions will exceed the needs of business aviation’s leaders across North America entirely.”

CBAA attendees will be able to see live demonstrations of JETNET’s Marketplace and Flight Insights tools at booth #510.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 108,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET and our exclusive services, visit <a href="https://www.jetnet.com">jetnet.com</a> or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or <a href="mailto:paul@jetnet.com">paul@jetnet.com</a>; Derek Jones, Lead Sales Contact: Americas & Asia Pacific, at 800.553.8638, or <a href="mailto:derek@jetnet.com">derek@jetnet.com</a>; Karim Derbala, Lead Sales Contact: EMEA (Europe, Middle East, Africa), at +41.0.43.243.7056 or <a href="mailto:karim@jetnet.com">karim@jetnet.com</a>.

[Download the full release (PDF)](/assets/uploads/news/JETNET CBAA PR.pdf)
