---
title: JETNET Reviews Business Aircraft Markets Amidst COVID-19 Crisis
meta_description: 
opengraph:
  title: "Market Review Amid Global Crisis"
  description: "Read The Details"
  image: /assets/opengraph/COVIDReportJN_Sept_FB.jpg
twitter_card:
  title: "Market Review Amid Global Crisis"
  description: "Read The Details"
  image: /assets/opengraph/COVIDReportJN_Sept_TW.jpg
---

UTICA, NY – JETNET LLC, purveyor of aircraft fleet and market intelligence, has released its review of the market for business aircraft in the first half of 2020 and amidst the global COVID-19 crisis. To add context to these findings, Paul Cardarelli, JETNET Vice President of Sales, offered these comments.
READMORE

“Make no mistake, the market for business aircraft is under stress and this crisis is not helping. But many of the underlying reasons for this stress were in place prior to the crisis. So far this year brokers, dealers and sellers of business aircraft are showing restraint to not over-react to the negative stimuli that now surround the market. They are taking a short-term position on the crisis—a temporary anomaly that can be waited out. As a result of this discipline many key market metrics have yet to turn exceedingly negative.”

[Download the full release (PDF)](/assets/uploads/news/JETNET_BusinessAircraftMarketsReview_COVIDCrisis_FINAL.pdf)
