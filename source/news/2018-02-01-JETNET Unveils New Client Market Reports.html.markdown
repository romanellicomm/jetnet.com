---
title: JETNET Unveils New Client Market Reports
---

UTICA, NY – JETNET LLC, celebrating its 30th anniversary as the leading provider of corporate aviation information, has announced the release of the most comprehensive set of market reports available in the industry. The new Market Report format, designed with inputs from a team of JETNET customers, provides approximately 20 pages of model intelligence, including model specifications, range maps, fleet composition, market characteristics and insight, buying habits, and sales trends. Complete with a function to incorporate client headers and logos, the Market Report format is designed for both novice and sophisticated customers.

READMORE

“This new Market Report capability puts professional, customized reports in the hands of our clients,” said Mike Foye, JETNET Director of Marketing. “Which means they can select from a wide range of datasets to satisfy what their clients need, in a clear, concise, readable format for easier decision-making.”

Interested parties may contact their JETNET representative or Mike Foye for a sample Market Report, or a demonstration of the new Market Reports capabilities.

For 30 years, JETNET has delivered the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of some 100,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time Internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET_Market Reports press release.pdf)
