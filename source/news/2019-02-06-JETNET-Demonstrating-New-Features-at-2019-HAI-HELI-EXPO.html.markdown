---
title: JETNET Demonstrating New Features at 2019 HAI HELI-EXPO
meta_description: 
opengraph:
  title: "Heli-Expo Demos"
  description: "What you’ll see at the show"
  image: /assets/opengraph/Feb7_FB.jpg
twitter_card:
  title: "Heli-Expo Demos"
  description: "What you’ll see at the show"
  image: /assets/opengraph/Feb7_TW.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, will be demonstrating new features at the 2019 HAI HELI-EXPO in Atlanta, Georgia. Subscribers can customize reports for clients and internal using their popular Market Reports tool, letting them choose which parameters to display under their own logo and branding. JETNET will also be demonstrating their full suite of software products and services at this year’s HELI-EXPO. JETNET’s exhibit will run March 4th-7th in booth #B6735.

READMORE

“Our new features will increase professionals’ productivity and improve their client decision-making,” said Mike Foye, JETNET Director of Marketing. “Nearly all of our improvements are based on client and industry input, so we are providing tools our users want and need.” 

The Market Reports feature yields booklet-style customized outputs from JETNET’s intelligence software. Inspired by improved research and visual data presentation tools in JETNET’s products, the tools were designed with formal presentations in mind, and can be customized to include a client’s header and logo, along with selected data sets determined and configured by the user. The Market Report function also includes the ability to compare three makes/models on the same range map, details key flight utilization information, and includes a dashboard detailing the Market Absorption Rate. With dozens of serial-number-specific and make/model fleet overviews, subscribers can customize data sets for a more sophisticated audience.

The Market Report format provides approximately 20 pages of model intelligence, including model specifications, range maps, fleet composition, market characteristics and insight, buying habits, and sales trends. The report function was designed with both novice and sophisticated subscribers in mind.
JETNET’s comprehensive helicopter database contains nearly 32,000 in-service airframes worldwide, and information on transactions for more than 52,000 retail helicopter sales over the last two decades, along with 12 makes and 143 models of both turbine and piston airframes. Subscribers also have access to detailed individual aircraft specifications, and an operator/airport utilization feature that provides information on more than 24,800 flights registered over the last 24 months.

JETNET employs a full-time staff of approximately 50 research specialists who contact industry professionals around the world each business day. They make real-time updates to the most comprehensive database of its kind.

The company will be offering demonstrations of all their software, including the Market Report feature. JETNET has invited HELI-EXPO attendees to learn more about their products and services in booth #B6735.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-2019-HELI-EXPO.pdf)
