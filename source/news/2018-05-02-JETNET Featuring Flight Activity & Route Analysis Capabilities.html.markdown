---
title: JETNET Featuring Flight Activity & Route Analysis Capabilities at EBACE 2018
opengraph:
  title: "See a Demo at EBACE 2018"
  description: "Visit us in Geneva"
  image: /assets/opengraph/May4_FB.jpg
twitter_card:
  title: "See a Demo at EBACE 2018"
  description: "Visit us in Geneva"
  image: /assets/opengraph/May4_TW.jpg
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will demonstrate a series of new releases and features in their aircraft research service lineup at EBACE2018 at the Geneva Palexpo in Geneva, Switzerland. Highlights this year include JETNET’s popular historical flight activity and route analysis capabilities, paired with their comprehensive owner/operator database, the perfect fit for aviation professionals focused on the European theater of operations. The firm will also be showcasing improved market intelligence features, JETNET Values reported sold prices program, INTEL in-depth intelligence with flight activity data, Model and Market Reports, and their full line of intelligence products and services. JETNET’s full suite of offerings will be available for review and demonstration at the European Business Aviation Convention & Exhibition, May 29-31, at booth # W127.

READMORE

“We are always expanding our offerings to the markets we serve,” said Tony Esposito, JETNET Vice President. “Part of that effort is responding to our European clients with many new capabilities they have requested.”

<b>Historical Flight Activity and Route Analysis</b>

JETNET Marketplace and Aerodex Elite programs offer in-depth European historical flight activity and route analysis capability. With this function, users can analyze date-specific historical flight activities at the serial number level; routes; city pairs; make/model/owner/operator information; the ability to identify fuel/tech stops; fuel burn; and repositioning aircraft. Users can analyze flight activity and routes by aircraft, fleet, airport, company, and owner/operator, and use JETNET’s Excel sheets to export any required data. JETNET will be demonstrating these capabilities at EBACE, as well as how flight activities and route analysis can improve prospecting and awareness of client activities.

<b>Values</b>

JETNET Values is a single-source solution, providing reported sold prices combined with data assessment tools that aviation professionals can use to make informed decisions in finance, insurance, valuation, appraisals, and more. This optional add-on component to the JETNET Marketplace program includes a database of reported aircraft sales prices, with tools to predict and summarize trends. New sales prices are voluntarily provided to JETNET daily by more than 600 sources from its network of subscribers. JETNET reports that Values is now populated with more than 4,200 sales prices for 297 different aircraft models, most of which pertain to transactions having occurred within the last 18 months.

<b>INTEL</b>

INTEL is JETNET’s shortcut tool for Evolution Marketplace and Aerodex Elite customers. The function provides one- click access to comprehensive single-page summaries about flight activity, and extensive portfolio data on each aircraft, company, operator, airport, and more. INTEL makes each search more efficient, pulling together and presenting data accurately, easily, and with customized export capabilities.

<b>Model and Market Reports</b>

JETNET’s improved Model and Market Reports are becoming one of the most used features in their product lineup. The new format is visually appealing, and allows users to customize a PDF- or Word-formatted document with dozens of key sales and market-essential data sets.

<b>JETNET iQ Market Briefing at EBACE</b>

JETNET iQ provides independent quarterly intelligence for the business aviation industry, including owner/operator survey results, economic and industry analyses, and forecasts. It is the result of a collaboration between JETNET and aviation consultancy Rolland Vincent Associates. At the core of JETNET iQ is a detailed quarterly survey of more than 500 business aircraft owners and operators worldwide. Responses from more than 14,000 owners/operators have been collected since surveys began in 2011, representing the largest commercially available database of its kind in the industry.

JETNET iQ will hold a State of the Market Briefing for show attendees on Tuesday, May 29 from 11:00-11:45am, in Salle R at Palexpo. JETNET iQ founder Rolland Vincent, and Paul Cardarelli, JETNET Vice President of Sales, will provide an overview of the service’s latest findings, including reasons for the current wave of optimism in the industry. Learn about the latest pre-owned retail sales, the global impact of the 2017 Tax Cuts and Jobs Act in the U.S., the impending replacement cycle due for the light and medium class jets, and the prevailing optimism for operators, especially in Europe. They will also discuss the market challenges that still exist, especially with deflated aircraft residual values and utilization rates.

<b>JETNET Database</b>

JETNET’s database of in-operation aircraft exceeds 110,000 airframes, which includes business jets, business turboprops, commercial airliners (both jets and turboprops), and helicopters (both turbine and piston).

“Our seventeenth year at EBACE is a testament to our commitment to the European aviation market,” added Paul Cardarelli, JETNET Vice President of Sales. “The global business aviation market is constantly changing, and we want to provide the best tools available to our aviation colleagues to help support this recovering market.”

The JETNET database includes comprehensive details on aircraft airframes, engines, avionics, and cabin amenities, as well as aircraft owners and operators, lessors and lessees, fractional owners, and a host of other entities associated with aircraft. Also included are transaction histories on aircraft dating back more than 25 years, monthly aircraft market summary reports, and a variety of other tools for assessing the global fleet and aircraft marketplace.

<b>YachtSpot</b>

JETNET’s database on the worldwide luxury fleet, YachtSpot, continues to expand, and is the only database providing detailed information about those individuals and businesses with relationships to both business aircraft and yachts on a worldwide basis. YachtSpot provides OEMs, dealers and brokers, and shipyards the ability to examine the ranking of top performers in yacht and aircraft resale and charter. Subscribers can access assets with key contacts about resale or charter opportunities, classification, flagged countries, amenities, pedigree, and more, with opportunities for aircraft and luxury marine professional collaboration in resale, charter, referrals, insurance, finance, and more.

<b>EBAA and CBAA Memberships</b>

JETNET is a member of the European Business Aviation Association (EBAA), and serve on the subcommittee for Aircraft Sales and Acquisitions.

“We consider Europe a major player in our industry,” said Karim Derbala, JETNET Managing Director of Global Sales. “Membership and participation in EBAA give us the opportunity to better serve professionals in European business aviation, one of the continent’s economic driving forces.”

In an effort to further expand their global relationships, JETNET has also joined the Canadian Business Aviation Association (CBAA). JETNET will participate in the CBAA annual conference, and continue to build their research and client contacts in this vital market.

JETNET invites attendees to see all of their new product and service offerings at this year’s EBACE, May 29-31, at booth # W127. Additionally, on Tuesday, May 29 from 11:00-11:45am in Salle R, they will conduct their annual JETNET iQ European State of the Business Jet Market briefing. All business aviation professionals and press are invited to attend.

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of- the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET_EBACE_pressrelease.pdf)
