---
title: JETNET iQ Summit to Focus on Sustainability
meta_description: "This year’s Summit theme is “Bluer Skies. Greener Future.”"
---

UTICA, NY - JETNET, the world’s leading provider of business aviation data, has announced their 2022 JETNET iQ Global Business Aviation Summit. The 11th Summit will be held September 15-16th, 2022 at the New York Marriott Marquis in Times Square.
READMORE

This year’s Summit theme is “Bluer Skies. Greener Future.” Attendees will have the chance to hear from the “Who’s Who” of business aviation. The industry’s top leaders will discuss the complexities of the present, as well as the philosophies and actions required to achieve a sustainable future. Key perspectives from experts such as analysts, brokers, and C-level executives will be delivered during panel discussions and speaker events. The Summit will also allow attendees time to meet their contemporaries, network, and develop topics outside of scheduled sessions.

Two-tiered pricing has begun with a discounted Early Bird Registration rate for the JETNET iQ Summit, a $200 savings, and is available through May 16th, 2022. Beginning May 17th, 2022, through September 1st, 2022, pricing for attending the event will be $1,445 USD. Registration can be found at <a href="/summit">jetnet.com/summit</a>.

**Cancellation Policy**

Refunds will be 50% for cancellation notices received prior to August 1st, 2022, and 0% thereafter.

**About JETNET iQ**

Summits are part of JETNET iQ, an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET iQ Summit, visit jetnet.com or contact Rolland Vincent, JETNET iQ Creator/Director, at 972.439.2069 or <a href="mailto:rollie@jetnet.com">rollie@jetnet.com</a>. For information on JETNET, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or <a href="mailto:paul@jetnet.com">paul@jetnet.com</a>; Derek Jones, Lead Sales Contact: Americas & Asia Pacific, at 800.553.8638, or <a href="mailto:derek@jetnet.com">derek@jetnet.com</a>; Karim Derbala, Lead Sales Contact: EMEA (Europe, Middle East, Africa), at +41.0.43.243.7056 or <a href="mailto:karim@jetnet.com">karim@jetnet.com</a>.

[Download the full release (PDF)](/assets/uploads/news/JETNET iQ Summit Announcement PR.pdf)
