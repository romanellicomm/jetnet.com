---
title: JETNET Releases December 2019 and the Year 2019 Pre-Owned Business Jet, Business Turboprop, Piston, Helicopter, and Commercial Airliner Market Information
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---


UTICA, NY – JETNET LLC, the leading provider of aviation market information, has released December 2019 and 2019 year-end results for the pre-owned business jet, business turboprop, piston, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in **Table A** are key worldwide trends across all pre-owned aircraft market sectors, comparing
December 2019 to December 2018, as well as 2019 year-end, and comparisons to 2018.

**The ‘For Sale’ inventory grew by 2 percent across all seven market sectors in a comparison of 2019 (6,324) to 2018 (6,201) with business jets and fixed wing piston aircraft accounting for the majority of this increase. Also, there were almost 1,200 or 11.8 percent fewer full-sale transactions in 2019 compared to 2018 with most of the sectors down by double digits percentages. Interestingly, business jets and commercial jets declined around 14 percent each. These two sectors accounted for nearly half of all transactions in 2019.**

The Fleet For-Sale percentages for business jets and piston aircraft jumped up .7 percentage point each. Business Jets are at 9.7% increased from 9 percent but still in a buyer’s market of under 10 percent. All aircraft segments were taking more time to sell but only by 2 more days in 2019 compared to 2018 across all market sectors. However, this was mixed, several were taking longer, and several were taking fewer days to sell. For example, business jets were selling in 273 days, less than two weeks-time (13 days) than 2018.

<img src="/assets/uploads/images/2020-02-11-Table-A.png" class="img-fluid mx-auto d-block" alt="Chart A">

As the business jet market has finally broken below the 10 percent threshold of inventory for sale for two years now, a period of transition is now in play, wherein the pendulum continues to swing in favor from the buyer to the seller. Today the market of available aircraft generally continues to shrink, and still many models exhibit the soft pricing brought on by the diminishment of residual values that completely dominated the post- recession years.

**Chart A** illustrates that the “For Sale” inventory of business jets has decreased steadily from a high point in July 2009 (2,938) to 2,167 jets in December 2019. That’s a reduction in the percentage of the in-service fleet from 17.7 percent in July 2009 to 9.7 percent now. This is a positive sign, as the inventory “For Sale” has remained below the 10 percent line, albeit slowly. However, the inventory level registered above the 2,000 mark for 11 out of the 12 months of 2019.

<img src="/assets/uploads/images/2020-02-11-Chart-A.png" class="img-fluid mx-auto d-block" alt="Chart B">

**Table B** shows the Pre-owned Business Jet Retail Transactions (Whole & Leases) comparing 2019 and 2018 by Age grouping. There were 422, or 14.5% fewer Pre-owned Business Jets sold in 2019 compared to 2018.

Two observations are:

<ul>
  <li>The age groups 0 to 20 years saw double digit percentage declines of 20% or greater reflecting
significantly fewer sold transactions in 2019.</li>
  <li>The age group over 41 saw a 19% increase in pre-owned transactions in 2019.</li>
</ul>

<img src="/assets/uploads/images/2020-02-11-Table-B.png" class="img-fluid mx-auto d-block" alt="Chart B">

**Chart B** displays the 12-Month moving average for full retail transactions from December 2011 to December 2019 for business jets.

From December 2011, used business jet transactions steadily increased until June 2016 at 2,725. Then there was a sudden drop to 2,534 transactions in December 2016. From this point on, the used business jet market segment has shown a sharp recovery through December 2018 (2,918). Following this peak, however, transactions took a sharp decline to 2,496 in December 2019. This is a new low below the lowest point recorded three years earlier in December 2016 at 2,534.

<img src="/assets/uploads/images/2020-02-11-Chart-B.png" class="img-fluid mx-auto d-block" alt="Chart B">

**Table C** shows the pre-owned transaction comparison of 2019 and 2018 by aircraft size. Two groups showed increases – Personal Jet and Airline business jet. The remaining 8 groups all showed large declines.

<img src="/assets/uploads/images/2020-02-11-Table-C.png" class="img-fluid mx-auto d-block" alt="Chart B">

**Special note:** The Personal Jet category is new and today has but one participant - the Cirrus Jet launched just 4 years ago and having experienced exponential growth in new deliveries over that last 3 years. The Cirrus Vision SF-50 is the first jet of any kind to come with a whole aircraft ballistic parachute. It is also considered the first civilian single-engine jet to achieve certification with the FAA.

**Summary**

The recovery in business aviation during the post-recession period has been with mixed results, with poor overall aircraft residual values that continue to be problematic. Now that 2020 is here, we hope the U.S. pre- owned market, along with improvements in the world economy, will continue to push more new aircraft purchases. As for now, the pre-owned market is in a seller’s market environment, with Pre-Owned For-Sale inventories running at 9.7%.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-December-2019-Market-Info-2.pdf)
