---
title: JETNET Sourced in December AIN Special Report
opengraph:
  url: https://www.jetnet.com/news/jetnet-sourced-in-december-ain-special-report.html
  title: "JETNET in AIN Special Report"
  description: "Download and read"
  image: /assets/opengraph/2018/JN_AIN_Facebook_OG.jpg
---

Aviation International News heavily sourced JETNET iQ and founder Rollie Vincent in their December Special Report on the Preowned Aircraft Report.

READMORE

<a href="/assets/uploads/pages.reports/ain_2017_preownedspecialreportnewcompressed.pdf">Download and read the complete report.</a>
