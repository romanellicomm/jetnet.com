---
title: JETNET Debuting New Features at 2022 HAI HELI-EXPO
meta_description: "JETNET has announced they will be taking part in the 2022 HAI HELI-EXPO, with an emphasis on their technological advancements in data analysis."
---

UTICA, NY - JETNET, the industry-leading business aviation research company, has announced they will be taking part in the 2022 HAI HELI-EXPO, with an emphasis on their technological advancements in data analysis. The 2022 HAI HELI-EXPO will be held March 7-10, 2022, in Dallas, Texas.
READMORE

“JETNET is excited to be back at the HAI HELI-EXPO,” stated Paul Cardarelli, JETNET’s Vice President of Sales. “We will be presenting new features of both JETNET Helicopter Services and our Fleet Analyzer. We’ll also be offering a sneak peek of our new JETNET BI (Business Intelligence) platform.”

JETNET’s Helicopter Services offers curated data on makes/models, aircraft status, owner/operator information, historical airframe records, transaction information, and now the two new features of utilization data and consumable component tracking. Utilization data includes information on mission usages and maintenance regulations such as SAR, offshore, heavy lift, firefighting, FAR Part 133, and more. Consumable component tracking provides information on interior configurations, engine models, significant rotor blades/gearbox/transmission overhauls by AFTT/cycles and engine hours, and much more.

JETNET’s Fleet Analyzer offers the ability to view, analyze, and custom build complex fleets using their attributes, utilization, transaction history, owner/operator statistics, and more. Users have the ability to build and review fleets of aircraft based on a wide variety of perspectives such as age, value, maintenance analytics, flight activity, models, and history, among other factors.

Attendees of the EXPO will also get a sneak peek of JETNET BI prior to the company’s official product launch. JETNET BI will provide graphic visualizations and interactive dashboards for insights around fleet stats, flight activity, Values, JETNET iQ surveys and Forecast research, and more. “JETNET BI is our latest and best visualization of our data,” added Derek Jones, JETNET’s Lead Sales Contact: Americas and Asia Pacific. “This great new technological solution will provide customers with the industry’s best insights.”

JETNET will be showcasing its services at booth #4207 at the HAI HELI-EXPO.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 108,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET and our exclusive services, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or paul@jetnet.com; Lead Sales Contact: Americas & Asia Pacific: Derek Jones, at 800.553.8638, or derek@jetnet.com; EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at +41.0.43.243.7056 or karim@jetnet.com.

[Download the full release (PDF)](/assets/uploads/news/JETNET Heli-Expo PR.pdf)
