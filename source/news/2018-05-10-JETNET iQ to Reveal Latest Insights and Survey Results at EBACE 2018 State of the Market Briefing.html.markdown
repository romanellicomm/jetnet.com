---
title: JETNET iQ to Reveal Latest Insights and Survey Results at EBACE 2018 State of the Market Briefing
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will present a briefing by <a href="https://www.jetnet.com/products/jetnet-iq/">JETNET iQ</a> on the state of the business aviation market by drawing from results of their latest industry report, at <a href="https://ebace.aero/2018/"> EBACE2018</a> at the Geneva Palexpo in Geneva, Switzerland. Rolland Vincent, founder of JETNET iQ, and JETNET Vice President of Sales Paul Cardarelli will conduct their annual JETNET iQ State of the Market briefing, free to attendees, Tuesday, May 29 from 11:00-11:45am, in Salle R.

READMORE

JETNET iQ provides independent quarterly intelligence for the business aviation industry, including owner/operator survey results, economic and industry analyses, and forecasts. It is the result of a collaboration between JETNET and aviation consultancy <a href="http://www.navigating360.com/"> Rolland Vincent Associates</a>. At the core of JETNET iQ is a detailed quarterly survey of more than 500 business aircraft owners and operators worldwide. Responses from more than 14,000 owners/operators have been collected since surveys began in 2011, representing the largest commercially available database of its kind in the industry.

The EBACE2018 briefing will include an overview of the service’s latest findings, including reasons for the current wave of optimism in the industry. Learn about the latest pre-owned retail sales, the global impact of the 2017 Tax Cuts and Jobs Act in the U.S., the impending replacement cycle due for light and medium class jets, and the prevailing optimism for operators, especially in Europe. The presenters will also discuss the market challenges that still exist, especially with deflated aircraft residual values and utilization rates.

Customers for JETNET iQ include airframe and power plant OEMs, Tier 1 suppliers, fleet operators, aircraft dealers and brokerages, FBOs, MROs, aircraft lenders/lessors, and aerospace investment analysts.

“Net optimism in the fixed-wing turbine-powered owner/operator community is the highest we have ever measured in 29 consecutive quarters since Q1 2011, when we initiated our JETNET iQ Surveys,” said Rolland Vincent. “We think attendees will enjoy hearing the positive news in the global market.”

“Net Optimism”, the difference between respondents who indicate that current market conditions are “Past the low point” and those who say that we have “Not yet reached the low point”, reached almost 45% in Q1 2018, the highest JETNET iQ has yet measured.

“This optimism,” Paul Cardarelli added, “is driven by improved economic outlooks in 2018 in both the U.S. and European markets. Coupled with sharply reduced availability of younger pre-owned aircraft inventory and new aircraft designs coming into the market, this sets the stage for a stronger new aircraft order and delivery outlook in the years ahead.”

Those interested in learning more about the latest market and owner/operator sentiments can attend the JETNET iQ EBACE2018 State of the Market briefing and press conference in Salle R on Tuesday, May 29 from 11:00-11:45am.

JETNET invites attendees to see all of their new product and service offerings at this year’s EBACE, May 29- 31, at booth # W127.

Among its other services, JETNET iQ holds an annual summit featuring spirited presentations and dialog from industry leaders. The 2018 JETNET iQ Global Business Aviation Summit will be held June 19-20 at The Ritz- Carlton New York, Westchester at Three Renaissance Square, White Plains, NY. Details are available on the <a href="https://www.jetnet.com/summit/"> JETNET website</a>.

JETNET, celebrating its 30th anniversary as the leading provider of aviation market information, delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNETiQ_EBACE_MarketBriefing.pdf)
