---
title: JETNET Expands Leadership Team with Appointment of Derek Swaim as President
meta_description: "Former Executive-in-Residence at Silversmith Capital Partners and Executive Vice President of Corporate Development at Validity Brings Significant Operational and Leadership Experience"
opengraph:
  title: "JETNET Expands Leadership Team"
  description: "Read More"
  image: /assets/opengraph/Swaim_FB.jpg
twitter_card:
  title: "JETNET Expands Leadership Team"
  description: "Read More"
  image: /assets/opengraph/Swaim_TW.jpg
---

_Former Executive-in-Residence at Silversmith Capital Partners and Executive Vice President of Corporate Development at Validity Brings Significant Operational and Leadership Experience_

UTICA, NY - [JETNET](https://www.jetnet.com/), a leading provider of aviation data and market intelligence, today announced the appointment of Derek Swaim as President. With more than 20 years of experience executing corporate acquisitions, mergers, and growth equity investments for market-leading software, business information, and industrial technology companies, Swaim brings deep operational and strategic leadership skills to the company. In addition to his role as President, Swaim has joined JETNET’s Board of Directors.

Most recently, Swaim served as an Executive-in-Residence at [Silversmith Capital Partners](https://www.silversmith.com/) and previously served as Executive Vice President of Corporate Development at Validity, a Silversmith portfolio company, where he played an integral role as the SaaS platform increased revenue by over 10x and scaled into a global company.  During his tenure at Validity, Swaim oversaw three accretive acquisitions—Return Path, AppBuddy, and 250ok—and over 200 corporate partnerships. Prior to Validity, Swaim was a Managing Director at Aeris Partners, a provider of M&A advisory services to software, digital media, and business information companies. Previous roles include investment banking positions at Harris Williams and Goldman Sachs.

In July 2022, JETNET secured a majority growth investment led by Silversmith Capital Partners, a Boston-based growth equity firm. The investment marked the first time the company had raised outside capital.

“I am thrilled to welcome Derek to the JETNET executive team and look forward to working with him,” said JETNET CEO Greg Fell.  “With a demonstrated track record of driving strategic transformations at software and business information companies, we feel fortunate to have him join our team as we look to expand our product offerings and global footprint.”

JETNET offers the most comprehensive and reliable database of business and commercial aviation information to its global customer base. Working directly with operators, owners, and manufacturers, JETNET maintains the most accurate information in the industry on pricing, availability, and location of mission-critical aviation resources.

“I am honored to be joining the JETNET family as they enter into this new era of growth,” said Swaim. “The JETNET team has built an exceptional company with subscription-based products that customers simply love. JETNET has a huge opportunity to expand its offerings within the aviation industry and beyond, and I look forward to being part of this next chapter.”

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For information on JETNET, visit [jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com); Derek Jones, Lead Sales Contact: Americas & Asia Pacific, at 800.553.8638, or [derek@jetnet.com](mailto:derek@jetnet.com); Karim Derbala, Lead Sales Contact: EMEA (Europe, Middle East, Africa), at +41.0.43.243.7056 or [karim@jetnet.com](mailto:karim@jetnet.com).

**About Silversmith Capital Partners**

Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $3.3 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, Appfire, DistroKid, impact.com, Iodine Software, LifeStance Health, and Webflow. For more information about Silversmith, please visit [www.silversmith.com](https://www.silversmith.com/) or follow the firm on [LinkedIn](https://www.linkedin.com/company/silversmith-capital-partners).

[Download the full release (PDF)](/assets/uploads/news/JETNET_DerekSwaimAppointedPresident.pdf)
