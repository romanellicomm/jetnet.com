---
title: JETNET iQ to Reveal Insights and Survey Results at EBACE 2019 State of the Market Briefing
meta_description: 
opengraph:
  title: "JETNET iQ Briefing at EBACE"
  description: "Market present & future"
  image: /assets/opengraph/May7_EBACE_FB.jpg
twitter_card:
  title: "JETNET iQ Briefing at EBACE"
  description: "Market present & future"
  image: /assets/opengraph/May7_EBACE_TW.jpg
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will present a briefing by JETNET iQ on the state of business aviation in Europe at EBACE2019 at the Geneva Palexpo in Geneva, Switzerland. At the briefing, which is free for attendees, JETNET iQ Creator and Director Rolland Vincent and JETNET Vice President of Sales Paul Cardarelli will provide an overview of the service’s findings, as pertinent to Europe and their exclusive industry-wide forecast for the next ten years. Using results from their latest industry survey and report, they will discuss pre-owned retail sales, the prevailing attitude from operators (especially in Europe), and the market challenges and opportunities that still exist. The briefing will be on Tuesday, May 21 from 11:30am-12:15pm in Salle R, Hall 3, at Palexpo.

READMORE

**EBACE**

At the European Business Aviation Convention & Exhibition, May 21-23 at booth #W127, JETNET will also be demonstrating their full suite of software for aviation professionals focused on the European theater of operations. Among their other products and enhancements, they will be featuring their new JETNET iQ product level, JETNET iQ OEM Insider.

**JETNET iQ**

JETNET iQ (Independent, Quarterly) is an initiative launched in 2011 by JETNET LLC in collaboration with the aviation consultancy Rolland Vincent Associates LLC of Plano, TX. Each quarter the JETNET research team conducts a detailed survey of 500 business aircraft owners and operators around the globe. They have gathered some 17,000 surveys since iQ’s launch in 2011, likely the most comprehensive ongoing commercially available study of business aircraft owner/operator behavior and sentiment ever conducted.

JETNET iQ surveys monitor the pulse of the business aviation owner/operator community—jets and turboprops—on such issues as their regard for major airframe and engine brands, intentions for future aircraft utilization and purchases, perceived obstacles to new aircraft purchases, and sentiment for the marketplace as a whole. From this information, they compose a detailed study on a quarterly basis, segmented into four categories:

<ul>
  <li>The Economy – A review of global economic indicators driving the new and pre-owned business jet markets.</li>
  <li>The Industry – A review of new deliveries, fleet developments, and retirements for each of the major business jet and turboprop airframe OEMs.</li>
  <li>The Survey – The sentiments of aircraft owners and operators on their plans to fly and buy aircraft, their regard for key brands in business aviation, and other relevant topics.</li>
  <li>The Forecast – The JETNET iQ exclusive ten-year prognosis for business jet and turboprop deliveries and retirements.</li>
</ul>

**JETNET iQ 2019 Summit**

JETNET iQ will be holding its 9th Annual JETNET iQ Global Business Aviation Summit June 4-5 at The Ritz- Carton New York, Westchester in White Plains, NY. Rolland Vincent, iQ Creator and Director, and Paul Cardarelli, JETNET Vice President of Sales, will join dozens of global business aviation C-level leaders and professionals to present information and engage in conversation on a wide range of topics. The event’s Keynote speaker will be Bill Boisture, Chairman of Global Jet Capital. For more information visit <a href="https://www.jetnet.com/summit">jetnet.com/summit</a>.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/2019-JETNET-EBACE-Briefing.pdf)
