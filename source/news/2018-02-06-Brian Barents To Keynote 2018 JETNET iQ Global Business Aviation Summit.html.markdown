---
title: Brian Barents To Keynote 2018 JETNET iQ Global Business Aviation Summit
---

UTICA, NY – JETNET LLC, celebrating its 30th anniversary as the leading provider of corporate aviation information, has announced that Brian E. Barents, Executive Chairman and CEO of Aerion Corporation, will be the Keynote Speaker for its 8th annual JETNET iQ Global Business Aviation Summit in White Plains, New York, June 19-20, 2018.

A virtual “Who’s Who” of the business aviation industry gather for the annual JETNET iQ Summit, from firms in fields as diverse as finance, manufacturing, supply, data analysis. Speakers and panelists share their unfiltered take on the current state and future direction of aviation with attendees. This year’s Summit has scheduled a workshop and opening reception on Tuesday, June 19, followed by an all-day program featuring industry speakers and panelists on Wednesday, June 20, at The Ritz-Carlton New York, Westchester, Three Renaissance Square, White Plains, NY. For the convenience of participants, the Summit will once again coincide with the NBAA Regional Forum at Westchester County Airport (HPN) in White Plains, NY on Thursday, June 21, 2018.

READMORE

Keynote Speaker Brian E. Barents, a veteran of business aviation, is Executive Chairman and CEO of Aerion Corporation, developer of the AS2 Supersonic Business Jet. He has held a number of leadership roles in the industry, including President and CEO of Learjet, and President and CEO of Galaxy Aerospace. Attendees, including analysts, bankers, brokers, wealth consultants, attorneys, sales and marketing leaders, and C-level executives, look forward to talks—as well as networking opportunities—with leaders like Barents.

“This year, we have located our summit for the convenience of our attendees, many of whom go on to attend NBAA’s New York Regional Forum the very next day,” said Paul Cardarelli, JETNET Vice President of Sales. “The spirit of our event has always been to raise the level of frank and honest dialog on the challenges that confront our industry. To that end, there is a great deal of insight and intelligence shared for the benefit of all who attend our summit.”

JETNET iQ provides independent quarterly intelligence for the business aviation industry, including economic and industry analyses, aircraft owner/operator survey results, and delivery and fleet forecasts. JETNET iQ recently published its 27th quarterly report. The foundation of JETNET iQ is a proprietary survey database with information from more than 14,000 owner/operator respondents from 129 countries, the largest on-going research of customer sentiment available in the business aviation industry.

“Our business jet delivery forecasts have proven to be better than 99.5% accurate over the last several years, and we are working hard on that last 0.5%,” said Rolland Vincent, Creator/Director of JETNET iQ. “Participants subscribe to our reports to stay ahead of the market throughout the year, and these Summits help give them even better perspective from different industry leaders. It’s invaluable.”

Those interested in attending the event can register at <a href="https://www.jetnet.com/summit">jetnet.com/summit</a>. Space is limited, and a special “early bird” registration rate expires on February 28th.

Since 1988, JETNET has delivered the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of some 100,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET_2018_iQ_Summit.pdf)
