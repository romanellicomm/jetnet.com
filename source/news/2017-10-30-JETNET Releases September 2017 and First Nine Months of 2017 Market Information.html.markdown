---
title: JETNET Releases September 2017 and First Nine Months of 2017 Pre-Owned Business Jet, Business Turboprop, Helicopter and Commercial Airliner Market Information
opengraph:
  url: http://www.jetnet.com/news/jetnet-releases-september-2017-and-first-nine-months-of-2017-market-information.html
  title: The Market is Turning
  description: "Read the full story"
  image: /assets/opengraph/Nov7_FB.jpg
---

UTICA, NY – JETNET LLC, the leading provider of corporate aviation information, has released September 2017 and the first nine months of 2017 results for the pre-owned business jet, business turboprop, helicopter, and commercial airliner markets.

READMORE

**Market Summary**

Highlighted in Table A below are key worldwide trends across all aircraft market segments, comparing September 2017 to September 2016. Fleet For Sale percentages for all market sectors were down in the September comparisons, dropping a half of a percentage overall. Business Jets and Business Turboprops showed the largest declines in the Percentages For Sale compared to the other markets. Business Jets are at 10.4%, compared to 11.5% at this same time last year.

This is very good news, but we are just above the 10% line and still in a buyer’s market. Generally, inventories of pre-owned business jets for sale have decreased, and are now just above the 2,200 mark as well.

Business Jets are showing an increase (5.9%) in pre-owned sale transactions in the first nine months of 2017 compared to the same period in 2016. Also, Business Jets are taking the same amount of time to sell (313 days YTD) as last year at 312 days.

Turbine Helicopters showed an increase of 5.7% in YTD sales transactions, whereas Piston Helicopters saw a double-digit decline in YTD sale transactions, at 14.1%.

Commercial Airliners are reported by JETNET in Table A - Worldwide Trends, and include the numbers for sale for both commercial jets (including airliners converted to VIP) and commercial turboprops. Commercial Jet YTD sale transactions, at 1,636, are trailing Business Jets, at 1,946 sale transactions. However, the Pre-owned Business Jet, Pre-owned Commercial Jet, and Turbine Helicopter market sectors each have surpassed the 1,000 mark for YTD sale transactions for a combined 4,643 transactions; that represented 71% of the total compared to the other market segments.

For the first nine months of 2017, there have been 6,060 pre-owned commercial and business jets, turboprops, and helicopters sold. This is an increase of 35, or a half-percent more sale transactions compared to 2016.

<img src="/assets/uploads/images/2017-10-30-Table-A_Worldwide-Trends.png" class="img-fluid mx-auto d-block" alt="Table A">

**Market Trend Segment Analysis**

Further analysis of Table A showed mixed results for the six segments reported. Business Jets (5.9%), Turbine Helicopters (5.7%), and Commercial Airliners (8.8%) increased YTD in September 2017 compared to YTD September 2016. The remaining three segments decreased by double-digit percentages, with piston helicopters showing the largest decrease (down 14.1%) in the same YTD comparisons for 2017 vs. 2016.

The following chart will focus on the Pre-owned Turbine Helicopters 12-month moving average retail sale transactions from January 2012 to September 2017. Since January 2017 (lowest point in the past 5 years), the pre-owned turbine helicopter market segment has shown a very rapid recovery up to September 2017. This is great news for the Pre-owned Turbine Helicopter segment.

<img src="/assets/uploads/images/2017-10-30-Pre-owned-Turbine-Helicopters.png" class="img-fluid mx-auto d-block" alt="Pre-owned Turbine Helicopters 12-month moving average retail sale transactions from January 2012 to September 2017">

[Download the full release (PDF)](/assets/uploads/news/JETNET September 2017 Market Information Report.pdf)
