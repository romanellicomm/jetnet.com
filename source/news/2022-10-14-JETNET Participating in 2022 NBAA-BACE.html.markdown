---
title: JETNET Participating in 2022 NBAA-BACE
meta_description: "JETNET, the world leader in aviation market intelligence, will participate in the
2022 NBAA Business Aviation Convention & Exhibition (NBAA-BACE) on October 18-20, 2022"
opengraph:
  title: ""
  description: "Read More"
  image: /assets/opengraph/
twitter_card:
  title: ""
  description: "Read More"
  image: /assets/opengraph/
---

UTICA, NY - JETNET, the world leader in aviation market intelligence, will participate in the
2022 NBAA Business Aviation Convention & Exhibition (NBAA-BACE) on October 18–20, 2022,
at the Orange County Convention Center in Orlando, Florida, at booth #1236.

“JETNET is honored to be back at this year’s show and to help NBAA celebrate 75 years of
serving the business aviation community,” stated Greg Fell, JETNET’s CEO. “NBAA-BACE
shines a spotlight on future-forward, innovative perspectives on our dynamic industry. It’s a
great place for exploring modern high-tech aircraft, and business aviation’s best products and
services.”

JETNET will have a strong presence at NBAA-BACE with many top-level executives in
attendance, including:

* Greg Fell, CEO
* Derek Swaim, President
* Rollie Vincent, JETNET iQ Creator and Director
* Jason Lorraine, Director of Strategic Solutions and Product Sales
* Johanna Pion, Team Mentor, Aviation Research Specialist, and Product Sales
* Erin Burke, Client Relationship Specialist and Product Sales
* Tanya Elthorp, Research Manager and Product Sales
* Paul Cardarelli, Vice President of Sales
* Karim Derbala, Managing Director of Global Sales
* Derek Jones, Sales Coordinator
* Justine Strzepek, Marketing Manager
* David Labrecque, Director of Research and Inside Sales
* Sam Hester, General Counsel

JETNET will also showcase and provide demonstrations of their Evolution Marketplace,
Aerodex, JETNET BI (Business Intelligence), Flight Insights, JETNET for Salesforce®, API,
Flight Activity, and more from their line of services.

On Tuesday, October 18, 2022, JETNET will present the JETNET iQ State of the Market Press
Briefing from 12:00 pm EST to 12:45 pm EST, in room N210-D of the Orange County
Convention Center.

“The JETNET iQ State of the Market Press Briefing is a great opportunity to share owner and
operator opinions from across the industry,” stated JETNET iQ Co-Founder and Director Rollie
Vincent. “It’s the perfect place to get fresh perspectives and directional ideas regarding business
aviation.”

For any press-related inquiries to obtain materials from the JETNET iQ State of the Market
Press Briefing, please reach out to Justine Strzepek, Marketing Manager, at
800.553.8638 or [justine@jetnet.com](mailto:justine@jetnet.com).

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and
reliable business aircraft research to its exclusive clientele of aviation professionals worldwide.
JETNET is the ultimate source of information and intelligence on the worldwide business,
commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000
airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers
comprehensive, user-friendly aircraft data via real-time internet access or regular updates.
JETNET is a portfolio company of Silversmith Capital Partners.

For information on JETNET and our exclusive services, visit [jetnet.com](mailto:jetnet.com) or contact Paul
Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com); Lead
Sales Contact: Americas & Asia Pacific: Derek Jones, at 800.553.8638, or [derek@jetnet.com](mailto:derek@jetnet.com);
EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global
Sales, at +41.0.43.243.7056 or [karim@jetnet.com](mailto:karim@jetnet.com).

**About JETNET iQ**

Summits are part of JETNET iQ, an aviation market research, strategy, and forecasting service
for the business aviation industry. JETNET iQ also provides independent, Quarterly Intelligence,
including consulting, economic and industry analyses, business aircraft owner/operator survey
results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between
JETNET and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research
consultancy.

[Download the full release (PDF)](/assets/uploads/news/JETNET_NBAA_PR.pdf)
