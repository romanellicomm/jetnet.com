---
title: JETNET Participating in 2021 NBAA-BACE
meta_description: ""
opengraph:
  title: "Visit Us At NBAA-BACE"
  description: "Booth #601"
  image: /assets/opengraph/JN_NBAA_FB.jpg
twitter_card:
  title: "Visit Us At NBAA-BACE"
  description: "Booth #601"
  image: /assets/opengraph/JN_NBAA_TW.jpg
---

UTICA, NY - JETNET, a leading provider of corporate aviation information, will be taking part in the 2021 NBAA Business Aviation Convention & Exhibition (NBAA-BACE) October 12-14, 2021, at the Las Vegas Convention Center in Las Vegas.
READMORE

“JETNET is thrilled to exhibit at NBAA-BACE in person again,” stated Paul Cardarelli, JETNET’s Vice President of Sales. “At this year’s show, we will be debuting our reimagined Values program in partnership with Asset Insight at booth #601 in the West Hall.”

JETNET Values is a unique single-source asset valuation platform that combines key historical market data and reported sold prices with access to estimated fair market and residual value figures produced by Asset Insight’s “eValuesTM”.

Additionally, JETNET will provide a sneak peak of JETNET BI, a new business intelligence platform scheduled to launch in Q1 of 2022. The new data solution will provide graphic visualizations and interactive dashboards for insights around fleet stats, flight activity, values, JETNET iQ surveys and JETNET iQ forecast data, and more.

“JETNET is committed to serving its customers with the latest technology solutions to provide the industry’s best insights. We look forward to sharing JETNET BI, our latest and best visualization of our data,” added Jason Lorraine, Director of Strategic Solutions and Product Sales.

In conjunction with JETNET iQ, JETNET will also present the JETNET iQ State of the Market briefing during NBAA-BACE on Tuesday, October 12, 2021 at 11:30am PST, in room W213. The presentation will highlight market conditions, the latest forecasts on fleet and new aircraft deliveries of business fixed wing turbines, and proprietary insights from JETNET iQ’s Global Business Aviation Surveys.

“The JETNET iQ State of the Market Press Conference is a great opportunity to share owner and operator opinions from across the industry,” stated JETNET iQ President and Co-Founder, Rollie Vincent. “It’s the perfect place to get fresh perspectives and directional ideas regarding business aviation.”

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 112,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For more information on JETNET and our exclusive services, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 ext. 254 (USA) or paul@jetnet.com; Lead Sales Contacts: Americas & Asia Pacific: Derek Jones, at 800.553.8638 ext. 603 or derek@jetnet.com; EMEA (Europe, Middle East, Africa): Karim Derbala, JETNET Managing Director of Global Sales, at 41.0.43.243.7056 ext. 854 or karim@jetnet.com.

**About JETNET iQ**

JETNET iQ is an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET LLC and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy. For more information on the JETNET iQ, visit jetnet.com or contact Rolland Vincent, JETNET iQ Creator/Director, at 972.439.2069 or rollie@jetnet.com.

[Download the full release (PDF)](/assets/uploads/news/JETNET 2021 NBAA-BACE.pdf)
