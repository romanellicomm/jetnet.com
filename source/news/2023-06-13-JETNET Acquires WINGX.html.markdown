---
title: JETNET Acquires WINGX to Bolster Global Business Jet Intelligence and Accelerate International Growth
meta_description: "Together, the two companies plan to enrich the way customers access and utilize data and insights relating to the deployment, management, and utilization of business jets."
opengraph:
  title: "JETNET Acquires WINGX"
  description: "Know More"
  image: /assets/opengraph/JN_AcquiresWINGX_FB.jpg
twitter_card:
  title: "JETNET Acquires WINGX"
  description: "Know More"
  image: /assets/opengraph/JN_AcquiresWINGX_TW.jpg
---

UTICA, NY and HAMBURG, GERMANY – JETNET, a leading provider of aviation data and market intelligence, today announced its acquisition of WINGX Advance GmbH (“WINGX”), a provider of market insight tools to the global business aviation sector. Together, the two companies plan to enrich the way customers access and utilize data and insights relating to the deployment, management, and utilization of business jets. WINGX is based in Hamburg, Germany.

“We are thrilled to welcome WINGX to the JETNET family,” stated Derek Swaim, CEO of JETNET. "WINGX offers innovative solutions for aggregating and visualizing flight activity and operational performance insights. WINGX’s configurable product offerings, industry expertise, and strong international presence make WINGX an ideal addition to JETNET."

By utilizing both JETNET’s and WINGX’s expertise and data, customers will gain expanded access to comprehensive coverage of the worldwide business aviation market. Customers will have the opportunity to access this information through dashboards specifically tailored to their own needs. Interactive portals to global fleet inventories and flight analysis, operating metrics, and supply-chain performance insights will provide users the ability to unlock valuable information and identify key trends. 

“WINGX is very excited to partner with JETNET and collaborate on building an outstanding hub for all-around market intelligence to serve the business aviation industry,” said Richard Koe, Co-Founder and Managing Director of WINGX. "JETNET is renowned for its superior research and aviation fleet database, and we look forward to creating more insights and actionable intelligence for our customers," added Christoph Kohler, joint Co-Founder of WINGX.

**About JETNET**

As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via APIs, real-time internet access and regular updates. 

For information on JETNET and our exclusive services, visit [www.jetnet.com](https://www.jetnet.com/) or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or [paul@jetnet.com](mailto:paul@jetnet.com) or Karim Derbala, JETNET Managing Director of Global Sales at +41 (0) 43.243.7056 (Switzerland) or [karim@jetnet.com](mailto:karim@jetnet.com).

**About WINGX Advance GmbH**

WINGX is a leading provider of data and market intelligence on the global business aviation market. WINGX aggregates and blends data on global flight activity, fleet distribution, fleet operating metrics, and delivers the information to customers through an extensive library of reports and modern interactive dashboards that customers can customize for their specific needs. WINGX’s customer base includes aircraft manufacturers, engine and avionics suppliers, airports, MRO businesses, aircraft operators, brokers, fuel suppliers, FBOs, insurance companies, banks, regulators, legal advisors, leasing companies, investors, and owners of business aircraft. With a focus on actionable intelligence and custom insights, WINGX enables clients to transform aviation data into a strategic advantage, driving growth and innovation. WINGX was founded in 2012 and is based in Hamburg, Germany. 

**Media Contact:**<br>
Justine Strzepek<br>
[justine@jetnet.com](mailto:justine@jetnet.com)

[Download the full release (PDF)](/assets/uploads/news/JETNET_WINGX_PR.pdf)
