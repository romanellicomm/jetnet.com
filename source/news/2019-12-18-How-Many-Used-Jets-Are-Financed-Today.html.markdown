---
title: How Many Used Jets Are Financed Today?
meta_description: 
opengraph:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
twitter_card:
  title: ""
  description: ""
  image: /assets/opengraph/.jpg
---


**As 2019 neared its end, what new trends had developed in new business jet deliveries and pre-owned sales? Mike Chase draws some highlights in his latest JETNET >> KNOW MORE review...**

READMORE

Under the US 2017 Tax Cuts & Jobs Act, taxpayers could deduct up to 100% of the cost of both new and used aircraft purchased after September 27, 2017 and placed in service before January 1, 2023.

After December 31, 2022 the Act decreases the percentage available each year by 20% to depreciate qualified aircraft until December 31, 2026. The question right now is whether this law has been a stimulus of growth for the business jet market?

**Business Jet Market Bifurcation**

<img src="/assets/uploads/images/2019-12-18-Chart-A.png" class="img-fluid mx-auto d-block" alt="Chart A">

Chart A (above) shows that in 2009, a separation began between the market values (B&CA equipped prices) of the business jets forming the top half of the segment, and those in the bottom half. That gap has not closed since.

Between 2008 and 2010, the market value for the bottom half bizjet segment declined sharply, but since 2011 has changed relatively little. While maintaining the gap, the top segment of business jets increased sharply between 2013 and 2015 and then dropped.

Estimates for 2019 indicate another sharp increase for the top half segment - the result of several new Large Cabin and Ultra-Long-Range Jets entering the market in 2019.

**New and Pre-Owned Business Jet Cycles**

<img src="/assets/uploads/images/2019-12-18-Chart-B.png" class="img-fluid mx-auto d-block" alt="Chart B">

New and pre-owned business jet cycles are different, as highlighted by Chart B (above) which illustrates the differences between 2003 and 2018.

One of the pre-owned market metrics we often consider is the fleet percentage for sale. This has steadily declined, largely because the total fleet has grown. More new business jets have been built, and fewer older jets have been retired.

The number of business jets for sale had dropped below 2,000 but is now rising again. As of September 2019, there were 2,187 used jets for sale (9.8% of the worldwide fleet). That compares with 2,961 for sale (17.7% of the fleet) in July 2009.

Although the fleet for sale percentage has been rising, we’re still in a traditional ‘seller’s market’ where less than 10% of the fleet is on the market.

Many aircraft brokers will attest that the reduction of pre-owned sales in 2019 has less to do with declining demand and more a lack of late model, clean aircraft appealing to today’s typical buyer.

**How are Business Jets Being Purchased?**

<img src="/assets/uploads/images/2019-12-18-Chart-C.png" class="img-fluid mx-auto d-block" alt="Chart C">

The percentage of pre-owned retail transactions for business jets that were financed (as opposed to cash- bought) was roughly a 50/50 split from 2000 leading up to the 2008 financial crisis. When the Great Recession came, securing debt financing became challenging for buyers.

Chart C shows that during the 18 months of the Great Recession that there was a 20% increase in cash purchases. Today, the ratio of transactions is 29% financed compared to 71% cash bought, according to JETNET’s research of actual financial documents filed at the FAA in Oklahoma City at the time of sale. This is relatively unchanged since June 2009.

Moreover, using data from Chart C we can establish there’s been growth in the average number of cash transactions but a decline in the financed transactions, as shown in Table A (below).

<img src="/assets/uploads/images/2019-12-18-Table-A.png" class="img-fluid mx-auto d-block" alt="Table A">

Since the great recession banks are more closely regulated (Basel III Accord), and the aircraft finance environment is very different than it was before the recession. This could be one of the underlying reasons for the shift toward cash transactions today. Other possible causes are the higher deposits required and personal loan guaranty from company executives.

Debt financing for pre-owned business jets is, of course, available today and there were over 450 banks and financial institutions that processed pre-owned business jet debt instruments (not including leases) in 2019, per JETNET.

**Twelve-Month Moving Average Retail Sale Transactions Analysis**

From January 2012, the twelve-month moving average for used business jet transactions steadily increased until June 2016, when it reached 2,725 units (Chart D, below). A falling-off occurred in H2 2016 when a low point of 2,531 transactions was reached. The twelve-month moving average subsequently increased again, peaking at 2,908 in December 2018.

<img src="/assets/uploads/images/2019-12-18-Chart-D.png" class="img-fluid mx-auto d-block" alt="Chart D">

Starting in January 2019, the moving average started to decline, and had dipped to 2,556 by September 2019, only slightly above the 2,531 recorded in December 2016.

Business Aviation has experienced a major change in the pre-owned market through Q3 2019. While the new jet market at Q3 2019 was up 15.4% Year-over-Year (per GAMA data), the pre-owned/used market was down 16.9% YoY.

**In Summary**

Has Bonus Depreciation been a stimulus of growth for the business jet sales market? We hope so, but we have not found a method for measuring it. What we do know is that cash purchases continue to be the preferred method among buyers.

As we continue through these unprecedented economic times, there’s no doubt that buying patterns, and the means of purchasing assets like business aircraft will continue to be impacted. Ten years after the effects of the great recession were felt in earnest, we can reflect on the following:

<ul>
  <li>During the 2003 market recovery, it took approximately three years for prices to bottom-out, languish, and then start to climb. It then took approximately five years (2003-2008) for prices to reach their peak.</li>
  <li>More than 10 years since aircraft values began to drop in mid-2008, prices are still at historic lows.</li>
  <li>US corporations and wealthy individuals have a stockpile of liquid cash available to purchase. However, demand has been very selective.</li>
  <li>New business jet models continue to be certified and generate interest in the marketplace, which is good news.</li>
</ul>


Ultimately, the trend to watch is the pre-owned business jet market’s downward trend. Expectations are for a strong Q4 to reverse the downward trend. We’ll keep you updated in future articles


[Download the full release (PDF)](/assets/uploads/news/AvBuyer-Market-Insights-2020-3.pdf)
