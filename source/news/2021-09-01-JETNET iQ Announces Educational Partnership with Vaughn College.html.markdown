---
title: JETNET iQ Announces Educational Partnership with Vaughn College for 2021 JETNET iQ Global Business Aviation Summit
meta_description: ""
opengraph:
  title:
  description:
  image:
twitter_card:
  title:
  description:
  image:
---

UTICA, NY – JETNET, the leading provider of corporate aviation information, has partnered with Vaughn College to encourage diversity and higher learning for students at the 2021 JETNET iQ Global Business Aviation Summit. New York City-based Vaughn College is a four-year private college offering programs in engineering, technology, management, and aviation. Three students have been selected to participate in the Summit, which will be held September 15–16, 2021, at the TWA Hotel at JFK Airport.
READMORE

“We are thrilled to partner with Vaughn College and have students take part in this year’s 2021 JETNET iQ Global Business Aviation Summit,” stated Paul Cardarelli, JETNET’s Vice President of Sales. He continued, “We are always looking for the next generation to step into their roles in business aviation, and lead us into the future. Students from Vaughn College represent a terrifically diverse background, which JETNET believes is essential for company and industry success. They will help pave the way and we hope their experience at the Summit will inspire them, and us, to new opportunities and aspirations within business aviation.”

The three students selected are Sadia Akhi, Sadia Harun, and Tommaso Rossi. Akhi and Harun are both majoring in Airport Management and Rossi is enrolled in the Airport/Airline Dual Major program. “I am very honored and excited to be participating in the annual JETNET iQ Summit,” said Saida Akhi. “This goes without saying, but I am blessed to have such an opportunity and more than eager to learn and understand the latest in industry trends and policies for the foreseeable future that is aviation.”

JETNET’s dedication to diversity is also present on their Summit’s agenda, including a key panel titled “The Importance of Workplace Diversity.” This panel includes Dr. Sharon DeVivo, President of Vaughn College and Chair of FAA’s Youth Access to American Jobs in Aviation Task Force (YIATF). Dr. DeVivo said, “Vaughn College serves an incredibly diverse population of students who become outstanding contributors to all facets of the aviation and aerospace industry.” She continued, “We are pleased to partner with JETNET iQ, an industry leader, during their annual conference, and are so appreciative of the opportunity provided to students to attend, network, and add to their educational experience.”

**About JETNET iQ**

JETNET iQ is an aviation market research, strategy, and forecasting service for the business aviation industry. JETNET iQ also provides independent, Quarterly intelligence, including consulting, economic and industry analyses, business aircraft owner/operator survey results, and new aircraft delivery and fleet forecasts. JETNET iQ is a collaboration between JETNET LLC and Rolland Vincent Associates LLC of Plano, Texas, an aviation market research consultancy.

**About JETNET**

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 108,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

For more information on the JETNET iQ, visit jetnet.com or contact Rolland Vincent, JETNET iQ Creator/Director, at 972.439.2069 or rollie@jetnet.com. For information on JETNET, visit jetnet.com or contact Paul Cardarelli, JETNET Vice President of Sales, at 800.553.8638 (USA) or paul@jetnet.com; International inquiries, contact Karim Derbala, JETNET Managing Director of Global Sales, at +41 (0) 43.243.7056 or karim@jetnet.com.

**About Vaughn College**

Founded in 1932, Vaughn College is a private, nonprofit, four-year college that enrolls more than 1,500 students in master’s, bachelor’s, and associate degree programs in engineering, technology, management, and aviation on its main campus in New York City and online. The student-faculty ratio of 14 to 1 ensures a highly personalized learning environment. Ninety-nine percent of Vaughn College graduates are placed in professional positions, 89 percent in their field of study, or choose to continue their education within one year of graduation. The institution serves many first-generation college students and is recognized by the U.S. Department of Education as a Hispanic-Serving Institution. Vaughn was ranked no. 1 in upward mobility nationwide in a study conducted by The Equality of Opportunity Project.

[Download the full release (PDF)](/assets/uploads/news/JETNET iQ Vaughn College.pdf)
