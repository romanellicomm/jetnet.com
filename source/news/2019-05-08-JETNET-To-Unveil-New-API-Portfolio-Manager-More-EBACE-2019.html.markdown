---
title: JETNET To Unveil New API, Portfolio Manager, More At EBACE 2019
meta_description: 
opengraph:
  title: "See What’s New at EBACE"
  description: "Live demos"
  image: /assets/opengraph/May8_EBACE_FB.jpg
twitter_card:
  title: "See What’s New at EBACE"
  description: "Live demos"
  image: /assets/opengraph/May8_EBACE_TW.jpg
---

UTICA, NY – JETNET LLC, the world leader in aviation market intelligence, will be featuring a series of new products and service enhancements at EBACE2019 at the Geneva Palexpo in Geneva, Switzerland, including their JETNET API and Portfolio Manager. JETNET API allows clients to create customizable data delivery solutions using JETNET’s database. Portfolio Manager provides users with the flexibility to organize, review, and analyze aircraft fleets. In addition to featuring these new releases, JETNET will be holding their annual JETNET iQ European State of the Business Jet Market on May 21 at the Palexpo. They will be demonstrating their full suite of software at the European Business Aviation Convention & Exhibition, May 21-23, at booth #W127 for aviation professionals focused on the European theater of operations.

READMORE

“We are providing the tools and data that professionals need in the European aviation market,” said Karim Derbala, JETNET Managing Director of Global Sales. “We want to enable them to take full advantage of the growing optimism in global markets.”

**JETNET API: Customizable Data Delivery Solutions**

JETNET API (Application Programming Interface) is a flexible application program interface that allows clients to build their own complex custom solutions powered by JETNET’s data. Users can also explore and design proprietary programs able to connect with and manage the data delivered via the firm’s RESTful API, including CRMs, market analysis tools, transaction trends, aircraft inventory programs, fleet management activities, or other complex applications.

“JETNET API is our way of giving users even more opportunity to put the full power of JETNET’s extensive data to work for them in their current applications,” said Tony Esposito, JETNET President. “With JETNET API, our clients will be more efficient, and be able to find their answers more quickly.”

**Flight Activity Report**

JETNET’S Marketplace and AERODEX Elite products provide the flexibility to organize and analyze individual aircraft, fleet, or operator/company historical flight activity by arrivals or departures—or both—for selected airports worldwide. Users can now create a detailed report with the number of flights per month; top origin and destination airports, countries, and continents; fuel/tech stops; top operators by business type, country, and continent; and top aircraft by serial number with the seat capacity for crew and passengers.

**New Portfolio Manager**

JETNET’s Portfolio Manager provides clients with the ability to organize, review, and analyze even the most complex fleets of aircraft. They can build their fleet based on their needs (using aircraft folders) and then review it based on a wide variety of perspectives, such as age of fleet, value of fleet, maintenance analytics, flight activity, owners, operations, models, history, and more, as well as review existing corporate fleets.

**New Aircraft Prospector Tool**

JETNET Evolution Marketplace clients can now use the company’s comprehensive historical transaction database to target companies most likely to purchase a given make/model of aircraft. Saving hours of research, the new tool correlates the make/model specific length of ownership data against the length of time for the current owner, as well as identifying companies which have sold aircraft and not bought another, companies with expiring leases or fractions, and more.

**EASA Part 145 Maintained Aircraft Search**

JETNET allows users to find aircraft maintained under the European Union Aviation Safety Agency Part 145 by searching and identifying them along with the company contact information of the professionals with responsibilities to operate, maintain, finance, insure, lease, service, crew, and pilot each aircraft.

**JETNET Values and eValues**

JETNET Evolution Marketplace and Marketplace Manager subscribers can improve their ability to price and compare aircraft in the resale market with the JETNET Values add-on, which provides actual reported sold prices on aircraft transactions, as well as tools for better analysis of pricing data. JETNET has further enhanced the tool with eValuesTM, developed in collaboration with Asset Insight, LLC, which adds mark-to- market and projected residual values for a wide range of business aircraft, including Current Market Values and 60-month Estimated Residual Value figures.

**JETNET iQ European State of the Business Jet Market Briefing**

JETNET invites attendees to see all of their new product and service offerings at this year’s EBACE, May 21- 23, at booth #W127. All business aviation professionals and press are also invited to attend their annual JETNET iQ European State of the Business Jet Market briefing on Tuesday, May 21 from 11:30am-12:15pm in Salle R at Palexpo.

JETNET will also be holding their 9th Annual JETNET iQ Global Business Aviation Summit June 4-5 at The Ritz-Carton New York, Westchester in White Plains, NY.

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/2019-JETNET-EBACE.pdf)
