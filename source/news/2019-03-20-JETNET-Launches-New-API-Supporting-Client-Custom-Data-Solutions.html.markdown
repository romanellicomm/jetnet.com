---
title: JETNET Launches New API Supporting Client Custom Data Solutions
meta_description: 
opengraph:
  title: ""
  description: ""
  image:
twitter_card:
  title: ""
  description: ""
  image:
---

UTICA, NY – JETNET LLC, the leading provider of aviation market information, has launched a new application program interface, or API, dubbed JETNET API, to allow their users to build their own custom data solutions and applications. Subscribers can also contract with JETNET to provide custom applications to their specifications.

READMORE

JETNET API is a flexible interface allowing users to build complex solutions powered by JETNET’s world class fixed and rotary wing data. When a client needs to build custom data applications to interface with their own internal management systems, customer relationship management (CRM) systems (such as Salesforce, Microsoft Dynamics, ZoHo, etc.), or other non-competing databases and services, they can subscribe to the JETNET API to service this need.

“JETNET API is robust, flexible, and available now,” said Mike Foye, JETNET Director of Marketing. “If our subscribers are interested or don’t have the programming expertise in-house, we can help them design a custom connection between their company and JETNET API.”

Since JETNET API is a customized data delivery product, users can begin the estimate process by contacting JETNET’s API product specialists to review the amount and types of data, frequency of updates, and other special considerations they require, and JETNET will generate a proposal.

About JETNET

As the leading provider of aviation market information, JETNET delivers the most comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source for information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprised of more than 110,000 airframes. Headquartered in its state-of-the- art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

[Download the full release (PDF)](/assets/uploads/news/JETNET-Launches-API.pdf)
