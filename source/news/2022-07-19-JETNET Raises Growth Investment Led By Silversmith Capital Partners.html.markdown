---
title: JETNET Raises Growth Investment Led By Silversmith Capital Partners
opengraph:
  title: ""
  description: "Learn More"
  image: /assets/opengraph/
twitter_card:
  title: ""
  description: "Learn More"
  image: /assets/opengraph/
---

**UTICA, NY** – July 19, 2022 – JETNET, a leading provider of aviation data and market intelligence, today announced that it has secured a majority growth investment led by Silversmith Capital Partners, a Boston-based growth equity firm. The investment marks the first time JETNET has raised outside capital since its founding in 1988. The partnership with Silversmith will enable JETNET to expand its product footprint, invest in sales and marketing, and scale the team.

“We are thrilled to partner with Silversmith for the next chapter of JETNET’s growth,” said Tony Esposito, minority owner and member of the Board of Directors. “Since my father founded the company 34 years ago, we have built JETNET with a relentless focus on our customers and our culture. Partnering with an investor for the first time was not a decision we took lightly. In Silversmith we found a partner who not only brings the expertise we need to continue to grow, but also shared the values that have served as the company’s foundation.”

JETNET offers the most comprehensive and reliable database of business and commercial aviation information to its global customer base. Working directly with operators, owners, and manufacturers, JETNET maintains the most accurate information in the industry on pricing, availability, and location of mission-critical aviation resources. In December 2021, the company hired aviation executive Greg Fell as its first Chief Executive Officer.

“I made the decision to join JETNET because as a long-time user of the company’s services, I believed that the platform was not only the best product offering in the industry, but that it was well positioned to see continued organic growth across many end markets,” said JETNET Chief Executive Officer, Greg Fell. “Silversmith’s extensive experience in helping information companies accelerate growth both organically and inorganically will be invaluable to our team as we look to expand our research capabilities and global footprint.”

“JETNET has done an exceptional job building a best-in-class solution that customers love,” said Jim Quagliaroli, Managing Partner of Silversmith. “The JETNET platform powers the day-to-day operations of thousands of customers around the world, and we are excited to support the company in its next phase of growth.”

Silversmith Vice President Ned Kingsley added, “As a profitable, growing business providing data and information services to a global market, JETNET is exactly the type of business which Silversmith seeks to support.”

Quagliaroli, Kingsley, and Silversmith Senior Associate Becca Katzen have joined the company’s Board of Directors alongside Esposito and Fell. Kirkland & Ellis served as legal counsel to Silversmith Capital Partners and Koley Jessen served as legal counsel to JETNET.

**About JETNET**<br>
As a leading provider of aviation market information, JETNET delivers comprehensive and reliable business aircraft research to its exclusive clientele of aviation professionals worldwide. JETNET is the ultimate source of information and intelligence on the worldwide business, commercial, and helicopter aircraft fleet and marketplace, comprising more than 110,000 airframes. Headquartered in its state-of-the-art facility in Utica, NY, JETNET offers comprehensive, user-friendly aircraft data via real-time internet access or regular updates.

**About Silversmith Capital Partners**<br>
Founded in 2015, Silversmith Capital Partners is a Boston-based growth equity firm with $2.0 billion of capital under management. Silversmith’s mission is to partner with and support the best entrepreneurs in growing, profitable technology and healthcare companies. Representative investments include ActiveCampaign, DistroKid, impact.com, Iodine Software, LifeStance Health, and Webflow. The partners have served on the boards of numerous successful growth companies including ABILITY Network, Dealer.com, Liberty Dialysis, Passport Health, SurveyMonkey, and Wrike. For more information about Silversmith, please visit [www.silversmith.com](https://www.silversmith.com/) or follow the firm on LinkedIn.

**Media Contact**<br>
Kate Castle<br>
Silversmith Capital Partners<br>
Chief Marketing Officer<br>
P: 617-670-4345<br>
[kate@silversmith.com](mailto:kate@silversmith.com)
