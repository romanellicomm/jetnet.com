---
title: "Here Comes the Sun"
issue: 6
pdf: "/assets/uploads/iq-pulse/JETNET iQ Pulse - June 18 2020.pdf"
meta_description:
next_issue_teaser: A message from Tim Obitts, President and CEO of NATA, the National Air Transportation Association.
opengraph:
  title: "JETNET iQ PULSE"
  description: "Free Business Aviation Insights"
  image: /assets/opengraph/Pulse_Issue2_FB.jpg
twitter_card:
  title: "JETNET iQ PULSE"
  description: "Free Business Aviation Insights"
  image: /assets/opengraph/Pulse_Issue2_TW.jpg
---

As the obstacles of the pandemic continue to upend the realities of a swift return to “normal”, our industry explores innovative ways to better serve our customers. In this issue, Pete Bunce, GAMA’s President and CEO, shares his insights on the ways in which the Business & General Aviation industry is preparing for a return to business.

READMORE

This issue of JETNET iQ PULSE is brought to you by:

<a href="https://www.embraer.com/">
  <img src="/assets/uploads/iq-pulse/Embraer_SponsorBlock_HR.jpg" alt="Embraer" width="400" class="img-fluid" />
</a>
