---
title: "The Rules Of The Game Have Changed"
issue: 1
pdf: "/assets/uploads/iq-pulse/JETNET iQ Pulse - April 7 2020.pdf"
meta_description:
opengraph:
  title: "JETNET iQ PULSE"
  description: "Free Business Aviation Insights"
  image: /assets/opengraph/Pulse_Issue1_FB.jpg
twitter_card:
  title: "JETNET iQ PULSE"
  description: "Free Business Aviation Insights"
  image: /assets/opengraph/Pulse_Issue1_TW.jpg
---
With the back-to-back cancellation of every key industry gathering in recent weeks, business aviation leaders have not lost their appetite for market and competitive intelligence. Leveraging our core competencies in aviation market research, strategy, and forecasting, we are proud to launch JETNET iQ PULSE. We trust that you will find insights here to safely and intelligently navigate the skies ahead.
