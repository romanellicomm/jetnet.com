---
title: "I’ve Been Afraid of Changin’"
issue: 34
pdf: "/assets/uploads/iq-pulse/JETNET iQ Pulse - July 29 2021.pdf"
meta_description: "Teams that have adaptation in their DNA give themselves the best chances of operating now and into the future. Leadership expert David Porter says it all starts by asking the right questions. #KnowMore"
next_issue_teaser:
opengraph:
  title: "Effective Approaches To Leadership"
  description: "Embrace Change"
  image: /assets/opengraph/Pulse34_FB.jpg
twitter_card:
  title: "Effective Approaches To Leadership"
  description: "Embrace Change"
  image: /assets/opengraph/Pulse34_TW.jpg
---

Fleetwood Mac’s self-examining song, <i>Landslide</i>, was written by Stevie Nicks just months before she joined the band. Her cleaning rags-to-riches path to success, shortly after fronting Fleetwood Mac, offers a foundational element of leadership for the ages. Leadership in any industry is both an art and a science. Perhaps the most effective approach to ready ourselves and our teams for change is to start by simply asking the right questions.

In this issue, David Porter, leadership expert, Founder, and President of David Porter Advisors, discusses a topic of widespread interest during these unprecedented times—leading momentous change.

READMORE

This issue of JETNET iQ PULSE is brought to you by:

<a href="https://embraer.com/global/en">
  <img src="/assets/uploads/iq-pulse/Embraer_SponsorBlock_HR.jpg" alt="Embraer" width="400" class="img-fluid" />
</a>
