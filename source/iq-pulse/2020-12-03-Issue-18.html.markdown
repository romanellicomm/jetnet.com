---
title: "Like A Bridge Over Troubled Water"
issue: 18
pdf: "/assets/uploads/iq-pulse/JETNET iQ Pulse - December 3 2020.pdf"
meta_description:
next_issue_teaser: "A message from David Crick, Managing Director at DavAir Group."
opengraph:
  title: "Business Aviation Insights"
  description: "Take Advantage Of This Free Report"
  image: /assets/opengraph/Dec8_FB.jpg
twitter_card:
  title: "Business Aviation Insights"
  description: "Take Advantage Of This Free Report"
  image: /assets/opengraph/Dec8_TW.jpg
---

Joining the ranks of earlier-trailblazer FedEx, modern-day logistics experts like Quest Diagnostics, LabCorp and Pfizer, among others, transport diagnostic COVID-19 vaccination test samples, and operate pandemic and disaster relief flights, a real-life endorsement of the many tangible and intangible benefits of business and general aviation aircraft.

In this issue, Bob Zuskin, Founder and CEO of Jet Perspectives, and one of the industry’s leading aircraft appraisers, offers his refreshingly global perspective on the markets, and shares his thoughts on topics of enduring interest for aviation stakeholders, with special relevance to today’s commercial and business aircraft sectors.

READMORE

This issue of JETNET iQ PULSE is brought to you by:

<a href="https://txtav.com/">
  <img src="/assets/uploads/iq-pulse/TextronAviation_SponsorBlock.jpg" alt="Textron Aviation" width="400" class="img-fluid" />
</a>
