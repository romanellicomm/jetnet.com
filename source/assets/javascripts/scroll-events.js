import "../../node_modules/waypoints/lib/jquery.waypoints.min"
import "../../node_modules/waypoints/lib/shortcuts/inview.min"

function fadeInArrows(selector) {
  var $arrows = jQuery(selector)
  if ($arrows.length === 0) return

  $arrows.addClass("animated").css("opacity", 0)
  var waypointDown = new Waypoint({
    element: $arrows.get(0),
    handler: function(direction) {
      if (direction === "down") {
        $arrows.addClass("fadeInLeft")
      }
    },
    offset: "80%"
  })

  var waypointUp = new Waypoint({
    element: $arrows.get(0),
    handler: function(direction) {
      if (direction === "up") {
        $arrows.addClass("fadeInLeft")
      }
    },
    offset: "20%"
  })

  var inview = new Waypoint.Inview({
    element: $arrows.get(0),
    exited: function(direction) {
      $arrows.removeClass("fadeInLeft").css("opacity", 0)
    }
  })
}

fadeInArrows(".index-who-we-are .icon--double-arrow")
fadeInArrows(".index-know-more .icon--double-arrow")
//fadeInArrows('.about-mission .icon--double-arrow-outset');
//fadeInArrows('.about-history .icon--double-arrow-outset');
