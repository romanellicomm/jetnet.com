import React from "react";
import ReactDOM from "react-dom";
import Lightbox from "react-image-lightbox";

export default class MarketReportGallery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
    };
  }

  handleClick(e, photoIndex) {
    e.preventDefault();
    this.setState({
      photoIndex: photoIndex,
      isOpen: true,
    });
    return false;
  }

  render() {
    const images = this.props.images;
    const { photoIndex, isOpen } = this.state;

    return (
      <div className="gallery">
        {images.map((image, photoIndex) => {
          return (
            <a key={photoIndex} href="#" onClick={(e) => this.handleClick(e, photoIndex)}>
              <img
                src={image.thumb}
                width="150"
                height="auto"
                className="galleryImage"
              />
            </a>
          );
        })}

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex].full}
            nextSrc={images[(photoIndex + 1) % images.length].full}
            prevSrc={
              images[(photoIndex + images.length - 1) % images.length].full
            }
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
          />
        )}
      </div>
    );
  }
}

const rootElement = document.getElementById("MarketReportGallery");
if (rootElement) {
  const imgs = Array.from(rootElement.children);
  const images = imgs.map((image) => {
    return {
      thumb: image.dataset.thumb,
      full: image.src
    }
  })
  ReactDOM.render(<MarketReportGallery images={images} />, rootElement);
}
