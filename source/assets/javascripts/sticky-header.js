import "waypoints/lib/noframework.waypoints.min.js"
import scrollStop from "./scrollStop.js"

const siteHeader = document.querySelector(".site-header")

const sticky = new Waypoint({
  element: siteHeader,
  handler: function(direction) {
    switch (direction) {
      case "up":
        siteHeader.classList.remove("stuck")
      case "down":
        siteHeader.classList.add("stuck")
    }
  }
})

const getDropType = function() {
  const vh = window.innerHeight
  const headerRect = siteHeader.getBoundingClientRect()
  return headerRect.top > vh / 2 ? "dropup" : "dropdown"
}

const updateDropType = function(navElement, direction) {
  const navItems = navElement.querySelectorAll(".navbar-nav > .nav-item")
  switch (direction) {
    case "dropup":
      ;[].forEach.call(navItems, function(item) {
        item.classList.remove("dropdown")
        item.classList.add("dropup")
      })
      break
    case "dropdown":
      ;[].forEach.call(navItems, function(item) {
        item.classList.remove("dropup")
        item.classList.add("dropdown")
      })
      break
  }
}

scrollStop(function() {
  updateDropType(siteHeader, getDropType())
})

updateDropType(siteHeader, getDropType())
