import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";

export default class SummitPricing extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      time: moment()
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick() {
    this.setState({
      time: moment()
    });
  }

  render() {
    var currentRate = "regular";
    if (this.state.time.isBefore("2020-02-01 00:00:01")) {
      currentRate = "earlyBird";
    }

    return (
      <div className="iq-summit-pricing">

        {currentRate === "earlyBird" && (
          <ul class="list--double-arrow">
            <li>A very limited number of seats are available. <span style={{ color: "#ffea00" }}>Reserve by January 31, 2020 for our early bird rate of $895 and save $200 off our regular rate of $1,095.</span></li>
          </ul>
        )}

        {currentRate === "regular" && (
          <ul class="list--double-arrow">
            <li>A very limited number of seats are available. Reserve your seat today.</li>
          </ul>
        )}

      </div>
    );
  }
}

const rootElement = document.getElementById("SummitPricing");
if (rootElement) {
  ReactDOM.render(<SummitPricing />, rootElement);
}
