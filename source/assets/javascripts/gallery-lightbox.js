import { Luminous, LuminousGallery } from 'luminous-lightbox'

const ratio = window.innerHeight / window.innerWidth
const isMobile = ratio > 1.5

/* Turns out this is not a great way to detect mobile, because
   it fails on my desktop monitor turned vertical. Hmph. */
if (!isMobile) {
  initGalleries()
} else {
  const galleryImageLinks = document.querySelectorAll("[data-gallery]")
  galleryImageLinks.forEach(a => {
    const parent = a.parentNode;
    while (a.firstChild) parent.insertBefore(a.firstChild, a)
    parent.removeChild(a)
  })
}

function initGalleries() {

  const galleryImages = document.querySelectorAll("[data-gallery]")
  const galleries = new Set(Array.from(galleryImages).map(n => n.dataset.gallery))
  
  console.log({galleries})

  galleries.forEach(galleryID => {
    const images = document.querySelectorAll(`[data-gallery='${galleryID}']`)

    if (images.length == 1) {
      new Luminous(images[0], {
        caption: el => el.dataset.caption
      })
    } else {
      new LuminousGallery(images, {}, {
        caption: el => el.dataset.caption
      })
    }
  })

}
