import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";

export default class SummitBanner extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      time: moment()
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick() {
    this.setState({
      time: moment()
    });
  }

  render() {
    var currentRate = "regular";
    if (this.state.time.isBefore("2020-02-01 00:00:01")) {
      currentRate = "earlyBird";
    }

    return (
      <div className="iq-summit-banner">

        {currentRate === "earlyBird" && (
          <div className="summit-banner text-center">
            <a href="/summit">
              Register for the <span className="banner-callouts font-weight-bold">2020 JETNET iQ Summit</span> by <span className="banner-callouts font-weight-bold">Jan. 31, 2020</span> for the early bird rate of $895—<span className="banner-callouts font-weight-bold">$200 off our regular rate.</span>
            </a>
          </div>
        )}

        {currentRate === "regular" && (
          <div className="summit-banner text-center">
            <a href="/summit">
              Register today for the <span className="banner-callouts font-weight-bold">2020 JETNET iQ Summit</span> and see and meet business aviation’s leaders.
            </a>
          </div>
        )}

      </div>
    );
  }
}

const rootElement = document.getElementById("SummitBanner");
if (rootElement) {
  ReactDOM.render(<SummitBanner />, rootElement);
}
