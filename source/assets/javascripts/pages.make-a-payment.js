import React from "react"
import ReactDOM from "react-dom"

class PaymentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      customerName: "",
      customerId: "",
      invoiceNumber: "",
      description: ""
    }

    this.handleInputChange = this.handleInputChange.bind(this)
  }

  handleInputChange(event) {
    const name = event.target.name

    this.setState({
      [name]: event.target.value
    })
  }

  generateDescription() {
    const inputs = {
      "Customer Name": this.state.customerName,
      "Customer ID": this.state.customerId,
      "Invoice Number": this.state.invoiceNumber
    }

    return Object.keys(inputs)
      .filter(key => Boolean(inputs[key]))
      .map(key => `${key}: ${inputs[key].trim()}`)
      .join(", \r\n")
  }

  render() {
    return (
      <form action={this.props.action} method="POST">
        <div className="form-group row">
          <label className="col-sm-4 col-md-3 col-lg-4 col-xl-3">Customer Name</label>
          <div className="col-sm-8 col-md-6 col-lg-8">
            <input
              type="text"
              className="form-control"
              name="customerName"
              id="customerNameInput"
              value={this.state.customerName}
              onChange={this.handleInputChange}
              required
            />
            <small id="customerNameHelp" className="form-text text-muted">
              Customer name or company name
            </small>
          </div>
        </div>

        <div className="form-group row">
          <label
            className="col-sm-4 col-md-3 col-lg-4 col-xl-3"
            htmlFor="customerIdInput"
          >
            Customer ID <small class="text-muted">(optional)</small>
          </label>
          <div className="col-sm-6 col-md-4">
            <input
              type="text"
              className="form-control"
              name="customerId"
              id="customerIdInput"
              value={this.state.customerId}
              onChange={this.handleInputChange}
            />
          </div>
        </div>

        <div className="form-group row">
          <label
            className="col-sm-4 col-md-3 col-lg-4 col-xl-3"
            htmlFor="invoiceNumberInput"
          >
            Invoice Number <small class="text-muted">(optional)</small>
          </label>
          <div className="col-sm-6 col-md-4">
            <input
              type="text"
              className="form-control"
              name="invoiceNumber"
              id="invoiceNumberInput"
              value={this.state.invoiceNumber}
              onChange={this.handleInputChange}
            />
          </div>
        </div>

        <div className="form-group row d-none">
          <label
            className="col-sm-4 col-md-3 col-lg-4 col-xl-3"
            htmlFor="descriptionInput"
          >
            Description
          </label>
          <div className="col-sm-12 col-md-8">
            <textarea
              rows="3"
              type="text"
              className="form-control"
              name="ItemDesc"
              value={this.generateDescription()}
              id="descriptionInput"
            />
          </div>
        </div>

        <div className="form-group row">
          <label className="col-sm-4 col-md-3 col-lg-4 col-xl-3" htmlFor="amountInput">
            Payment Amount
          </label>
          <div className="col-sm-4 col-md-3">
            <div className="input-group">
              <span className="input-group-addon">$</span>
              <input
                type="number"
                step="any"
                className="form-control"
                name="ItemCost"
                defaultValue=""
                id="amountInput"
                required
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-4 offset-sm-4 offset-md-3 offset-lg-3 offset-xl-3">
            <button type="submit" className="btn btn-primary">
              Continue&#8230;
            </button>
          </div>
        </div>

        <input type="hidden" name="ItemQty" value="1" />
        <input type="hidden" name="ePNAccount" value={this.props.accountId} />
        <input type="hidden" name="ReturnToURL" value={this.props.returnUrl} />
        <input type="hidden" name="BackgroundColor" value="White" />
        <input type="hidden" name="TextColor" value="Black" />
      </form>
    )
  }
}

const rootElement = document.getElementById("paymentForm")
if (rootElement) {
  ReactDOM.render(
    <PaymentForm
      action="https://www.eprocessingnetwork.com/cgi-bin/wo/order.pl"
      accountId="0416305" // eProcessingNetwork account ID
      returnUrl="https://www.jetnet.com/make-a-payment/confirm.html"
    />,
    rootElement
  )
}
