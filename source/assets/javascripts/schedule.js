document.addEventListener('DOMContentLoaded', function() {
  const scheduledElements = document.querySelectorAll('[data-schedule]');
  
  scheduledElements.forEach((el) => {
    const now = new Date();
    const startDateAttr = el.getAttribute('data-schedule-start');
    const endDateAttr = el.getAttribute('data-schedule-end');
    const startDate = startDateAttr ? new Date(startDateAttr) : null;
    const endDate = endDateAttr ? new Date(endDateAttr) : null;

    // Hide by default
    el.style.display = 'none';
    
    // No start or end date, hide and return early
    if (!startDate && !endDate) return;
    
    // Only start date is provided and it's in the past
    if (startDate && !endDate && now >= startDate) {
      el.style.display = 'initial';
      return;
    }
    
    // Only end date is provided and it's in the future
    if (!startDate && endDate && now <= endDate) {
      el.style.display = 'initial';
      return;
    }

    // Both start and end dates are provided
    if (startDate && endDate && now >= startDate && now <= endDate) {
      el.style.display = 'initial';
      return;
    }
  });
});
