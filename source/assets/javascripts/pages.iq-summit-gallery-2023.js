import React from "react";
import ReactDOM from "react-dom";
import Lightbox from "react-image-lightbox";

const TOTAL_IMAGES = 98;

function range(start, end) {
  return new Array(end - start + 1).fill().map((d, i) => i + start);
}

function imageObject(i) {
  return {
    thumb: `/assets/images/summit/galleries/iq-summit-2023-gallery-${i}.jpg`,
    large: `/assets/images/summit/galleries/iq-summit-2023-gallery-${i}.jpg`
  };
}

const galleryImages = range(1, TOTAL_IMAGES).map(i => imageObject(i));

export default class SummitGallery2023 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      galleryIndex: 0,
      isOpen: false
    };
  }

  launchGallery(e, galleryIndex) {
    e.preventDefault();
    this.setState({
      galleryIndex: galleryIndex,
      isOpen: true
    });
    return false;
  }

  render() {
    const { galleryIndex, isOpen } = this.state;

    return (
      <div className="iq-summit-gallery">
        <div className="container">
          <div className="row">
            <div className="offset-lg-3 col-lg-9 mb-5">
              <br />
              <h2>2023 Summit Snapshots</h2>

              <img
                src="/assets/images/summit/galleries/iq-summit-2023-gallery-header.jpg"
                className="img-fluid d-block mb-3"
                alt="iQ Summit"
              />

              <p>See photos from the 12th JETNET iQ Summit that marked our return to the beautifully retro TWA Hotel at JFK.</p>

              <a
                className="btn btn-primary"
                href="#"
                onClick={e => this.launchGallery(e, this.state.galleryIndex)}
              >
                <span className="icon icon--double-arrow" /> View Gallery
              </a>
            </div>
          </div>
        </div>

        <div>
          {isOpen && (
            <Lightbox
              mainSrc={galleryImages[galleryIndex].large}
              nextSrc={
                galleryImages[(galleryIndex + 1) % galleryImages.length].large
              }
              prevSrc={
                galleryImages[
                  (galleryIndex + galleryImages.length - 1) %
                    galleryImages.length
                ].large
              }
              onCloseRequest={() => this.setState({ isOpen: false })}
              onMovePrevRequest={() =>
                this.setState({
                  galleryIndex:
                    (galleryIndex + galleryImages.length - 1) %
                    galleryImages.length
                })
              }
              onMoveNextRequest={() =>
                this.setState({
                  galleryIndex: (galleryIndex + 1) % galleryImages.length
                })
              }
            />
          )}
        </div>
      </div>
    );
  }
}

const rootElement = document.getElementById("SummitGallery2023");
if (rootElement) {
  ReactDOM.render(<SummitGallery2023 />, rootElement);
}
