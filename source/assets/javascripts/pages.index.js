import Choreographer from "choreographer-js"

jQuery(function() {
  const vh = document.documentElement.clientHeight

  const intro_foreground_selector = ".index-intro .hero__parallax__foreground"
  const intro_foreground = document.querySelector(intro_foreground_selector)

  if (intro_foreground !== null) {
    const introChoreographer = new Choreographer({
      animations: [
        {
          range: [
            intro_foreground.offsetTop,
            intro_foreground.offsetTop +
              intro_foreground.offsetHeight +
              document.documentElement.clientHeight / 2
          ],
          selector: intro_foreground_selector,
          type: "scale",
          style: "transform:translateY",
          from: 0,
          to: 75,
          unit: "vh"
        },
        {
          range: [
            intro_foreground.offsetTop,
            intro_foreground.offsetTop + intro_foreground.offsetHeight
          ],
          selector: intro_foreground_selector,
          type: "scale",
          style: "opacity",
          from: 1,
          to: 0
        }
      ]
    })

    window.addEventListener("scroll", function() {
      introChoreographer.runAnimationsAt(window.pageYOffset)
    })
  }

  const know_more_fg_selector = ".index-know-more .hero__parallax__foreground"
  const know_more_fg = document.querySelector(know_more_fg_selector)
  //const know_more_bg_selector = ".index-know-more .hero__parallax__background"
  //const know_more_bg = document.querySelector(know_more_bg_selector)

  if (know_more_fg !== null) {
    const knowMoreChoreographer = new Choreographer({
      animations: [
        {
          range: [
            window.scrollY + know_more_fg.getBoundingClientRect().top - vh,
            window.scrollY + know_more_fg.getBoundingClientRect().bottom
          ],
          selector: know_more_fg_selector,
          type: "scale",
          style: "transform:translateY",
          from: -15,
          to: 15,
          unit: "vh"
        },
        {
          range: [
            window.scrollY + know_more_fg.getBoundingClientRect().top,
            window.scrollY + know_more_fg.getBoundingClientRect().bottom
          ],
          selector: know_more_fg_selector,
          type: "scale",
          style: "opacity",
          from: 1,
          to: 0
        }
      ]
    })

    window.addEventListener("scroll", function() {
      knowMoreChoreographer.runAnimationsAt(window.pageYOffset)
    })
  }
})
