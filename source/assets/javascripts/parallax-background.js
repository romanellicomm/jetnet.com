import Choreographer from "choreographer-js"

jQuery(function() {
  const vh = window.innerHeight
  //const dh = document.body.scrollHeight
  const dh = jQuery(".parallax-wrapper").height()

  const choreographer = new Choreographer({
    animations: [
      {
        range: [-1, vh * 1.5],
        selector: ".parallax-background-top",
        type: "scale",
        style: "transform:translateY",
        from: 0,
        to: 50,
        unit: "vh"
      },
      {
        range: [dh - vh * 2, dh - vh],
        selector: ".parallax-background-bottom",
        type: "scale",
        style: "transform:translateY",
        from: -50,
        to: 0,
        unit: "vh"
      }
    ]
  })

  window.addEventListener("scroll", function() {
    choreographer.runAnimationsAt(window.pageYOffset)
  })
})
