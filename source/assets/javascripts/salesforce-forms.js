// Custom pipeline function that resolves even if a step fails because
// for our use-case, capturing the form submissions is absolutely critical.
function pipeline(steps) {
  return steps.reduce((promise, step) => {
    return promise.then(step).catch(error => {
      console.error('Pipeline step failed:', error);
      return Promise.resolve(); // Resolve the promise even on failure
    });
  }, Promise.resolve());
}

function addHiddenField(form, name, value) {
  var inputField = document.createElement('input');
  inputField.type = 'hidden';
  inputField.name = name;
  inputField.value = value;
  form.appendChild(inputField);
}

function initOutdatedWufooTracking(event) {
  return new Promise((resolve, reject) => {
    // Parse the URL parameters for referrer title and URL
    var urlParams = new URLSearchParams(window.location.search);
    var referrerTitle = urlParams.get('referrer_title');
    var referrerUrl = urlParams.get('referrer_url');
  
    // If the parameters exist, add the hidden fields to the form
    if (referrerTitle && referrerUrl) {
      var form = document.querySelector('form[data-salesforce]');
      addHiddenField(form, 'Previous_Page_Title__c', decodeURIComponent(referrerTitle));
      addHiddenField(form, 'Previous_Page_URL__c',   decodeURIComponent(referrerUrl));
    }
    resolve();
  });
}

function initSalesforceTracking(form) {
  return new Promise((resolve, reject) => {
    getGA4ClientId().then(client_id => {
      addHiddenField(form, 'GA4_Client_ID__c', client_id);
      resolve();
    });
  });
}

function initSalesforceRecaptcha(form) {
  return new Promise((resolve, reject) => {
    grecaptcha.ready(() => {
      grecaptcha.execute('6LdYNhomAAAAAAv2ELrgop5YzE5O-jjSvaVhFbNx', {action: 'submit'}).then(token => {
        addHiddenField(form, 'g-recaptcha-response', token);
        resolve();
      }).catch(reject);
    });
  });
}

function submitToPipedream(form) {
  var formData = new FormData(form);
  var pipedreamEndpoint = form.dataset.pipedreamEndpoint;

  fetch(pipedreamEndpoint, {
    method: 'POST',
    body: formData
  }).then(() => {
    console.log('Request sent to Pipedream.');
  }).catch(error => {
    console.error('Error sending data to Pipedream: ', error);
  });
  
  return Promise.resolve();
}

function initSalesforceForm(form) {
  pipeline([
    () => initSalesforceTracking(form),
    () => initOutdatedWufooTracking(form),
  ]).then(() => {
    console.log('Form initialized.');
  });
}

function handleSalesforceFormSubmission(event) {
  event.preventDefault();
  var form = event.target;

  pipeline([
    () => initSalesforceRecaptcha(form),
    () => submitToPipedream(form),
  ]).then(() => {
    form.submit();
  });
}

document.addEventListener("DOMContentLoaded", function() {
  var form = document.querySelector('form[data-salesforce]');
  
  if (form) {
    initSalesforceForm(form);
    form.addEventListener('submit', handleSalesforceFormSubmission);
  }
});