import "cookieconsent";

if (window.location.pathname == '/cookie-banner.html') {

  window.cookieconsent.initialise({
    type: "opt-out",
    content: {
      message:
        '<span class="s1">This website uses cookies to improve your experience. By continuing to use this website, you are agreeing to accept cookies from the JETNET LLC website.</span> <span class="s2">By choosing “Accept,” you consent to the use of all cookies.</span>',
      deny: "Reject",
      allow: "Accept",
      link: "Read More >>",
      href: "/cookie-policy.html",
      target: "_self",
    },

    onInitialise: function (status) {
      var type = this.options.type;
      var didConsent = this.hasConsented();
      if (type == "opt-in" && didConsent) {
        // enable cookies
      }
      if (type == "opt-out" && !didConsent) {
        // disable cookies
      }
    },

    onStatusChange: function (status, chosenBefore) {
      var type = this.options.type;
      var didConsent = this.hasConsented();
      if (type == "opt-in" && didConsent) {
        // enable cookies
      }
      if (type == "opt-out" && !didConsent) {
        // disable cookies
      }
    },

    onRevokeChoice: function () {
      var type = this.options.type;
      if (type == "opt-in") {
        // disable cookies
      }
      if (type == "opt-out") {
        // enable cookies
      }
    },
  });

}
