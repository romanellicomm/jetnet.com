import Choreographer from "choreographer-js"

jQuery(function() {
  const vh = window.innerHeight
  const dh = Math.max(
    document.documentElement.clientHeight,
    document.documentElement.offsetHeight,
    document.documentElement.scrollHeight
  )

  const choreographer = new Choreographer({
    animations: [
      {
        range: [0, 10000],
        selector: ".altimeter__content",
        type: "scale",
        style: "transform:translateY",
        from: 0,
        to: -250,
        unit: "vh"
      },
      {
        //range: [vh, Math.max(dh - vh, vh * 2)],
        range: [0, Math.min(dh - vh, vh * 4)],
        selector: ".altimeter__content",
        type: "scale",
        style: "opacity",
        from: 1,
        to: 0
      },
      {
        range: [0, 60],
        selector: ".navbar-tagline",
        type: "scale",
        style: "opacity",
        from: 1,
        to: 0
      },
    ]
  })

  window.addEventListener("scroll", function() {
    choreographer.runAnimationsAt(window.pageYOffset)
  })
})
