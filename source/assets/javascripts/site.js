import jQuery from "jquery"
import Popper from "popper.js"
import SmoothScroll from "smooth-scroll"

import "bootstrap"

import "./cookies.js";
import "./altimeter.js"
import "./parallax-background.js"
import "./referer-tracking.js"
import "./gallery-lightbox.js"
import "./forms.js"
import "./schedule.js"
import "./pages.index.js"
import "./pages.reports.js"
import "./pages.make-a-payment.js"
import "./pages.iq-summit-gallery-2018.js"
import "./pages.iq-summit-gallery-2019.js"
import "./pages.iq-summit-gallery-2021.js"
import "./pages.iq-summit-gallery-2022.js"
import "./pages.iq-summit-gallery-2023.js"
//import "./pages.iq-summit-galleries.js"

import "./iq-summit-pricing.js"

const scroll = new SmoothScroll('a[href^="#"]:not([href="#"])')
