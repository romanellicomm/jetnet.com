import intlTelInput from 'intl-tel-input';

const form = document.querySelector('[data-salesforce]');

if (form) {

	const phoneNumberInput = document.querySelector('[data-intl-phone-number]');
	const countryCodeInput = document.querySelector('[data-intl-country-code]');
	
	if (phoneNumberInput) {
		const iti = intlTelInput(phoneNumberInput, {
			//initialCountry: "auto",
			preferredCountries: ["us"],
			customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
				return "e.g. " + selectedCountryPlaceholder;
			},
			formatOnDisplay: true,
			hiddenInput: phoneNumberInput.getAttribute('id'),
			separateDialCode: true,
			utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.19/js/utils.min.js",
		});
	
		// Prevent the original phone number input from submitting
		phoneNumberInput.name = "phone_number_raw";
	
		// Function to update the hidden country code input field
		function updateCountryCode() {
			const selectedCountryData = iti.getSelectedCountryData();
			countryCodeInput.value = selectedCountryData.dialCode;
		}
		
		if (countryCodeInput) {	
			// Update the country code input field initially
			updateCountryCode();
		
			// Update the country code input field when the selected country changes
			phoneNumberInput.addEventListener('countrychange', updateCountryCode);
		}
		
		/*
		// Update the hidden field when the user types, not just on
		// form submit (to avoid conflicts with other onsubmit handlers)
		const hiddenPhoneNumberInput = document.querySelector(`[name='${phoneNumberInput.getAttribute('name')}']`);
		phoneNumberInput.addEventListener('keyup', (event) => {
			hiddenPhoneNumberInput.value = iti.getNumber();
		});
		*/
	
	}
}