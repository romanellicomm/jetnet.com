var requestDemoButtons = document.querySelectorAll('[href*="/request-demo"]');

for (var i = 0; i < requestDemoButtons.length; i++) {
  var button = requestDemoButtons[i];
  var url = new URL(button.href);
  var params = new URLSearchParams(url.search);

  // TODO: scrub query params from these values to avoid referrer-inception
  url.searchParams.set('referrer_title', document.title);
  url.searchParams.set('referrer_url', window.location.href);

  button.href = url.toString();
}
