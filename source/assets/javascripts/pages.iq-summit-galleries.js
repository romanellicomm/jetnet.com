import React from "react";
import Lightbox from "react-image-lightbox";

// Helper function to generate an array of numbers
function range(start, end) {
  return new Array(end - start + 1).fill().map((d, i) => i + start);
}

// Helper function to generate image URLs
function imageObject(year, i) {
  return {
    thumb: `/assets/images/summit/galleries/iq-summit-${year}-gallery-${i}.jpg`,
    large: `/assets/images/summit/galleries/iq-summit-${year}-gallery-${i}.jpg`
  };
}

export default class SummitGallery extends React.Component {
  constructor(props) {
    super(props);

    const { year, totalImages } = props;
    this.state = {
      galleryImages: range(1, totalImages).map(i => imageObject(year, i)),
      galleryIndex: 0,
      isOpen: false
    };
  }

  launchGallery(e, galleryIndex) {
    e.preventDefault();
    this.setState({
      galleryIndex: galleryIndex,
      isOpen: true
    });
    return false;
  }

  render() {
    const { year, title, description } = this.props;
    const { galleryImages, galleryIndex, isOpen } = this.state;

    return (
      <div className="iq-summit-gallery">
        <div className="container">
          <div className="row">
            <div className="offset-lg-3 col-lg-9 mb-5">
              <br />
              <h2>{title}</h2>

              <img
                src={`/assets/images/summit/galleries/iq-summit-gallery-header-${year}.jpg`}
                className="img-fluid d-block mb-3"
                alt="iQ Summit"
              />

              <p>{description}</p>

              <a
                className="btn btn-primary"
                href="#"
                onClick={e => this.launchGallery(e, galleryIndex)}
              >
                <span className="icon icon--double-arrow" /> View Gallery
              </a>
            </div>
          </div>
        </div>

        <div>
          {isOpen && (
            <Lightbox
              mainSrc={galleryImages[galleryIndex].large}
              nextSrc={galleryImages[(galleryIndex + 1) % galleryImages.length].large}
              prevSrc={galleryImages[(galleryIndex + galleryImages.length - 1) % galleryImages.length].large}
              onCloseRequest={() => this.setState({ isOpen: false })}
              onMovePrevRequest={() =>
                this.setState({
                  galleryIndex: (galleryIndex + galleryImages.length - 1) % galleryImages.length
                })
              }
              onMoveNextRequest={() =>
                this.setState({
                  galleryIndex: (galleryIndex + 1) % galleryImages.length
                })
              }
            />
          )}
        </div>
      </div>
    );
  }
}

function initSummitGallery(element) {
  const year = element.dataset.year; // || '2023';
  const totalImages = parseInt(element.dataset.totalImages, 10); // || 98;
  const title = element.dataset.title; // || `${year} Summit Snapshots`;
  const description = element.dataset.description; // || `See photos from the ${year} JETNET iQ Summit.`;

  ReactDOM.render(
    <SummitGallery
      year={year}
      totalImages={totalImages}
      title={title}
      description={description}
    />,
    element
  );
}

// Initialize all SummitGallery elements on the page
document.addEventListener("DOMContentLoaded", () => {
  alert('1');
  const summitGalleryElements = document.querySelectorAll("[data-gallery='summit']");
  summitGalleryElements.forEach(initSummitGallery);
});
