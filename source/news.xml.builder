---
blog: news
title: JETNET News
---

articles = blog.articles

xml.instruct!
xml.feed "xmlns" => "http://www.w3.org/2005/Atom" do
  site_url = config[:host] + "/"
  feed_url = URI.join(site_url, current_page.path).to_s

  xml.title current_page.data.title
  xml.id "urn:uuid:" + UUIDTools::UUID.md5_create(UUIDTools::UUID_DNS_NAMESPACE, feed_url)
  xml.link "href" => site_url
  xml.link "href" => feed_url, "rel" => "self"
  xml.updated(articles.first.date.to_time.iso8601) unless articles.empty?
  xml.author { xml.name "JETNET" }

  articles[0..10].each do |article|
    xml.entry do
      article_url = URI.join(site_url, article.url).to_s

      xml.title article.title, "type" => "html"
      xml.link "rel" => "alternate", "href" => article_url
      xml.id "urn:uuid:" + UUIDTools::UUID.md5_create(UUIDTools::UUID_DNS_NAMESPACE, article_url)
      xml.published article.date.to_time.iso8601
      xml.updated File.mtime(article.source_file).iso8601
      xml.author { xml.name "JETNET" }
      xml.summary article.summary, "type" => "html"
      # xml.content article.body, "type" => "html"
    end
  end
end
