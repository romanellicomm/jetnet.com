<%@ Language=VBScript %>
<%
Option Explicit

' This script intercepts requests for PDF files, sends a page_view event to
' Google Analytics 4 for tracking, and then streams the PDF file back to the client
' without changing the URL.
'
' It first generates a random client ID for use with GA4 tracking, and then retrieves
' the requested PDF file from the query string and checks if it exists on the server.
' If it doesn't exist, the script returns a 404 error.
'
' The script then sends a page_view event to GA4 using the Measurement Protocol API
' with the client ID and page location information.
'
' The response is then configured to serve the requested PDF file with the appropriate
' content type and filename headers.
'
' Finally, the script streams the contents of the PDF file to the client in chunks,
' with a configurable chunk size to avoid memory issues with large files.
'
' Developed by zac@romanelli.com for JETNET, May 2023.

' Generate a random client ID for Google Analytics
Function GenerateClientId()
	Randomize
	GenerateClientId = Int((10^16 - 1) * Rnd + 1)
End Function

' Get the requested PDF filename from the query string
Dim requestedPdfFileName, requestedPdfFilePath
requestedPdfFileName = Request.QueryString("pdf")
requestedPdfFilePath = Server.MapPath(requestedPdfFileName)

' Get the full request URL
Dim protocol, serverName, originalUrl, requestUrl
If Request.ServerVariables("HTTPS") = "on" Then
	protocol = "https://"
Else
	protocol = "http://"
End If
serverName = Request.ServerVariables("SERVER_NAME")
originalUrl = Request.ServerVariables("HTTP_X_ORIGINAL_URL")
requestUrl = protocol & serverName & originalUrl

' Check if the requested PDF file exists
Dim objFSO
Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
If Not objFSO.FileExists(requestedPdfFilePath) Then
	Response.Status = "404 Not Found"
	Response.End
End If

' Send a page_view event to Google Analytics 4
Dim measurementId, apiSecret, eventName, pageLocation, clientId, jsonPayload, objHttp
measurementId = "G-S67RL0RN7S"
apiSecret = "qrucffsSR0SQMUFf4ZdefQ"
eventName = "page_view"
clientId = GenerateClientId()
pageLocation = requestUrl
jsonPayload = "{""client_id"":""" & clientId & """,""events"":[{""name"":""" & eventName & """,""params"":{""page_location"":""" & pageLocation & """}}]}"
Set objHttp = Server.CreateObject("Msxml2.ServerXMLHTTP.6.0")
With objHttp
	.Open "POST", "https://www.google-analytics.com/mp/collect?measurement_id=" & measurementId & "&api_secret=" & apiSecret, False
	.SetRequestHeader "Content-Type", "application/json"
	.Send jsonPayload
End With
Set objHttp = Nothing

' Set the content type and filename for the response
Dim responsePdfFileName
responsePdfFileName = Replace(requestedPdfFileName, " ", "%20") ' Encode spaces in filename
Response.ContentType = "application/pdf"
Response.AddHeader "Content-Disposition", "inline; filename=""" & responsePdfFileName & """"

' Stream the PDF file back to the client
Dim objStream
Set objStream = Server.CreateObject("ADODB.Stream")
objStream.Type = 1 ' adTypeBinary
objStream.Open
objStream.LoadFromFile requestedPdfFilePath

' Set chunk size for streaming the PDF file
Const chunkSize = 131072 ' 128KB buffer size

' Read and stream the PDF file to the client in chunks
Dim bytesRead
Do Until objStream.EOS
	bytesRead = objStream.Read(chunkSize)
	Response.BinaryWrite bytesRead
	Response.Flush
Loop

' Clean up
objStream.Close
Set objStream = Nothing
Set objFSO = Nothing

%>
