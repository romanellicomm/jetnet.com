---
title: "Especialista en Estudios en Aviación – Hispanohablante"
language: "es"
meta_description:
opengraph:
  title: "Set Your Sights On A Career In Business Aviation"
  description: "Apply Online Today"
  image: /assets/opengraph/JNCareers_FB.jpg
twitter_card:
  title: "Set Your Sights On A Career In Business Aviation. Apply Online Today."
  description: ""
  image: /assets/opengraph/JNCareers_TW.jpg
noindex: true
---

<p class="lead">Buscamos personas excepcionales para el puesto de Especialista en estudios de aviación en apoyo de nuestro programa de soluciones de nuestra base de datos de aviación que ofrecemos al comunidad de profesionales de aviación mundiales. Esta posición es de tiempo completo.</p>

<h3>Descripción</h3>

<p>Al término de nuestra formación integral, proporcionada al contratarlo, se pondrá en contacto con los propietarios y operadores de la comunidad de la aviación ejecutiva para obtener, verificar y registrar correctamente la información en nuestra base de datos personalizada, mientras se mantiene la integridad del programa global de soluciones de bases de datos de aviación de JETNET. Obtendrá información a través de varios medios, incluyendo llamadas nacionales e internacionales salientes, correo electrónico y el uso de diversos registros y fuentes externas.</p>

<h3>Cualificaciones</h3>

<ul class="list--double-arrow">
  <li>Mínimo de 18 años con diploma de escuela secundaria o equivalente</li>
  <li>Autorizado para trabajar en los Estados Unidos</li>
  <li>Excelentes habilidades verbales y escritas</li>
  <li>Experiencia de correo electrónico de Microsoft Office</li>
  <li>La atención al detalle - la entrada de datos precisa requerido</li>
  <li>Capacidad para realizar múltiples tareas en un entorno de ritmo rápido</li>
  <li>Voluntad de permanecer sentado y usar el teléfono y la computadora durante períodos prolongados</li>
  <li>Posee una personalidad extrovertida</li>
  <li>Poseer un alto nivel de comodidad al comunicarse con muchas personas y diferentes personalidades, tanto internas como externas, incluida una audiencia global de personas de alto perfil.</li>
  <li>Mantente profesional mientras demuestra integridad, diplomacia y tacto en todo momento.</li>
  <li>1 año de experiencia del cliente por teléfono que se considera una ventaja</li>
  <li>La experiencia de la aviación es una ventaja</li>
</ul>

<p>Consideramos a nuestro personal extraordinario como profesionales valiosos, que brindan un ambiente de trabajo acogedor, y es por eso que ofrecemos un salario inicial competitivo, la última tecnología, beneficios superiores e independencia, y reconocemos experiencia adicional y habilidades lingüísticas.</p>
