---
title: "Aviation Research Specialist – English Speaking"
language: "en"
meta_description:
opengraph:
  title: "Set Your Sights On A Career In Business Aviation"
  description: "Apply Online Today"
  image: /assets/opengraph/JNCareers_FB.jpg
twitter_card:
  title: "Set Your Sights On A Career In Business Aviation. Apply Online Today."
  description: ""
  image: /assets/opengraph/JNCareers_TW.jpg
---

<p class="lead">JETNET is looking for an entry-level, full-time Aviation Research Specialist to join our research department. Extensive training is provided upon hire.</p>

<h3>Responsibilities</h3>

<ul class="list--double-arrow">
  <li>Collect, update, and verify information surrounding business aircraft in our internal database</li>
  <li>Communicate with national and international business aviation professionals by phone and email</li>
  <li>Conduct independent web research relating to business aviation</li>
  <li>Maintain the highest level of honesty and integrity</li>
</ul>

<h3>Qualifications</h3>

<ul class="list--double-arrow">
  <li>Minimum of 18 years of age with high school diploma or equivalent</li>
  <li>Authorized to work in the United States</li>
  <li>Excellent verbal and written communication skills</li>
  <li>Detail oriented - accurate data entry required</li>
  <li>Ability to multitask in a fast-paced environment</li>
  <li>Comfortable communicating with individuals from around the globe by phone and email</li>
  <li>Team oriented</li>
  <li>1 year of phone-based customer experience considered a plus</li>
  <li>Aviation experience a plus</li>
</ul>
