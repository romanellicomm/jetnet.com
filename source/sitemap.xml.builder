xml.instruct!
xml.urlset 'xmlns' => "http://www.sitemaps.org/schemas/sitemap/0.9" do
  sitemap.resources.select { |resource| resource.destination_path =~ /\.html(\.erb)?$/ }.each do |page|
    next if page.data.noindex

    xml.url do
      xml.loc URI.join(config[:host], page.destination_path.gsub("/index.html", "/").gsub(" ", "%20"))

      xml.lastmod File.mtime(page.source_file).to_time.iso8601
      xml.changefreq page.data.changefreq || "monthly"
      xml.priority page.data.priority || "0.5"
    end
  end

  # We're not indexing PDFs right now, but I suspect that will change
  # sitemap.resources.select { |resource| resource.destination_path =~ /\.pdf$/ }.each do |pdf|
  #   #last_mod = if page.path.start_with?('articles/')
  #   #  File.mtime(page.source_file).to_time
  #   #else
  #   #  Time.now
  #   #end
  #
  #   xml.url do
  #     xml.loc URI.join(config[:host], pdf.destination_path.gsub(" ", "%20"))
  #
  #     xml.lastmod File.mtime(pdf.source_file).to_time.iso8601
  #     xml.changefreq pdf.data.changefreq || "monthly"
  #     xml.priority pdf.data.priority || "0.5"
  #   end
  # end
end
