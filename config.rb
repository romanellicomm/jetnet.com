require 'rubygems'
require 'sanitize'

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

helpers do
  def nav_link(link_text, url, options = {})
    options[:class] ||= ""
    options[:class] << " active" if url == current_page.url
    link_to(link_text, url, options)
  end

  def sanitize(html, options = {})
    Sanitize.clean(html, options)
  end

  def format_date_range(start_date, end_date)
    return start_date.strftime('%B %-d, %Y') if start_date == end_date
  
    if start_date.year == end_date.year
      if start_date.month == end_date.month
        "#{start_date.strftime('%B %-d')}–#{end_date.strftime('%-d, %Y')}"
      else
        "#{start_date.strftime('%B %-d')}–#{end_date.strftime('%B %-d, %Y')}"
      end
    else
      "#{start_date.strftime('%B %-d, %Y')}–#{end_date.strftime('%B %-d, %Y')}"
    end
  end

  def blog_meta_description
    if current_article
      blog_name = current_article.blog_controller.name.to_s.strip
      
      if blog_name == "blog"
        return current_article.data.summary || sanitize(current_article.summary)
      end

      if blog_name == "news"
        return current_article.data.summary || sanitize(current_article.summary)
      end
      
      if blog_name == "iq_pulse"
        return current_article.data.summary || sanitize(current_article.summary)
      end
      
      #if blog_name == "careers"
      #end
      
      if blog_name == "training_courses"
        return current_article.data.summary || sanitize(current_article.summary)
      end
    end
    
    nil
  end
end

configure :build do
  after_build do |builder|
    source = 'build/redirects'
    dest = 'build/_redirects'
    FileUtils.mv(source, dest)
  end
end

activate :asset_hash, :ignore => [
  /summit\/galleries/,
  /assets\/stylesheets\/wufoo.css/
]

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end

config[:css_dir] = 'assets/stylesheets'
config[:fonts_dir] = 'assets/fonts'
config[:js_dir] = 'assets/javascripts'

configure :development do
  config[:env] = "development"
  config[:host] = "http://localhost:4567"
end

configure :staging do
  config[:env] = "staging"
  config[:host] = "https://staging--jetnet-com.netlify.app"
end

configure :production do
  config[:env] = "production"
  config[:host] = "https://www.jetnet.com"
end

activate :blog do |blog|
  blog.name = 'blog'
  blog.prefix = 'blog'
  blog.layout = 'blog_layout'
  blog.sources = '{year}-{month}-{day}-{title}.html'
  blog.permalink = '{title}.html'
  blog.summary_length = 500
  blog.publish_future_dated = true
  blog.taglink = "tags/{tag}.html"
  blog.tag_template = "blog/tag.html"
  #blog.tag_template = "blog/tag.html.erb"
  #blog.tag_template = "tag.html"
  #blog.tag_template = "tag.html.erb"
end

activate :blog do |blog|
  blog.name = 'news'
  blog.prefix = 'news'
  blog.layout = 'news_layout'
  blog.sources = '{year}-{month}-{day}-{title}.html'
  blog.permalink = '{title}.html'
  blog.summary_length = 350
  blog.publish_future_dated = true
end

activate :blog do |blog|
  blog.name = 'iq_pulse'
  blog.prefix = 'iq-pulse'
  blog.layout = 'iq_pulse_layout'
  blog.sources = '{year}-{month}-{day}-{title}.html'
  blog.permalink = '{title}.html'
  blog.summary_length = nil
  blog.publish_future_dated = true
end

activate :blog do |blog|
  blog.name = 'careers'
  blog.prefix = 'careers'
  blog.layout = 'careers_layout'
  blog.sources = '{year}-{month}-{day}-{title}.html'
  blog.permalink = '{title}.html'
  blog.summary_length = nil
  blog.publish_future_dated = true
end

activate :blog do |blog|
  blog.name = 'training_courses'
  blog.layout = 'training_course_layout'
  blog.prefix = 'training'
  blog.sources = 'courses/{order}-{title}.html'
  blog.permalink = '{title}.html'
  blog.summary_length = nil
  blog.publish_future_dated = true
end

activate :livereload

activate :external_pipeline,
  name: :webpack,
  command: build? ?
    "./node_modules/webpack/bin/webpack.js --bail -p" :
    "./node_modules/webpack/bin/webpack.js --watch -d --progress --color",
  source: "build",
  latency: 1

config[:rewrites] = [
  {
    name: "Training Courses",
    match_url: '^about-us/training-courses/?(.*)',
    action_type: :redirect,
    action_url: '/training/{R:1}',
    permanent: true,
  },
  {
    name: "PDF Passthrough for Google Analytics",
    match_url: '^(.*\.pdf)$',
    action_type: :rewrite,
    action_url: '/pdf-handler.asp?pdf={UrlEncode:{R:1}}',
  },  
]

config[:redirects] = [
  {
    permanent: true,
    destination: '/legal/privacy-policy',
    paths: [
      'privacy',
    ]
  },
  {
    permanent: true,
    destination: '/legal/cookie-policy',
    paths: [
      'cookie-policy.html',
    ]
  },
  {
    permanent: true,
    destination: '/',
    paths: [
      'products/jetnet-global.html',
      'jetnetglobal.shtml',
      'jetnet40.pdf'
    ]
  },
  {
    permanent: true,
    destination: '/events/ebace',
    paths: [
      'events/ebace/2023'
    ]
  },
  {
    permanent: true,
    destination: '/research',
    paths: [
      'about-us/index.html',
      'about-us/our-research.html',
      'about.html',
      'businessaviation.html',
      'businessaviation.shtml',
      'our-research.html',
      'research.html',
      'research.shtml',
      'research/aviation-research.html',
      'research/our-research.html',
    ]
  },
  {
    permanent: true,
    destination: '/research/business-aircraft/',
    paths: [
      'research/business-aircraft.html',
    ]
  },
  {
    permanent: true,
    destination: '/products/aerodex.html',
    paths: [
      'aerodex.html',
      'aerodex.shtml',
      'aerodex4.html',
      'aerodex5.html',
      'aerodex40.pdf',
      'aerodex50.pdf',
      'aerodexevo.html',
      'aerodexevolution.pdf',
    ]
  },
  {
    permanent: true,
    destination: '/products/marketplace-manager.html',
    paths: [
      'crm.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/fleets/business-jets-turboprops-pistons.html',
    paths: [
      'aircraft.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/fleets/commercial-aircraft.html',
    paths: [
      'bigplanes.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/about-us/our-team/',
    paths: [
      'about-us/our-team.html',
      'leadership.shtml',
      'alliances.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/solutions/industry-analysts-consultants.html',
    paths: [
      'finance.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/solutions/aircraft-sales.html',
    paths: [
      'aircraftsales.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/solutions/charter-management.html',
    paths: [
      'charter.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/products/aerodex.html',
    paths: [
      'evaerodex.shtml',
      'products/aerodex',
    ]
  },
  {
    permanent: true,
    destination: '/products/evolution-marketplace.html',
    paths: [
      'marketplace.html',
      'marketplace.shtml',
      'evmktplace.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/contact',
    paths: [
      'contact/contact-us.html',
      'contactus.html',
    ]
  },
  {
    permanent: true,
    destination: 'https://www.jetnetevolution.com/',
    paths: [
      'login.html',
      'login.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/products/jetnet-iq',
    paths: [
      'products/jetnet-iq-investor.html',
      'products/jetnet-iq.html',
      'jetnetiq.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/summit',
    paths: [
      'products/jetnet-iq-summit.html',
    ]
  },
  {
    permanent: true,
    destination: '/summit/agenda',
    paths: [
      'summit/agenda.html',
    ]
  },
  {
    permanent: true,
    destination: '/summit/gallery',
    paths: [
      'summit/gallery.html',
    ]
  },
  {
    permanent: true,
    destination: '/summit/register',
    paths: [
      'summit/registration.html',
    ]
  },
  {
    permanent: true,
    destination: '/summit/speakers',
    paths: [
      'summit/speakers.html',
    ]
  },
  {
    permanent: true,
    destination: '/summit/hotel',
    paths: [
      'summit/travel.html',
    ]
  },
  {
    permanent: true,
    destination: '/research/iq-survey',
    paths: [
      'research/iq-survey.html',
    ]
  },
  {
    permanent: true,
    destination: '/research/business-aircraft/',
    paths: [
      'research/business-aircrafts.html',
    ]
  },
  {
    permanent: true,
    destination: '/request-demo',
    paths: [
      'req_demo.shtml',
      'demo',
    ]
  },
  {
    permanent: true,
    destination: '/news',
    paths: [
      'news.html',
    ]
  },
  {
    permanent: true,
    destination: '/make-a-payment/',
    paths: [
      'contact/make-a-payment.html',
      'payment.shtml',
    ]
  },
  {
    permanent: true,
    destination: '/careers',
    paths: [
      'about-us/careers.html',
    ]
  },
  {
    permanent: true,
    destination: '/careers/research-associate-americas-region.html',
    paths: [
      'careers/research-associate-english-speaking.html',
    ]
  },
  {
    permanent: true,
    destination: '/about-us/company-profile',
    paths: [
      'about-us/facilities.html',
    ]
  },
]

require "uuidtools"
