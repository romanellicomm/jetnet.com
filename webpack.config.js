"use strict"

const webpack = require("webpack")

const cssLoaders = [
  {
    loader: "css-loader",
    options: {
      modules: true,
      minimize: true
    }
  },
  {
    loader: "sass-loader"
  }
]

module.exports = {
  context: __dirname + "/source",
  entry: {
    head: ["./assets/javascripts/head.js"],
    site: ["./assets/javascripts/site.js"]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        query: {
          presets: ["env", "react"]
        }
      }
    ]
  },
  output: {
    path: __dirname + "/build/assets/javascripts",
    filename: "[name].bundle.js"
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      Popper: ["popper.js", "default"]
    })
  ]
}
